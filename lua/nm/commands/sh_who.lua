// RAWR!

local function who(ply, targets)
	local format = "%s%s %s%s "
	local text = string.format(format, "Name", string.rep(" ", 20 - 4), "SteamID", string.rep(" ", 20 - 7))
	text = text.."Rank"
	NM.util.Console(ply, text)
	if (!targets) then
		targets = player.GetAll()
	end
	for k,v in pairs(targets) do
		local steamid = tostring(v:SteamID())
		local nick = v:Nick()
		local text = string.format(format, nick, string.rep(" ", 20 - nick:len()), steamid, string.rep(" ", 20 - steamid:len()))
		text = text..v:NM_Rank_GetRank()
		NM.util.Console(ply, text)
	end
end
local whoCommand = NM.commands.Add("utility", "who", who)
whoCommand:AddParam(NM.commands.args.Players, "target", false, {})
whoCommand:SetHelp("Display users, groups, and their SteamIDs for ease")
whoCommand:ConsoleSafe()
