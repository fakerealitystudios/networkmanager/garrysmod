// RAWR!

// Thanks ULX team, make my life easier this way... (I'll only modify a little)
local function playerSend( from, to, force )
	if not to:IsInWorld() and not force then return false end -- No way we can do this one

	local yawForward = to:EyeAngles().yaw
	local directions = { -- Directions to try
		math.NormalizeAngle( yawForward - 180 ), -- Behind first
		math.NormalizeAngle( yawForward + 90 ), -- Right
		math.NormalizeAngle( yawForward - 90 ), -- Left
		yawForward,
	}

	local t = {}
	t.start = to:GetPos() + Vector( 0, 0, 32 ) -- Move them up a bit so they can travel across the ground
	t.filter = { to, from }

	local i = 1
	t.endpos = to:GetPos() + Angle( 0, directions[ i ], 0 ):Forward() * 47 -- (33 is player width, this is sqrt( 33^2 * 2 ))
	local tr = util.TraceEntity( t, from )
	while tr.Hit do -- While it's hitting something, check other angles
		i = i + 1
		if i > #directions then	 -- No place found
			if force then
				from._prevpos = from:GetPos()
				from._prevang = from:EyeAngles()
				return to:GetPos() + Angle( 0, directions[ 1 ], 0 ):Forward() * 47
			else
				return false
			end
		end

		t.endpos = to:GetPos() + Angle( 0, directions[ i ], 0 ):Forward() * 47

		tr = util.TraceEntity( t, from )
	end

	from._prevpos = from:GetPos()
	from._prevang = from:EyeAngles()
	return tr.HitPos
end

local function returntp(ply, targets)
	local target = targets[1]
    if (not target:NM_Alive()) then
        return false, "Person must be alive!"
    end
	if (!target._prevpos) then
		return false, "Target has no previous location!"
	end
	target.ulx_prevpos = nil // Fuck yourself ulx
	if (target:InVehicle()) then
		target:ExitVehicle()
	end

	target:SetPos(target._prevpos)
	target:SetEyeAngles(target._prevang)
	target:SetLocalVelocity(Vector(0, 0, 0)) // Stop!
	target._prevpos = nil

	NM.administration.Broadcast(ply, " returned ", targets)
end
local returnCommand = NM.commands.Add("teleportation", "return", returntp)
returnCommand:AddParam(NM.commands.args.Player, "target", true, {})
returnCommand:SetHelp("Return a player to their last location")
returnCommand:ConsoleSafe()

local function bring(ply, targets)
	local target = targets[1]
    if (not target:NM_Alive()) then
        return false, "Person must be alive!"
    end
    local newpos, newang, force = false, Angle(), ply:GetMoveType() == MOVETYPE_NOCLIP
	newpos = playerSend(target, ply, force)
	if (!newpos) then
		return false, "Can't find a place to put the target!"
	end
	newang = (target:GetPos() - newpos):Angle()

	if (target:InVehicle()) then
		target:ExitVehicle()
	end

	target:SetPos(newpos)
	target:SetEyeAngles(newang)
	target:SetLocalVelocity(Vector(0, 0, 0)) -- Stop!

	NM.administration.Broadcast(ply, " brought ", targets)
end
local bringCommand = NM.commands.Add("teleportation", "bring", bring)
bringCommand:AddParam(NM.commands.args.Player, "target", true, {})
bringCommand:SetHelp("Bring a player to you")

local function goto(ply, targets)
	local target = targets[1]
    if (not target:NM_Alive()) then
        return false, "Person must be alive!"
    end
    local newpos, newang, force = false, Angle(), ply:GetMoveType() == MOVETYPE_NOCLIP
	newpos = playerSend(ply, target, force)
	if (!newpos) then
		return false, "Can't find a place to put you!"
	end
	newang = (target:GetPos() - newpos):Angle()

	if (ply:InVehicle()) then
		ply:ExitVehicle()
	end

	ply:SetPos(newpos)
	ply:SetEyeAngles(newang)
	ply:SetLocalVelocity(Vector(0, 0, 0)) -- Stop!

	NM.administration.Broadcast(ply, " teleported to ", targets)
end
local gotoCommand = NM.commands.Add("teleportation", "goto", goto)
gotoCommand:AddParam(NM.commands.args.Player, "target", true, {canTargetHigherRank = true})
gotoCommand:SetHelp("Goto a player")
