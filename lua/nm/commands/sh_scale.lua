// RAWR!

local function scale(ply, targets, amount)
	for k,target in pairs(targets) do
		target:NM_Scale_Set(amount/100)
		if (amount == 100) then
			target:NM_TempData_Set("scale", nil, false)
		else
			target:NM_TempData_Set("scale", amount/100, false)
		end
	end
	NM.administration.Broadcast(ply, " scaled ", targets, " to ", (amount), "%")
end
local scaleCommand = NM.commands.Add("fun", "scale", scale)
scaleCommand:AddParam(NM.commands.args.Players, "targets", true, {})
scaleCommand:AddParam(NM.commands.args.Amount, "amount", false, {default = 1})
scaleCommand:SetHelp("Set a player's scale")
scaleCommand:ConsoleSafe()

local function SetScale(ply)
	ply:NM_Scale_Set(ply:NM_TempData_Get("scale", 1))
end

hook.Add("PlayerSpawn", "PlayerSpawn.SetScale", SetScale)
hook.Add("MK_Player.LoadedData", "MK_Player.LoadedData.SetScale", SetScale)
