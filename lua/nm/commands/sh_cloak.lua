// RAWR!

local function cloak(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		target:SetNoDraw(bool)
		table.insert(affectedTargets, target)
	end

	NM.administration.Broadcast(ply, {bool, " cloaked ", " uncloaked "}, affectedTargets)
end
local cloakCommand = NM.commands.Add("player", "cloak", cloak)
cloakCommand:AddParam(NM.commands.args.Players, "targets", true, {})
cloakCommand:AddParam(NM.commands.args.Boolean, "bool", false, {default = true})
cloakCommand:SetOpposite("uncloak", {nil, false})
cloakCommand:SetHelp("Make a player invisible")
