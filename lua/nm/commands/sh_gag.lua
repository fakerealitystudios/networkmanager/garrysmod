// RAWR!

local function gag(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		table.insert(affectedTargets, target)
		target:NM_TempData_Set(bool, "gagged")
	end

	NM.administration.Broadcast(ply, {bool, " gagged ", " ungagged "}, affectedTargets)
end
local gagCommand = NM.commands.Add("chat", "gag", gag)
gagCommand:AddParam(NM.commands.args.Players, "targets", true, {})
gagCommand:AddParam(NM.commands.args.Boolean, "bool", false, {default = true, hidden = true})
gagCommand:SetOpposite("ungag", {nil, false})
gagCommand:SetHelp("Disables a player's ability to talk in voice chat")
gagCommand:ConsoleSafe()

if (SERVER) then
	hook.Add("PlayerCanHearPlayersVoice", "NM_Administration.PlayerCanHearPlayersVoice", function(listener, talker)
		if (talker:NM_TempData_Get("gagged")) then return false end
	end)
else
	hook.Add("PlayerStartVoice", "NM_Administration.PlayerStartVoice", function(ply)
		if (ply == LocalPlayer()&&(ply:NM_TempData_Get("gagged"))) then
			ply:NM_SendChat(Color(255,255,255), "You've been gagged, sorry! No mic right now.")
		end
	end)
end
