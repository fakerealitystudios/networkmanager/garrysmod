// RAWR!

local function disguise(ply)

	local steamid = "STEAM_0:0:76795553"
	local uniqueid = "3124025067"
	ply:NM_TempData_Set(true, "disguise", "enabled")
	ply:NM_TempData_Set("Unknown Warrior", "disguise", "name")
	ply:NM_TempData_Set("user", "disguise", "rank")
	ply:NM_TempData_Set(steamid, "disguise", "steamid")
	ply:NM_TempData_Set(util.SteamIDTo64(steamid), "disguise", "steamid64")
	ply:NM_TempData_Set(uniqueid, "disguise", "uniqueid")

	if (PS) then
		for item_id, item in pairs(ply.PS_Items) do
			if item.Equipped then
				local ITEM = PS.Items[item_id]
				ITEM:OnHolster(ply, item.Modifiers)
			end
		end

		ply:PS_LoadData()
		ply:PS_SendClientsideModels()

		timer.Simple(1, function()
			if (!IsValid(ply) || !ply:NM_Alive()) then return end
			for item_id, item in pairs(ply.PS_Items) do
				local ITEM = PS.Items[item_id]
				local canEquip = true
				local message = ""
				if type(ITEM.CanPlayerEquip) == 'function' then
					canEquip, message = ITEM:CanPlayerEquip(ply)
				elseif type(ITEM.CanPlayerEquip) == 'boolean' then
					canEquip = ITEM.CanPlayerEquip
				end
				if item.Equipped and canEquip then
					ITEM:OnEquip(ply, item.Modifiers)
				end
			end
		end)
	end

	net.Start("NM_Disguise.Avatar")
		net.WriteEntity(ply)
	net.Broadcast()

	if (ply:NM_IsDisguised()) then
		return true, "You have been disguised!"
	end
end
local disguiseCommand = NM.commands.Add("player", "disguise", disguise)
disguiseCommand:SetHelp("Disguise yourself")

local function undisguise(ply)
	ply:NM_TempData_Set(false, "disguise", "enabled")

	if (PS) then
		for item_id, item in pairs(ply.PS_Items) do
			if item.Equipped then
				local ITEM = PS.Items[item_id]
				ITEM:OnHolster(ply, item.Modifiers)
			end
		end

		ply:PS_LoadData()
		ply:PS_SendClientsideModels()

		timer.Simple(1, function()
			if (!IsValid(ply) || !ply:NM_Alive()) then return end
			for item_id, item in pairs(ply.PS_Items) do
				local ITEM = PS.Items[item_id]
				local canEquip = true
				local message = ""
				if type(ITEM.CanPlayerEquip) == 'function' then
					canEquip, message = ITEM:CanPlayerEquip(ply)
				elseif type(ITEM.CanPlayerEquip) == 'boolean' then
					canEquip = ITEM.CanPlayerEquip
				end
				if item.Equipped and canEquip then
					ITEM:OnEquip(ply, item.Modifiers)
				end
			end
		end)
	end

	net.Start("NM_Disguise.Avatar")
		net.WriteEntity(ply)
	net.Broadcast()

	if (!ply:NM_IsDisguised()) then
		return true, "You have been undisguised!"
	end
end
local undisguiseCommand = NM.commands.Add("player", "undisguise", undisguise)
undisguiseCommand:SetHelp("Undisguise yourself")

if (SERVER) then
	util.AddNetworkString("NM_Disguise.Avatar")
else
	net.Receive("NM_Disguise.Avatar", function(len)
		local ply = net.ReadEntity()
		local function recursive(elem)
			if (elem.SetSteamID) then
				elem:SetSteamID(ply:SteamID64(), 32)
			end
			if (elem.GetChildren && table.Count(elem:GetChildren()) > 0) then
				for k,v in pairs(elem:GetChildren()) do
					recursive(v)
				end
			end
		end
		recursive(vgui.GetWorldPanel())
	end)
end