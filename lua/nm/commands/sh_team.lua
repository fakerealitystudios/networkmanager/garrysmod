// RAWR!

local function setteam(ply, targets, teamid)
	for k,target in pairs(targets) do
        if (DarkRP) then
            local canChange, reason = hook.Run("playerCanChangeTeam", target, teamid, true)
            if (canChange == false) then
                return false, reason
            end
        end

        local setTeam = target.changeTeam or target.SetTeam -- DarkRP compatibility
        setTeam(target, teamid, true)
	end

	NM.administration.Broadcast(ply, " set ", targets, " team to ", team.GetName(teamid))
end
local setteamCommand = NM.commands.Add("player", "setteam", setteam)
setteamCommand:AddParam(NM.commands.args.Player, "target", true, {})
setteamCommand:AddParam(NM.commands.args.Team, "team", true, {})
setteamCommand:SetHelp("Set a player's team")
setteamCommand:ConsoleSafe()
setteamCommand:NoChat()
