// RAWR!

local function setHealth(ply, targets, amount)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		target:SetHealth(amount)
		table.insert(affectedTargets, target)
	end

	NM.administration.Broadcast(ply, " set health for ", affectedTargets, " to ", amount)
end
local setHealthCommand = NM.commands.Add("player", "health", setHealth, {"hp", "sethealth", "sethp"})
setHealthCommand:AddParam(NM.commands.args.Players, "targets", true, {})
setHealthCommand:AddParam(NM.commands.args.Amount, "amount", false, {default = 100, min = 0})
setHealthCommand:SetHelp("Set a player's health")
setHealthCommand:ConsoleSafe()
