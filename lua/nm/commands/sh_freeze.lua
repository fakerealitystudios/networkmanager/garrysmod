// RAWR!

local function freeze(ply, targets, bool)
	for k,target in pairs(targets) do
		if (bool&&target:InVehicle()) then
			target:ExitVehicle()
		end
        if (bool) then
            target:Lock()
        else
            target:UnLock()
        end
		target:Freeze(bool)
        target:NM_TempData_Set(bool, "frozen")
	end

	NM.administration.Broadcast(ply, {bool, " froze ", " unfroze "}, targets)
end
local freezeCommand = NM.commands.Add("player", "freeze", freeze)
freezeCommand:AddParam(NM.commands.args.Players, "targets", true, {})
freezeCommand:AddParam(NM.commands.args.Boolean, "bool", false, {default = true, hidden = true})
freezeCommand:SetOpposite("unfreeze", {nil, false})
freezeCommand:SetHelp("Freeze the player")
freezeCommand:ConsoleSafe()

hook.Add("PlayerSwitchWeapon", "PlayerSwitchWeapon.NM_Freeze", function(ply, oldWpn, newWpn)
    if (ply:NM_TempData_Get("frozen")) then
        return false
    end
end)
