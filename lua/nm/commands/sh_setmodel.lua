// RAWR!

local function setmodel(ply, targets, mdl)
	for k,target in pairs(targets) do
        target:SetModel(mdl)
	end
	NM.administration.Broadcast(ply, " set ", targets, " model to " .. mdl)
end
local setmodelCommand = NM.commands.Add("player", "setmodel", setmodel)
setmodelCommand:AddParam(NM.commands.args.Player, "target", true, {})
setmodelCommand:AddParam(NM.commands.args.String, "model", true)
setmodelCommand:SetHelp("Set the player's model to any model on the server(or client)")
setmodelCommand:ConsoleSafe()
