// RAWR!

local function god(ply, targets, bool)
	for k,target in pairs(targets) do
		if (bool) then
			target:GodEnable()
		else
			target:GodDisable()
		end
	end

	NM.administration.Broadcast(ply, {bool, " godded ", " ungodded "}, targets)
end
local godCommand = NM.commands.Add("player", "god", god)
godCommand:AddParam(NM.commands.args.Players, "targets", true, {})
godCommand:AddParam(NM.commands.args.Boolean, "bool", false, {default = true, hidden = true})
godCommand:SetOpposite("ungod", {nil, false})
godCommand:SetHelp("God the player")
godCommand:ConsoleSafe()
