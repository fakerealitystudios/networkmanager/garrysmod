// RAWR!

local function reloadplayer(ply, targets)
	for k,target in pairs(targets) do
        // Loading creates a new session, end the current one
        NM.api.put("/member/"..ply:NM_GetId().."/session/"..target:NM_GetSessionId())
        target.nm.rankdata = {}
        target.nm.rank = NM.ranks.defaultRank
        target.nm.subranks = {}
        target.nm.realms = {}
		target:NM_DB_LoadData()
	end
	NM.administration.Broadcast(ply, " reloaded player ", targets)
end
local reloadplayerCommand = NM.commands.Add("player", "reloadplayer", reloadplayer)
reloadplayerCommand:AddParam(NM.commands.args.Player, "target", true, {})
reloadplayerCommand:SetHelp("Reload the player's data")
reloadplayerCommand:ConsoleSafe()
