// RAWR!

local function mute(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		table.insert(affectedTargets, target)
		target:NM_TempData_Set(bool, "muted")
	end

	NM.administration.Broadcast(ply, {bool, " muted ", " unmuted "}, affectedTargets)
end
local muteCommand = NM.commands.Add("chat", "mute", mute)
muteCommand:AddParam(NM.commands.args.Players, "targets", true, {})
muteCommand:AddParam(NM.commands.args.Boolean, "bool", false, {default = true, hidden = true})
muteCommand:SetOpposite("unmute", {nil, false})
muteCommand:SetHelp("Disables a player's ability to type in chat")
muteCommand:ConsoleSafe()

if (SERVER) then
	hook.Add("PlayerSay", "NM_Administration.PlayerSay", function(ply, text)
		if (ply:NM_TempData_Get("muted")) then
			ply:MK_SendChat(Color(255,255,255), "You've been muted, sorry! No chat right now.")
			return ""
		end
	end)
end
