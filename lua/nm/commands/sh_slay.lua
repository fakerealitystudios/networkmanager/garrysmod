// RAWR!

local function slay(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		if (not target:NM_Alive()) then
			return false, target:Name().." is already dead!"
		elseif (target:IsFrozen()) then
			return false, target:Name().." is frozen!"
		else
			target:Kill()
			table.insert(affectedTargets, target)
		end
	end

	NM.administration.Broadcast(ply, " has slain ", affectedTargets)
end
local slayCommand = NM.commands.Add("player", "slay", slay)
slayCommand:AddParam(NM.commands.args.Players, "targets", true, {})
slayCommand:SetHelp("Slay a player")
slayCommand:ConsoleSafe()

local function sslay(ply, targets, bool)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		if (not target:NM_Alive()) then
			return false, target:Name().." is already dead!"
		elseif (target:IsFrozen()) then
			return false, target:Name().." is frozen!"
		else
			if (target:InVehicle()) then
				target:ExitVehicle()
			end
			target:KillSilent()
			table.insert(affectedTargets, target)
		end
	end

	NM.administration.Broadcast(ply, " has silently slain ", affectedTargets)
end
local sslayCommand = NM.commands.Add("player", "sslay", sslay)
sslayCommand:AddParam(NM.commands.args.Players, "targets", true, {})
sslayCommand:SetHelp("Silently Slay a player")
sslayCommand:ConsoleSafe()
