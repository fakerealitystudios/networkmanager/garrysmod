// RAWR!

local function setArmor(ply, targets, amount)
	local affectedTargets = {}
	for k,target in pairs(targets) do
		target:SetArmor(amount)
		table.insert(affectedTargets, target)
	end

	NM.administration.Broadcast(ply, " set armour for ", affectedTargets, " to ", amount)
end
local setArmorCommand = NM.commands.Add("player", "armor", setArmor, {"setarmor", "setarmour"})
setArmorCommand:AddParam(NM.commands.args.Players, "targets", true, {})
setArmorCommand:AddParam(NM.commands.args.Amount, "amount", false, {default = 100, min = 0, max = 100})
setArmorCommand:SetHelp("Set a player's armor")
setArmorCommand:ConsoleSafe()
