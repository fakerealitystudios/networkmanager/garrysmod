// RAWR!

local function reloadbans(ply)
	NM.bans.Load()

	NM.administration.Broadcast(ply, " reloaded bans")
end
local reloadCommand = NM.commands.Add("utility", "reloadbans", reloadbans)
reloadCommand:SetHelp("Reload the server bans")
reloadCommand:ConsoleSafe()
reloadCommand:NoChat()

local function ban(ply, targets, length, reason)
	local properLength = length
	for k,target in pairs(targets) do
		properLength = NM.administration.Ban(target, length, reason, ply)
	end
	NM.api.external.Stat("ban")
	NM.administration.Broadcast(ply, " banned ", targets, " ", {(properLength == 0), "permanently", {"for ", NM.util.GetTimeleftByString(properLength)}}, "(", reason, ") ", "!")
end
local banCommand = NM.commands.Add("player", "ban", ban)
banCommand:AddParam(NM.commands.args.Player, "target", true, {})
banCommand:AddParam(NM.commands.args.Time, "length", false, {default = 30})
banCommand:AddParam(NM.commands.args.String, "reason", false, {default = "No reason provided"})
banCommand:SetHelp("Ban player")
banCommand:ConsoleSafe()
banCommand:NoChat()

local function banid(ply, steam, length, reason)
	NM.players.external.GetPlayer(steam, function(target)
		local properLength = NM.administration.Ban(target, length, reason, ply)
		NM.api.external.Stat("ban")
		NM.administration.Broadcast(ply, " banned ", target, " ", {(properLength == 0), "permanently", {"for ", NM.util.GetTimeleftByString(properLength)}}, "(", Color(150,150,55), reason, ") ", "!")
	end)
end
local banCommand = NM.commands.Add("player", "banid", banid)
banCommand:AddParam(NM.commands.args.String, "target", true, {})
banCommand:AddParam(NM.commands.args.Time, "length", false, {default = 30})
banCommand:AddParam(NM.commands.args.String, "reason", false, {default = "No reason provided"})
banCommand:SetHelp("Ban player by steam id")
banCommand:ConsoleSafe()
banCommand:NoChat()
/*
local function unbanid(ply, steam)

	NM.administration.Broadcast(ply, " unbanned ", target)
end
local unbanidCommand = NM.commands.Add("player", "unbanid", unbanid)
unbanidCommand:AddParam(NM.commands.args.String, "target", true, {})
unbanidCommand:SetHelp("Unban player by steam id")

local function bannmid(ply, nmid, length, reason)

	NM.administration.Broadcast(ply, " banned ", target)
end
local banCommand = NM.commands.Add("player", "bannmid", bannmid)
banCommand:AddParam(NM.commands.args.Amount, "nmid", true, {})
banCommand:AddParam(NM.commands.args.Time, "length", false, {default = 30})
banCommand:AddParam(NM.commands.args.String, "reason", false, {default = "No reason provided"})
banCommand:SetHelp("Ban player by network manager id")

local function unbannmid(ply, steam)

	NM.administration.Broadcast(ply, " unbanned ", target)
end
local unbanidCommand = NM.commands.Add("player", "unbannmid", unbannmid)
unbanidCommand:AddParam(NM.commands.args.Amount, "nmid", true, {})
unbanidCommand:SetHelp("Unban player by network manager id")
*/
