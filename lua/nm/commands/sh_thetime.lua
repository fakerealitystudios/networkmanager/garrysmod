// RAWR!

local function thetime(ply)
	if ((os.time()-(NM.latestTime or 0))<60) then
		return false, "Wait "..math.floor((os.time()-(NM.latestTime or 0))-60).." seconds before using thetime again!"
	end
	NM.administration.Broadcast("The time is now ", Color(16, 95, 148), os.date("%I:%M %p"))
	NM.latestTime = os.time()
end
local thetimeCommand = NM.commands.Add("utility", "time", thetime, {"thetime"})
thetimeCommand:SetHelp("Get the current server time")
thetimeCommand:ConsoleSafe()
