// RAWR!

local function map(ply, map, gamemode)
	if (!table.HasValue(NM.maps, map:lower())) then return end
	NM.administration.Broadcast(ply, " changed the map to ", Color(100,100,255), map, {(gamemode&&gamemode != ""), " with gamemode "..gamemode, ""})
	if (gamemode and gamemode != "") then
		game.ConsoleCommand("gamemode "..gamemode.."\n")
	end
    timer.Simple(1, function()
       game.ConsoleCommand("changelevel "..map.."\n")
   end)
end
local mapCommand = NM.commands.Add("server", "map", map, {"changelevel"})
mapCommand:AddParam(NM.commands.args.Map, "map", true, {})
mapCommand:AddParam(NM.commands.args.String, "gamemode", false, {})
mapCommand:SetHelp("Change the current map and gamemode")
mapCommand:ConsoleSafe()
