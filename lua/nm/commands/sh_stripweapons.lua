// RAWR!

local function strip(ply, targets)
	local affectedplys = {}
	for k,target in pairs(targets) do
		target:StripWeapons()
		table.insert(affectedplys, target)
	end

	NM.administration.Broadcast(ply, " stripped weapons from ", affectedplys)
end
local stripCommand = NM.commands.Add("player", "strip", strip, {"stripweapons"})
stripCommand:AddParam(NM.commands.args.Players, "targets", true, {})
stripCommand:SetHelp("Strip all weapons of a player")
stripCommand:ConsoleSafe()
