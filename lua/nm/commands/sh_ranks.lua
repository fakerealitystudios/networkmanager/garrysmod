// RAWR!

local function reloadranks(ply)
	NM.ranks.Load()

	NM.administration.Broadcast(ply, " reloaded ranks")
end
local reloadCommand = NM.commands.Add("utility", "reloadranks", reloadranks)
reloadCommand:SetHelp("Reload the server ranks")
reloadCommand:ConsoleSafe()
reloadCommand:NoChat()

local function ranks(ply, targets)
	local target = targets[1]
	local ranks = {}
	for k,v in pairs(target:NM_Rank_GetRanks()) do
		if (table.Count(ranks) > 1) then
			table.insert(ranks, Color(255,255,255))
			table.insert(ranks, ", ")
		end
		table.insert(ranks, Color(150, 150, 150))
		table.insert(ranks, v)
	end
	NM.administration.SelectiveBroadcast(ply, target, " has the following ranks ", unpack(ranks))
end
local ranksCommand = NM.commands.Add("player", "ranks", ranks)
ranksCommand:AddParam(NM.commands.args.Player, "target", true, {})
ranksCommand:SetHelp("Display ranks for this user")
ranksCommand:ConsoleSafe()

local function setrank(ply, targets, rank)
	if (IsValid(ply) && !ply:NM_Permission_Has("nm.user.rank."..rank)) then
		return false, "You don't have permission to set players to that rank!"
	end

	local target = targets[1]
	if (target == ply) then
		return false, "You can't set your own rank!"
	end

	if (target:NM_Rank_SetRank(rank)) then
		NM.administration.Broadcast(ply, " set ", target, "'s rank to ", Color(150, 150, 150), rank, " in the ", Color(200, 200, 200), NM.realm, " realm")
		return true
	end

	return false, "Failed to set user group"
end
local setrankCommand = NM.commands.Add("player", "setrank", setrank)
setrankCommand.permission = "nm.edit.user.rank" // Change the permission needed
setrankCommand:AddParam(NM.commands.args.Player, "target", true, {})
setrankCommand:AddParam(NM.commands.args.Rank, "rank", true, {})
setrankCommand:SetHelp("Set rank for this player(for this server only)")
setrankCommand:ConsoleSafe()
