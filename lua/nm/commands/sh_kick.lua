// RAWR!

local function kick(ply, targets, reason)
	for k,target in pairs(targets) do
		NM.administration.Kick(target, reason, ply)
	end
	NM.api.external.Stat("kick")
	NM.administration.Broadcast(ply, " kicked ", targets, "(", reason, ")")
end
local kickCommand = NM.commands.Add("player", "kick", kick)
kickCommand:AddParam(NM.commands.args.Player, "target", true, {})
kickCommand:AddParam(NM.commands.args.String, "reason", false, {default = "No reason, cause we can"})
kickCommand:SetHelp("Kick a player from the server")
kickCommand:ConsoleSafe()
