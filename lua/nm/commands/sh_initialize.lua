// RAWR!

local function initialize(ply, endpoint, sid, secret)
	if (NM.config.Get("api_authentication", {secret = "XXXXXXXX"}).secret != "XXXXXXXX") then return false, "Server already has a secret! Delete the nm data folder to reset!" end
    NM.config.Set("api_endpoint", endpoint, true, true)
    NM.config.Set("api_authentication", {serverid = sid, secret = secret}, true, true)

    NM.server.index()
    hook.Add("NM_Server.Ready", "NM_Server.Ready.InitializeCmd", function()
        print("[MK-A] Server has been initialized and cached!")
        hook.Remove("NM_Server.Ready", "NM_Server.Ready.InitializeCmd")
    end)
end
local initializeCommand = NM.commands.Add("server", "initialize", initialize)
initializeCommand:AddParam(NM.commands.args.String, "endpoint", true)
initializeCommand:AddParam(NM.commands.args.Amount, "serverid", true)
initializeCommand:AddParam(NM.commands.args.String, "secret", true)
initializeCommand:SetHelp("Initialize NetworkManager")
initializeCommand:ConsoleSafe()
initializeCommand:NoChat()
