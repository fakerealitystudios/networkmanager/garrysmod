// RAWR!

NM.menus.selectedCmd = nil

local CATEGORY = {}
	CATEGORY.name = "Commands"
	CATEGORY.adminOnly = true

	local function PaintButton(this, w, h)
		surface.SetDrawColor(255, 255, 255, 255)
		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(220, 220, 220, 255)
		surface.DrawOutlinedRect(1, 1, w - 2, h - 2)

		surface.SetDrawColor(0, 0, 0, 60)
		surface.DrawOutlinedRect(0, 0, w, h)

		if (this.entered) then
			surface.SetDrawColor(0, 0, 0, 75)
			surface.DrawRect(0, 0, w, h)
		end

		if (NM.menus.selectedCmd != nil && this.cmd == NM.menus.selectedCmd) then
			surface.SetDrawColor(NM.menus.color.r, NM.menus.color.g, NM.menus.color.b, 150)
			surface.DrawRect(0, 0, w, h)
		end
	end

	function CATEGORY:Layout(panel)
		NM.menus.selectedCmd = nil

		panel.search = panel:Add("DTextEntry")
		panel.search:Dock(TOP)
		panel.search:DockMargin(4, 4, 4, 0)
		panel.search.OnTextChanged = function(this)
			local text = this:GetText()

			if (text == "") then
				self:ListPlayers(panel.scroll)
			else
				local players = {}

				for k, v in ipairs(player.GetAll()) do
					if (NM.util.StringMatches(v:Name(), text) or v:SteamID():match(text)) then
						players[k] = v
					end
				end

				self:ListPlayers(panel.scroll, players)
			end
		end
		panel.search:SetToolTip("Search for players by their name/SteamID.")

		panel.contents = panel:Add("DPanel")
		panel.contents:DockMargin(4, 4, 4, 4)
		panel.contents:Dock(TOP)
		panel.contents:SetTall(panel:GetTall() - 31)

		panel.cmds = panel.contents:Add("DCategoryList")
		panel.cmds:Dock(LEFT)
		panel.cmds:SetWide(panel:GetWide() * 0.335)
		panel.cmds.Paint = PaintButton
		panel.cmds:DockMargin(0, 4, 4, 4)
		panel.cmds:DockPadding(0, 1, 0, 0)

		panel.button = panel.contents:Add("DButton")
		panel.button.Paint = function(this, w, h)
			surface.SetDrawColor(NM.menus.color.r, NM.menus.color.g, NM.menus.color.b, 255)
			surface.DrawRect(0, 0, w, h)

			if (this.entered) then
				surface.SetDrawColor(0, 0, 0, 75)
				surface.DrawRect(0, 0, w, h)
			end
		end
		panel.button:Dock(BOTTOM)
		panel.button:SetTall(28)
		panel.button:DockMargin(2, 2, 2, 0)
		panel.button.OnCursorEntered = function(this)
			this.entered = true
		end
		panel.button.OnCursorExited = function(this)
			this.entered = nil
		end
		panel.button:SetText("Send Command")
		panel.button:SetTextColor(Color(255, 255, 255, 230))
		panel.button.DoClick = function()
			local args = {}
			for k,v in pairs(panel.scroll.args) do
				//print(v:GetClassName())
				if (v.players) then
					local targets = {}
					for k, v in pairs(NM.menus.selected) do
						table.insert(targets, k)
					end
					if(table.Count(targets) > 0) then
						table.insert(args, targets)
					end
				elseif (v.GetSelected) then
					local str, data = v:GetSelected()
					table.insert(args, str)
				elseif (v.GetChecked) then
					if (v:GetChecked()) then
						table.insert(args, "true")
					else
						table.insert(args, "false")
					end
				else
					table.insert(args, v:GetValue())
				end
			end
			//PrintTable(args)
			NM.commands.Parse(LocalPlayer(), {NM.menus.selectedCmd, unpack(args)})
		end

		panel.scroll = panel.contents:Add("DScrollPanel")
		panel.scroll:Dock(FILL)
		panel.scroll:SetWide(panel:GetWide() * 0.6 - 11)
		panel.scroll.args = {}

		self:ListCommands(panel.cmds, panel.scroll)

		return true
	end

	function CATEGORY:ListCommands(list, argScroll)
		if (!IsValid(list)) then
			return
		end

		list:Clear()

		for k,cmdTbl in SortedPairs(NM.commands.cats) do
			local cat = list:Add(k)
			cat:SetExpanded(false)
			cat.Paint = function(this, w, h)
				surface.SetDrawColor(NM.menus.color.r, NM.menus.color.g, NM.menus.color.b, 255)
				surface.DrawRect(0, 0, w, h)
			end
			cat:SetPaintBackground(false)

			for _,v in SortedPairsByMemberValue(cmdTbl, "cmd") do
				if (v.hidden or !LocalPlayer():NM_Permission_Has(v.permission)) then
					continue
				end

				local button = cat:Add("DButton")
				button:Dock(TOP)
				button:SetTall(28)
				button.cmd = v.cmd
				button.Paint = PaintButton
				button:DockMargin(2, 2, 2, 0)
				button.OnCursorEntered = function(this)
					this.entered = true
				end
				button.OnCursorExited = function(this)
					this.entered = nil
				end
				button:SetText(v.cmd)
				button:SetTextColor(Color(0, 0, 0, 230))
				button.DoClick = function(this)
					NM.menus.selectedCmd = this.cmd
					for __,arg in pairs(argScroll.args) do
						arg:Remove()
					end
					argScroll.args = {}
					argScroll:Clear()

					local item = argScroll:Add("DLabel")
					item:SetText(this.cmd:upper())
					item:Dock(TOP)
					item:DockMargin(4, 4, 4, 0)
					item:SetTextColor(Color(50,50,50,255))
					item:SetFont("Trebuchet24")

					for __,arg in pairs(v.args) do
						if (!arg[3].hidden) then // When a argument is hidden, means it probably has an opposite command that we won't use
							self:ArgumentLayout(argScroll, arg[4].name, arg[1], arg[3])
						end
					end
				end

				if (v.opposite) then
					local oppbutton = cat:Add("DButton")
					oppbutton:Dock(TOP)
					oppbutton:SetTall(28)
					oppbutton.cmd = v.opposite[1]
					oppbutton.Paint = PaintButton
					oppbutton:DockMargin(2, 2, 2, 0)
					oppbutton.OnCursorEntered = button.OnCursorEntered
					oppbutton.OnCursorExited = button.OnCursorExited
					oppbutton:SetText(v.opposite[1])
					oppbutton:SetTextColor(button:GetTextColor())
					oppbutton.DoClick = button.DoClick
				end
			end
		end
	end

	function CATEGORY:ListPlayers(scroll, players)
		if (!IsValid(scroll)) then
			return
		end

		NM.menus.players = {}

		scroll:Clear()
		players = players or player.GetAll()

		for k, v in SortedPairs(players) do
			local item = scroll:Add("nm_Player")
			item:Dock(TOP)
			item:SetPlayer(v)
			item.index = #NM.menus.players + 1

			NM.menus.players[item.index] = item
		end
	end

	function CATEGORY:ArgumentLayout(scroll, arg, title, params)
		if (arg == NM.commands.args.Player.name) then
			local item = scroll:Add("DScrollPanel")
			item.players = true
			item:SetHeight(200)
			item:SetWide(scroll:GetWide())
			item.Paint = PaintButton
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
			self:ListPlayers(item)
		elseif (arg == NM.commands.args.Players.name) then
			local item = scroll:Add("DScrollPanel")
			item.players = true
			item:SetHeight(200)
			item:SetWide(scroll:GetWide())
			item.Paint = PaintButton
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
			self:ListPlayers(item)
		elseif (arg == NM.commands.args.Rank.name) then
			local item = scroll:Add("DComboBox")
			item:SetValue("Rank")
			for k,v in pairs(NM.ranks.list) do
				item:AddChoice(k)
			end
			item:SetTextColor(Color(0,0,0))
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
		elseif (arg == NM.commands.args.Team.name) then
			local item = scroll:Add("DComboBox")
			item:SetValue("Team")
			for k,v in pairs(NM.commands.args.Team.autocomplete("")) do
				//print(v)
				item:AddChoice(v)
			end
			item:SetTextColor(Color(0,0,0))
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
		elseif (arg == NM.commands.args.Time.name) then
			local item = scroll:Add("DTextEntry")
			item:SetWide(scroll:GetWide())
			item:SetPlaceholderText(title)
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
		elseif (arg == NM.commands.args.Amount.name) then
			local item = scroll:Add("DNumberWang")
			if (params.min&&params.max) then
				item:SetMinMax(params.min, params.max)
			elseif (params.min) then
				item:SetMin(params.min)
			elseif (params.max) then
				item:SetMin(params.max)
			end
			if (params.default) then
				item:SetValue(params.default)
			end
			item:SetTextColor(Color(0,0,0))
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
		elseif (arg == NM.commands.args.Boolean.name) then
			local item = scroll:Add("DCheckBoxLabel")
			item:SetText(title)
			item:SetTextColor(Color(0,0,0))
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
		elseif (arg == NM.commands.args.String.name) then
			local item = scroll:Add("DTextEntry")
			item:SetWide(scroll:GetWide())
			item:SetPlaceholderText(title)
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
		elseif (arg == NM.commands.args.Text.name) then
			local item = scroll:Add("DTextEntry")
			item:SetWide(scroll:GetWide())
			item:SetPlaceholderText(title)
			item:Dock(TOP)
			item:DockMargin(4, 4, 4, 0)
			table.insert(scroll.args, item)
		elseif (arg == NM.commands.args.Json.name) then

		end
	end

NM.menus.list.commands = CATEGORY
