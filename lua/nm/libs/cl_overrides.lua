// RAWR!

// Overrides for other addons
NM.overrides = NM.overrides or {}

function NM.overrides.GetRank(ply)
	if ULib || maestro then
		return Ply:GetUserGroup() or "user"
	elseif evolve then
		return Ply:GetProperty("Rank", "user")
	elseif moderator then
		return moderator.GetGroup(Ply) or "user"
	elseif ASS_VERSION then
		return Ply:GetLevel() or "user"
	elseif serverguard then
		return serverguard.player:GetRank(Ply) or "user"
	elseif NM then // Yes, I know the following else will never run.
		return Ply:GetUserGroup() or "user"
	else
		if Ply:IsSuperAdmin() then
			return "superadmin"
		elseif Ply:IsAdmin() then
			return "admin"
		else
			return "user"
		end
	end
end

function NM.overrides.Initialize()
	// FAdmin
	if (FAdmin && (!ulx && !evolve && !moderator && !ASS_VERSION && !serverguard)) then
		NM.overrides.fadmin = NM.overrides.fadmin or {}
		NM.overrides.fadmin.PlayerHasPrivilege = NM.overrides.fadmin.PlayerHasPrivilege or FAdmin.Access.PlayerHasPrivilege
		FAdmin.Access.PlayerHasPrivilege = function(ply, priv, target, ignoreImmunity)
			if (!ply.NM_Permission_Has) then return end
			local priv = "fadmin."..string.Replace(priv:lower(), " ", ".")
			local result = ply:NM_Permission_Has(priv, false)
			if (result && IsValid(target) && !ignoreImmunity) then
				result = ply:NM_CanTarget(target)
				if (!result) then
					return false
				end
			end
			return result
		end
		NM.overrides.fadmin.OnUsergroupRegistered = NM.overrides.fadmin.OnUsergroupRegistered or FAdmin.Access.OnUsergroupRegistered
		FAdmin.Access.OnUsergroupRegistered = function(usergroup, source)
			if (source == CAMI.NM_TOKEN) then return end
			return NM.overrides.fadmin.OnUsergroupRegistered(usergroup, source)
		end
	end

	// blogs
	if (blogs) then
		function bLogs:IsMaxPermitted(ply)
			if (not ply and CLIENT) then ply = LocalPlayer() end
			for _,v in pairs(bLogs.Config.MaxPermitted) do
				if (v == ply:SteamID() or v == ply:SteamID64() or v == ply:Team() or ply:IsUserGroup(v)) then // changed to use IsUserGroup to support subgrouping
					permissions_debug("SteamID, SteamID64, job/team or usergroup is MaxPermitted.")
					return true
				elseif (type(v) == "function") then
					if (v(ply,to_what)) then
						permissions_debug("MaxPermitted function called and returned true.")
						return true
					end
				end
			end
			return false
		end
	end

	// Pointshop
	if (PS) then
		local Player = FindMetaTable("Player")

		function Player:PS_GetUsergroup()
			return NM.overrides.GetRank(self)
		end
	end

	// LibK
	if (PermissionInterface) then
		function PermissionInterface.query( ply, access )
			--ULX
			if ULib then
				return ULib.ucl.query( ply, access )
			end

			--Evolve
			if ply.EV_HasPrivilege then
				return ply:EV_HasPrivilege( access )
			end

			--Exsto
			if exsto then
				return ply:IsAllowed( access )
			end

			--Network Manager
			if (NM) then // including network manager to list
				return ply:NM_Permission_Has( access )
			end

			KLogf(4, "[KReport] No compatible admin mod detected. ULX, Evolve and Exsto are supported- Defaulting." )

			if ply:IsSuperAdmin() then
				return true
			end

			return false
		end

		function PermissionInterface.getRanks( )
			local ranks = { } // internalName: string, title: string

			if ULib then
				for internalName, rankInfo in pairs( ULib.ucl.groups ) do
					if v != ULib.ACCESS_ALL then
						table.insert( ranks, { internalName = internalName, title = internalName } )
					end
				end
				return ranks
			end

			if evolve then
				for internalName, rankInfo in pairs( evolve.ranks ) do
					table.insert( ranks, { internalName = internalName, title = rankInfo.Title } )
				end
				return ranks
			end

			if serverguard then
				for internalName, rankInfo in pairs( serverguard.ranks.stored ) do
					table.insert( ranks, { internalName = internalName, title = rankInfo.name } )
				end
				return ranks
			end

			if NM then // including network manager to list
				for k,v in pairs(NM.ranks.list) do
					table.insert( ranks, { internalName = k, title = v.displayName } )
				end
				return ranks
			end

			return ranks
		end
	end

	if (NM.hotfix && NM.hotfix.Overrides) then
		NM.hotfix.Overrides()
	end
end