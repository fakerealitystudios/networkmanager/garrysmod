// RAWR!

NM.api = NM.api or {}
NM.api.external.update = NM.api.external.update or false

function NM.api.external.ServerStat()
	if (!NM.config.Add("nm_statistics")) then return end

	/*NM.api.external.post("/serverstat", {
		port = NM.server.port,
		game = NM.game,
		gamemode = gmod.GetGamemode().FolderName,
		name = GetConVarString("hostname"),
		version = NM.version
	})*/
end

function NM.api.external.VersionCheck()
	NM.api.external.get("/core/version/garrysmod", function(content, size, headers, status)
		if (status == 200) then
			NM.siteVersion = util.JSONToTable(content)
			if (!NM.siteVersion) then
				return
			end
			NM.siteVersion = NM.siteVersion['version']
			if (NM.siteVersion != NM.version) then
				local site_version = string.Explode('.', NM.siteVersion)
				local local_version = string.Explode('.', NM.version)
				local update = true
				if (site_version[1] < local_version[1]) then update = false end
				if (site_version[1] == local_version[1] && site_version[2] < local_version[2]) then update = false end
				if (site_version[1] == local_version[1] && site_version[2] == local_version[2] && site_version[3] < local_version[3]) then update = false end
				NM.api.external.update = update
				if (update) then
					NM.dev.Log(3, "Version mismatch! New version available from your web panel!")
					timer.Create("NM_VersionCheck", 300, 0, function()
						for k,v in pairs(player.GetSuperAdmins()) do
							v:NM_SendChat(Color(100,255,100), "[NM-VC] ", Color(255,50,50), "Network manager has an update available! Check networkmanager.io")
							v:NM_SendChat(Color(100,255,100), "[NM-VC] ", Color(255,255,255), "Current Version: ", Color(150,150,150), NM.version, Color(255,255,255), " Site Version: ", Color(150,150,150), NM.siteVersion)
						end
					end)
				else
					NM.dev.Log(3, "Current version is newer then API's! Check your API version.")
				end
			else
				NM.dev.Log(7, "You have the current version of NetworkManager from the API.")
			end
		end
	end)
end