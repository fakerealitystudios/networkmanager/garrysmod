// RAWR!

NM.log = NM.log or {}
NM.log.queue = NM.log.queue or {}

NM.log.LogType = {}
NM.log.LogType.SERVER = "SERVER"
NM.log.LogType.PLAYER = "PLAYER"

NM.log.DetailType = {}
NM.log.DetailType.NMID = "NMID"
NM.log.DetailType.USID = "USID"
NM.log.DetailType.TOKENID = "TOKENID"
NM.log.DetailType.ENTITY = "ENTITY"
NM.log.DetailType.NPC = "NPC"
NM.log.DetailType.WEAPON = "WEAPON"
NM.log.DetailType.WORLD = "WORLD"
NM.log.DetailType.NULL = "NULL"
NM.log.DetailType.TABLE = "TABLE"
NM.log.DetailType.STRING = "STRING"
NM.log.DetailType.INTEGER = "INTEGER"
NM.log.DetailType.FLOAT = "FLOAT"

NM.log.DamageType = {}
NM.log.DamageType.EXPLOSION = "EXPLOSION"
NM.log.DamageType.SHOT = "SHOT"
NM.log.DamageType.FALL = "FALL"
NM.log.DamageType.CRUSHED = "CRUSHED"
NM.log.DamageType.SLASHED = "SLASHED"
NM.log.DamageType.BURNED = "BURNED"
NM.log.DamageType.HIT = "HIT"
NM.log.DamageType.SHOCKED = "SHOCKED"
NM.log.DamageType.DROWN = "DROWN"
NM.log.DamageType.UNKNOWN = "UNKNOWN"
NM.log.DamageType.POISON = "POISON"
NM.log.DamageType.MAGIC = "MAGIC"
NM.log.DamageType.TRIGGER = "TRIGGER"
NM.log.DamageType.VEHICLE = "VEHICLE"

function NM.log.Initialize()
	NM.util.IncludeInternalDir("logs")
end

function NM.log.GMInitialize()
	// Load log types here(So that they have the gamemode, makes life easier for some)
	NM.util.IncludeInternalDir("logs/addons") // The log types should reject loading if their addon isn't loaded
	NM.util.IncludeInternalDir("logs/gamemodes") // Generic gamemode logs
	NM.util.IncludeInternalDir("logs/gamemodes/"..gmod.GetGamemode().FolderName)
end

function NM.log.Hook(logType, event, hookName, callback)
	local log = {}
	log['Log'] = function(...)
		NM.log.Log(logType, event, nil, table.pack(...))
	end
	log['LogSub'] = function(subevent, ...)
		NM.log.Log(logType, event, subevent, table.pack(...))
	end
	hook.Add(hookName, "NM_Logs_"..logType.."_"..event, function(...)
		callback(log, ...)
	end, HOOK_MONITOR_HIGH)
end

local function GetDamageCause(cause)
	if (cause == DMG_BULLET) then
		return NM.log.DamageType.SHOT
	elseif (cause == DMG_BUCKSHOT) then
		return NM.log.DamageType.SHOT
	elseif (cause == DMG_FALL) then
		return NM.log.DamageType.FALL
	elseif (cause == DMG_BLAST) then
		return NM.log.DamageType.EXPLOSION
	elseif (cause == DMG_CRUSH) then
		return NM.log.DamageType.CRUSHED
	elseif (cause == DMG_SLASH) then
		return NM.log.DamageType.SLASHED
	elseif (cause == DMG_BURN || cause == DMG_SLOWBURN) then
		return NM.log.DamageType.BURNED
	elseif (cause == DMG_CLUB) then
		return NM.log.DamageType.HIT
	elseif (cause == DMG_SHOCK) then
		return NM.log.DamageType.SHOCKED
	elseif (cause == DMG_DROWN) then
		return NM.log.DamageType.DROWN
	elseif (cause == DMG_SONIC) then
		return NM.log.DamageType.EXPLOSION
	elseif (cause == DMG_ENERGYBEAM) then
		return NM.log.DamageType.SHOCKED
	elseif (cause == DMG_NERVEGAS) then
		return NM.log.DamageType.POISON
	elseif (cause == DMG_POISON || cause == DMG_PARALYZE) then
		return NM.log.DamageType.POISON
	elseif (cause == DMG_ACID) then
		return NM.log.DamageType.POISON
	elseif (cause == DMG_DISSOLVE) then
		return NM.log.DamageType.MAGIC
	elseif (cause == DMG_RADIATION) then
		return NM.log.DamageType.POISON
	elseif (cause == DMG_PLASMA) then
		return NM.log.DamageType.SHOCKED
	elseif (cause == DMG_VEHICLE) then
		return NM.log.DamageType.VEHICLE
	end
	return NM.log.DamageType.UNKNOWN
end

function NM.log.GetDamageCause(cause, victim, inflictor, attacker)
	return {
		_type = "DamageCause",
		cause = GetDamageCause(cause),
		victim = victim,
		inflictor = inflictor,
		attacker = attacker
	}
end

function NM.log.FormatDetail(thing)
	local tbl = {}

	if (tonumber(thing)) then
		table.insert(tbl, {NM.log.DetailType.INTEGER, thing})
	elseif (type(thing) == "string") then
		table.insert(tbl, {NM.log.DetailType.STRING, thing})
	elseif (istable(thing) && thing._type && thing._type == "DamageCause") then
		table.insert(tbl, {NM.log.DetailType.STRING, thing.cause})
		if (IsValid(thing.inflictor)) then
			local detailType = NM.log.DetailType.ENTITY
			if (thing.inflictor:IsWorld()) then
				detailType = NM.log.DetailType.WORLD
			elseif (thing.inflictor:IsWeapon()) then
				detailType = NM.log.DetailType.WEAPON
			end
			table.insert(tbl, {detailType, thing.inflictor:GetClass()})
		end
	elseif (istable(thing)) then
		table.insert(tbl, {NM.log.DetailType.TABLE, util.TableToJSON(thing)})
	elseif (thing.IsWorld && thing:IsWorld()) then // Just in case
		table.insert(tbl, {NM.log.DetailType.WORLD, "world"})
	elseif (thing.IsValid && IsValid(thing)) then // Valid entities can get special treatment
		if (thing:IsPlayer()) then
			local val = thing:NM_GetId()
			if (val == -1 && NM.players.map[thing:SteamID64()]) then
				val = NM.players.map[thing:SteamID64()]
			end
			table.insert(tbl, {NM.log.DetailType.NMID, val})
			if (thing:NM_GetSessionId() != -1) then
				table.insert(tbl, {NM.log.DetailType.USID, thing:NM_GetSessionId()})
			end
			table.insert(tbl, {NM.log.DetailType.STRING, thing:GetName(), "name"})
		elseif (thing:IsNPC()) then
			table.insert(tbl, {NM.log.DetailType.NPC, thing:GetClass()})
			if (IsValid(thing:GetActiveWeapon())) then
				table.insert(tbl, {NM.log.DetailType.WEAPON, thing:GetActiveWeapon():GetClass()})
			end
		elseif (thing:IsWeapon()) then
			table.insert(tbl, {NM.log.DetailType.WEAPON, thing:GetClass()})
		elseif (thing:IsVehicle()) then
			table.insert(tbl, {NM.log.DetailType.VEHICLE, thing:GetClass()})
		else
			table.insert(tbl, {NM.log.DetailType.ENTITY, thing:GetClass()})
		end

		if (thing.NM_Creator && IsValid(thing.NM_Creator)) then
			table.Add(tbl, NM.log.FormatDetail(thing.NM_Creator))
		elseif (thing.NM_Creator_Id && thing.NM_Creator_Id != -1) then
			table.insert(tbl, {NM.log.DetailType.NMID, thing.NM_Creator_Id})
			table.insert(tbl, {NM.log.DetailType.USID, thing.NM_Creator_SessionId})
		end
	end

	return tbl
end

function NM.log.Log(logType, event, subevent, details)
	if (!logType || !event) then
		NM.dev.Log(2, "Didn't receive logType or event info!")
	end
	if (details != nil) then
		local tbl = {}
		for k,v in pairs(details) do
			table.insert(tbl, NM.log.FormatDetail(v))
		end
		details = tbl
	end

    local logDetails = {}
    if (details) then
        for n,d in pairs(details) do
            for _,d2 in pairs(d) do
                table.insert(logDetails, {
                    ["type"] = d2[1],
                    ["argid"] = n-1, // we start at 0, even though LUA is a fucktard
                    ["value"] = d2[2],
                    ["vid"] = d2[3] or nil
                })
            end
        end
    end

    local db_values = {}
    db_values.type = logType
    db_values.event = event
    db_values.subevent = subevent
    db_values.time = os.time()
    db_values.logDetails = util.TableToJSON(logDetails)

	if (!NM.server || NM.server.ready == false || NM.server.sessionid == -1) then
		table.insert(NM.log.queue, db_values)
		return -- Can't log when we don't have the server id yet(this SHOULD only be an issue when the server is started with NM for the first time)
	end
	if (table.Count(NM.log.queue) > 0) then
		for k,v in pairs(NM.log.queue) do
			NM.api.put("/server/"..NM.GetId().."/log", v)
		end
		table.Empty(NM.log.queue)
	end

    NM.api.put("/server/"..NM.GetId().."/log", db_values)
end