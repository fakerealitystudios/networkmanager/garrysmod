// RAWR!

NM.server = NM.server or {}
NM.server.fileHandle = "data/server_cache.txt"
NM.server.finished = NM.server.finished or false
NM.server.sessionid = NM.server.sessionid or -1
NM.server.ip = NM.server.ip or game.GetIP() or "client"
NM.server.port = NM.server.port or GetConVarNumber("hostport")
NM.server.address = NM.server.address or NM.server.ip..":"..NM.server.port
NM.server.gamemode = NM.server.gamemode or ""
NM.server.settings = NM.server.settings or NM.data.GetData(NM.server.fileHandle, {}, "settings")
NM.server.ready = NM.server.ready or false
// Secret is only valid once per instance. Valid during reloads, and re-generated on map change
// This is only ever sent directly to the database. Anything that should be connecting will have access to it there
// The secret will NEVER be sent to api.networkmanager.center
NM.server.secret = NM.server.secret or NM.util.GenPassword()

NM.server.id = NM.data.GetData(NM.server.fileHandle, -1, "server_id")

function NM.server.index()
	// Just for public, global statistics
	if (NM.config.Get("nm_statistics")) then
		NM.api.external.ServerStat()
	end

    NM.api.put("/server/"..NM.GetId(), {
        address = NM.server.address,
        name = GetConVarString("hostname"),
        game = NM.game,
        gamemode = NM.server.gamemode,
    }, nil, function(tbl)
        if (tbl.server.id) then
            // Need to replace our current id + authentication info
            NM.config.Set("api_authentication", {serverid = tbl.server.id, secret = tbl.server.token}, true, true)
        else
            NM.config.Set("api_authentication", {serverid = NM.GetId(), secret = tbl.server.token}, true, true)
        end
        NM.data.SetData(NM.server.fileHandle, "server_id", NM.GetId())
        for k,v in pairs(tbl.server.settings) do
            local value = v.value
            if (isstring(v.value)) then
                local fc = string.sub(v.value, 0, 1)
                local ec = string.sub(v.value, -1)
                if ((fc == "[" || fc == "{") && (ec == "}" || ec == "]")) then
                    value = util.JSONToTable(v.value)
                end
            end
            NM.server.settings[v.name] = value
        end
        local ver = NM.server.settings["version"]
        if (NM.nm_version != ver) then
            // Version mismatch! We should kill NM!
        end
        NM.server.ready = true
        hook.Run("NM_Server.Ready")

        if (!NM.socket) then
            // MySQL actions rather than socket based
            NM.api.put("/server/"..NM.GetId().."/communication", {communication = 1})
        end

        NM.api.put("/server/"..NM.GetId().."/session", {}, nil, function(tbl)
            NM.server.sessionid = tbl.ssid
            hook.Run("NM_Server.SessionIndexed")
        end)
    end)
end

function NM.server.OnServerReady()
    NM.server.PrepareServerSettings()
	// We automatically start polling, the timer is killed once the socket receives a valid payload
	timer.Remove("NM_ServerActions")
	timer.Create("NM_ServerActions", NM.config.Get("server_action_poll"), 0, function()
		NM.api.get("/server/"..NM.GetId().."/actions", nil, function(tbl)
            for k,v in pairs(tbl.actions) do
                NM.server.Payload(v.payload)
            end
        end)
	end)
end

function NM.server.PrepareServerSettings()
    // Doubt this is needed, but, just in case
    local function recursiveData(tbl)
        for k,v in pairs(tbl) do
            if (v == "true") then
                tbl[k] = true
            elseif (v == "false") then
                tbl[k] = false
            elseif (tonumber(v)) then
                tbl[k] = tonumber(v)
            elseif (istable(v)) then
                recursiveData(v)
            end
        end
    end
    recursiveData(NM.server.settings)

    // Create server default settings
	if (NM.server.GetSetting("whitelisted", nil) == nil) then
		NM.server.SetSetting("whitelisted", false)
    elseif (NM.server.GetSetting("whitelisted", false)) then
        local pass = NM.util.GenPassword()
        RunConsoleCommand("sv_password",pass)
        MsgN("Changing the server's Password to: \""..pass.."\"")
    end

    if (NM.server.GetSetting("advertisements", nil) == nil) then
		NM.server.SetSetting("advertisements", {
			{
				parts = {
					{
						text = "[NM] ",
						color = {255,0,0}
					},
					{
						text = "You can set your server advertisements on the NetworkManager web panel!",
						color = {255,255,255}
					}
				},
				enabled = true,
				interval = 1
			}
		})
    end
    if (NM.server.GetSetting("http_whitelist_enabled", nil) == nil) then
		NM.server.SetSetting("http_whitelist_enabled", true)
    end
    if (NM.server.GetSetting("http_whitelist", nil) == nil) then
		NM.server.SetSetting("http_whitelist", {
			"steamcommunity.com",
			"api.steampowered.com",

			// Network Manager
			"api.networkmanager.center",

			// Randoms
			"pastebin.com/raw/Pt0UWBHg", // simplac and other of his addons have a script to kick out people who leak his stuff. Up to you to keep it
			"jross.me", // Sammyservers text screens analytics
			"raw.githubusercontent.com/TeamUlysses", // ULX/ULib checks for versions
			"raw.githubusercontent.com/syl0r/MySQLOO", // MySQLOO checks for versions
			"raw.github.com/adamdburton/pointshop", // pointshop checks for versions
			"google.com", // ULib uses google to check if there is a connection(I guess? https://github.com/TeamUlysses/ulib/blob/master/lua/ulib/shared/plugin.lua#L186)
			"www.googleapis.com", // for google apis(wdj)
			"vcmod.org", // VCMod
			// blogs
			"lib.venner.io",
			"steamid.venner.io",
			// gmod DRM
			"cdn.rawgit.com/WilliamVenner/libgmodstore/master/libgmodstore.lua",
			"lib.gmodsto.re", // gmodstore url shortener
			"95.85.30.168:9000", // is cyan.wyozi.xyz
			"cyan.wyozi.xyz",
			"www.badassdevelopment.team",
			"drm.originahl-scripts.com",
			"securegmod.com:443",
			"xeon.gmodsto.re",
			"api.iamrichardt.com",

            "sentry.frs.llc", // Global NetworkManager error logging
		})
    end

    // Cache settings
    NM.data.SetData(NM.server.fileHandle, "settings", NM.server.settings)
end

function NM.server.OnShutDown()
	NM.api.put("/server/"..NM.GetId().."/shutdown")
end

NM.server.old_print = NM.server.old_print or print
function NM.server.print(...)
	local args = table.pack(...)
	NM.log.Log(NM.log.LogType.SERVER, "print", nil, {table.concat(args, " ")})
	NM.server.old_print(...)
end

function NM.server.Payload(tbl)
	NM.dev.Log(5, "{Payload} Received new payload from web service")
	NM.log.Log(NM.log.LogType.SERVER, "payload", nil, {tbl})
	if (tbl.action == "lua") then
		if (!pcall(RunString, tbl.lua)) then
			NM.dev.Log(2, "{Payload} Invalid LUA payload")
		end
	elseif (tbl.action == "command") then
		print = NM.server.print // Change print so that way we can log responses
		if (tbl.cmd[1] == "quit" || tbl.cmd[1] == "_restart") then // a little hack for the quotes
			// We prevent you from quitting/restart via cmd(just a safety net)
		else
			game.ConsoleCommand(tbl.cmd.."\n")
		end
		timer.Simple(1, function()
			print = NM.server.old_print
		end)
	elseif (tbl.action == "kick") then
		local ply = NM.players.GetByNMID(tbl.nmid)
		ply:Kick(tbl.reason)
	elseif (tbl.action == "ban") then
		NM.bans.Load()
		ply:Kick(tbl.reason)
	elseif (tbl.action == "giveRank") then
		local ply = NM.players.GetByNMID(tbl.nmid)
		if (IsValid(ply)) then
			local rankTbl = NM.ranks.Get(tbl.rank)
			table.insert(ply.nm.rankdata, {rank = tbl.rank, subgrouping = rankTbl.subgrouping, realm = tbl.realm, id = tbl.id})
			local rank_rows = table.Copy(ply.nm.rankdata)
			ply:NM_Rank_Load(rank_rows)
		end
	elseif (tbl.action == "takeRank") then
		local ply = NM.players.GetByNMID(tbl.nmid)
		if (IsValid(ply)) then
			for k,v in pairs(ply.nm.rankdata) do
				if (v.id == tbl.id) then
					table.remove(ply.nm.rankdata, k)
					break
				end
			end
			local rank_rows = table.Copy(ply.nm.rankdata)
			ply:NM_Rank_Load(rank_rows)
		end
	elseif (tbl.action == "givePermission" || tbl.action == "takePermission") then
		local ply = NM.players.GetByNMID(tbl.nmid)
		if (IsValid(ply)) then
			
		end
	elseif (tbl.action == "createRank" || tbl.action == "destroyRank" || tbl.action == "giveRankPermissions" || tbl.action == "takeRankPermissions") then
		NM.ranks.Load()
	else
		NM.dev.Log(2, "{Payload} Unknown payload type")
	end
end

function NM.server.GMInitialize()
	NM.server.gamemode = string.lower(gmod.GetGamemode().FolderName)
    // Use a delay, because when the server first starts the HTTPClient isn't available cause fuck you
	timer.Simple(1, NM.server.index)
end

function NM.server.Initialize()
	if (NM.server.finished) then
		return // already have/getting IP
	end
	NM.api.external.get("/core/ip", function(content, length, headers, code)
		NM.server.ip = util.JSONToTable(content)["ip"]
		NM.server.address = NM.server.ip..":"..NM.server.port

		NM.server.finished = true
	end, function(err)
		NM.server.ip = game.GetIP()
		NM.server.address = NM.server.ip..":"..NM.server.port

		NM.server.finished = true
	end)
end

function NM.server.GetIP()
	return NM.server.ip
end

function NM.server.GetAddress()
	return NM.server.address
end

function NM.server.SetSetting(index, value)
	NM.server.settings[index] = value
    if (istable(value)) then
        value = util.TableToJSON(value)
    end
	NM.api.put("/server/"..NM.GetId().."/setting/"..index, {value = value})
end

function NM.server.GetSetting(index, default)
    if (NM.server.settings[index] == nil) then
        return default
    end
	return NM.server.settings[index]
end
