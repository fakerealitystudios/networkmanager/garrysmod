// RAWR!

NM.bans.fileHandle = "administration/bans.txt"

function NM.bans.NetworkPlayer(ply)
	return NM.bans.list
end

function NM.bans.NetworkBroadcast(ply)
	if (ply) then return end
	return NM.bans.list
end

function NM.bans.OnServerReady()
	NM.bans.Load()
end

function NM.bans.Load()
	// Just incase the database isn't up, we store bans locally
	local bans = NM.data.GetData(NM.bans.fileHandle, {})

	if (bans) then
		for k, v in pairs(bans) do
			if (v.length > 0 and v.unbanTime <= os.time()) then
				bans[k] = nil
			end
		end

		NM.bans.list = bans
	end

    NM.api.get("/server/"..NM.GetId().."/bans", nil, function(tbl)
        NM.bans.list = {}
        for k,v in pairs(tbl.bans) do
            v.banTime = tonumber(v.banTime)
            v.unbanTime = tonumber(v.unbanTime)
            v.length = tonumber(v.length)
            NM.players.GetSteamId(v.uid, function(steam)
                if (steam == -1) then return end // wtf?
                local banid = table.insert(NM.bans.list, v)
                NM.bans.list[banid].steam = steam
                NM.players.external.GetPlayer(steam, function() end) // Cache them
                NM.players.GetSteamId(v.adminuid, function(admin_steam)
                    if (admin_steam == -1) then admin_steam = 1 end // Default to Console
                    NM.bans.list[banid].admin_steam = admin_steam
                    NM.players.external.GetPlayer(admin_steam, function() end) // Cache them
                end)
            end)
        end
        timer.Simple(5, function()
            NM.bans.Save() // store locally
            NM.dev.Log(5, "Loaded "..table.Count(NM.bans.list).." 'bans' from database")
        end)
    end)
end

function NM.bans.Save()
	for k, v in pairs(NM.bans.list) do
		if (v.length > 0 and v.unbanTime <= os.time()) then
			NM.bans.list[k] = nil
		end
		NM.players.external.GetPlayer(v.uid)
	end

	NM.net.BroadcastLib("bans")

	NM.data.SetData(NM.bans.fileHandle, nil, NM.bans.list)
end

function NM.bans.Ban(steam, name, length, reason, admin_steam, realm)
	if (!steam||!length) then return end
	if (!realm) then
		realm = NM.realm
	end

	local steamid = util.SteamIDFrom64(steam)
	local name = ""
	local ply = player.GetBySteamID(steamid)
	if (ply) then
		name = ply:Name()
	end

	local unbanTime = os.time() + length
	local reason = reason

	NM.players.GetNMId(steam, function(nmid)
		if (nmid == -1) then return end
		local banid = table.insert(NM.bans.list, {
			uid = nmid,
			steam = steam,
			admin_steam = admin_steam,
			banTime = os.time(),
			unbanTime = unbanTime,
			length = length,
			reason = reason,
		})
        local pl, usid = NM.players.GetByNMID(nmid)
        if (pl) then
            usid = pl:NM_GetSessionId()
        end

		NM.players.GetNMId(admin_steam, function(admin_nmid)
			if (admin_nmid == -1) then admin_nmid = 1 end
			NM.bans.list[banid].adminuid = admin_nmid
            local pl, admin_usid = NM.players.GetByNMID(nmid)
            if (pl) then
                admin_usid = pl:NM_GetSessionId()
            end
            NM.api.put("/member/"..nmid.."/ban", {
                usid = usid,
                length = length,
                reason = reason,
                admin_nmid = admin_nmid,
                admin_usid = admin_usid,
            }, realm)
		end)
	end)
end

function NM.bans.Unban(steam, delete)
	for k,v in pairs(NM.bans.list) do
		if (v.steam == steam) then
            if (!delete) then
                local length = os.time() - v.banTime
                v.length = length
                v.unbanTime = os.time()
                NM.api.put("/member/"..v.uid.."/ban/"..v.id, {
                    length = length
                })
            else
                table.remove(NM.bans.list, k)
                NM.api.delete("/member/"..v.uid.."/ban/"..v.id)
            end
		end
	end

	NM.bans.Save()
end

function NM.bans.IsBanned(steam)
	local steamid = util.SteamIDFrom64(steam)
	if (ULib&&ULib.bans&&ULib.bans[steamid]) then
		return true // We try to play nice with ULX bans, just incase
	end
	for k,v in pairs(NM.bans.list) do
		if (v.steam == steam&&(v.length == 0||v.unbanTime >= os.time())) then
			return true
		end
	end
	return false
end

function NM.bans.GetReason(steam)
	local reason = "Banned from server"
	local steamid = util.SteamIDFrom64(steam)
	if (ULib&&ULib.bans&&ULib.bans[steamid]) then
		reason = ULib.bans[steamid].reason
	end
	for k,v in pairs(NM.bans.list) do
		if (v.steam == steam&&(v.length == 0||v.unbanTime >= os.time())) then
			reason = v.reason
			break
		end
	end
	reason = reason..NM.config.Get("administration_banSuffix", "")
	return reason
end
