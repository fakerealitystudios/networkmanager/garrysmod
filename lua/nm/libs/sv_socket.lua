// RAWR!

if (true) then return false end

if (!pcall(require, "bromsock")) then
	return false
end

NM.socket = NM.socket or {}
NM.socket.Server = NM.socket.Server or BromSock()
NM.socket.ApiSecret = NM.socket.ApiSecret or NM.util.GenPassword() // Secret only used with the API, to ensure security for test

// Don't start to listen until we've confirmed our sevrer index
function NM.socket.OnServerReady()
	// Set the port to be equal to the server port, except in the 250 range instead of 270 range
	if (!NM.socket.Server:Listen(NM.server.port-2000)) then
		NM.dev.Log(3, "Failed to open socket on port "..(NM.server.port-2000))
	else
		NM.dev.Log(5, "Socket opened on port "..(NM.server.port-2000))
		NM.api.external.post("/sockettest", {
			port = (NM.server.port-2000),
			apisecret = NM.socket.ApiSecret
		}, function(content, length, headers, code)
			local tbl = util.JSONToTable(content)
			if (tbl && tbl.state == "error") then
				NM.dev.Log(2, "{Socket} "..tbl.error)
				NM.socket.Server:Close()
				NM.socket = nil
                NM.api.put("/server/"..NM.GetId().."/communication", {communication = 1})
			elseif (tbl && tbl.state == "success") then
                NM.api.put("/server/"..NM.GetId().."/communication", {communication = 2})
				if (timer.Exists("NM_ServerActions")) then
					timer.Remove("NM_ServerActions") // We've received a connection here, we know it works, don't worry about polling mysql
				end
			end
		end)
	end
end

function NM.socket.OnShutDown()
	if (NM.socket) then
		NM.socket.Server:Close()
	end
end

NM.socket.Server:SetCallbackAccept(function(ServerSock, ClientSock)
	ClientSock:SetCallbackReceive(function(Sock, Packet)
		local ip = Sock:GetIP()

		local json = string.TrimRight(Packet:ReadStringAll(), "\1")

		NM.dev.Log(7, "{Socket} Received confirmed payload from "..ip.." Containing: "..json)

		Sock:Disconnect()

		NM.socket.Receive(json, ip)
	end)
	local IP = ClientSock:GetIP()
	ClientSock:SetTimeout(2000)
	ClientSock:ReceiveUntil("\1")
	ServerSock:Accept()
end)
NM.socket.Server:Accept()

function NM.socket.Receive(json, ip)
	local tbl = util.JSONToTable(json)

	if (timer.Exists("NM_ServerActions")) then
		timer.Remove("NM_ServerActions") // We've received a connection here, we know it works, don't worry about polling mysql
	end

	if (!istable(tbl)) then
		NM.log.Log(NM.log.LogType.SERVER, "socket", "failed", {ip, json, "json"})
		NM.dev.Log(2, "{Socket} Received a payload that wasn't proper JSON from ip: "..ip)
		return
	end

	if (tbl.apisecret && tbl.apisecret == NM.socket.ApiSecret) then
		return // Doesn't need to do anything, just don't want to log it
	end

	if (!tbl.skey || tbl.skey != NM.config.Get("api_authentication").secret) then
		NM.log.Log(NM.log.LogType.SERVER, "socket", "failed", {ip, json, "skey"})
		NM.dev.Log(2, "{Socket} Received a payload that has an incorrect security key, possible attempt to hack, IP: "..ip)
		return
	end
	tbl.skey = nil // don't send it forth, just in case

	NM.log.Log(NM.log.LogType.SERVER, "socket", nil, {ip, json})

	NM.server.Payload(tbl)
end
