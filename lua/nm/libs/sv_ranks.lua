// RAWR!

// Setup default ranks
NM.ranks.list = NM.ranks.list or {
	["user"] = {
		rank = "user",
		displayName = "User",
		inherit = nil,
		subgrouping = 0,
		realm = "*",
	}
}

function NM.ranks.NetworkPlayer(ply)
	return NM.ranks.list
end

function NM.ranks.NetworkBroadcast(ply)
	if (ply) then return end
	return NM.ranks.list
end

function NM.ranks.OnServerReady()
	NM.ranks.Load()

	if (!Prometheus) then return end // If we have prometheus, add support for us

end

function NM.ranks.Load()
    NM.api.get("/server/"..NM.GetId().."/ranks", nil, function(tbl)
        NM.ranks.list = {}
        NM.ranks.children = {}
        NM.ranks.parents = {}

        for k,v in pairs(tbl.ranks) do
            NM.ranks.list[v.rank] = {
                rank = v.rank,
                displayName = v.displayName,
                inherit = v.inherit,
                subgrouping = v.subgrouping,
                realm = v.realm,
                realms = {}
            }

            for k2,v2 in pairs(v.permissions) do
                local realm = NM.realms.get(NM.ranks.list[v.rank].realms, v2.realm).__perms
                table.insert(realm, v2.perm)
            end

            if CAMI.GetUsergroup(v.rank) then continue end

            CAMI.RegisterUsergroup({
                Name = v.rank,
                Inherits = NM.ranks.list[v.rank].inherit
            }, CAMI.NM_TOKEN)
        end

        NM.net.BroadcastLib("ranks")
        NM.dev.Log(5, "Loaded 'ranks' from database")
    end)
end
