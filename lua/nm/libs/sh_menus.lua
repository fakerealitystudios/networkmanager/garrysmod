// RAWR!

NM.menus = NM.menus or {}
NM.menus.list = NM.menus.list or {}
NM.menus.version = "0.1.0"

if (CLIENT) then
	NM.menus.colors = {
		red = Color(192, 57, 43),
		orange = Color(211, 84, 0),
		yellow = Color(241, 196, 15),
		green = Color(39, 174, 96),
		blue = Color(41, 128, 185),
		purple = Color(142, 68, 173),
		dark = Color(43, 45, 50),
		light =  Color(189, 195, 199)
	}

	NM_MAINCOLOR = CreateClientConVar("nm_color", "dark", true)

	cvars.AddChangeCallback("nm_color", function(conVar, previous, value)
		local value = NM.menus.colors[NM_MAINCOLOR:GetString()]

		if (value) then
			NM.menus.color = value
		end
	end)

	NM.menus.color = NM.menus.colors.dark

	local value = NM.menus.colors[NM_MAINCOLOR:GetString()]

	if (value) then
		NM.menus.color = value
	end
elseif (SERVER) then
	resource.AddFile("materials/moderator/leave.png")
	resource.AddFile("materials/moderator/menu.png")
end

function NM.menus.Initialize()
	NM.util.IncludeInternalDir("menus")
end
