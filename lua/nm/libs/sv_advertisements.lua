// RAWR!

NM.advertisements = NM.advertisements or {}
NM.advertisements.list = NM.advertisements.list or {}

function NM.advertisements.OnServerReady()
	timer.Create("NM_Advertisements", 60, 0, NM.advertisements.Perform)
end

function NM.advertisements.Perform()
	for k,v in pairs(NM.server.GetSetting("advertisements")) do
		if (v.enabled && (!NM.advertisements.list[k] || CurTime() >= NM.advertisements.list[k])) then
			NM.advertisements.list[k] = CurTime() + (v.interval * 60) - 1 // Subtract 1 to make sure it runs on time in the timer
			local text = {}
			for kk,vv in pairs(v.parts) do
				table.insert(text, Color(table.unpack(vv.color)))
				table.insert(text, vv.text)
			end
			player.BroadcastChat(table.unpack(text))
		end
	end
end