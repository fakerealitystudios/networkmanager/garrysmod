// RAWR!

// Overrides for other addons
NM.overrides = NM.overrides or {}

function NM.overrides.GetRank(ply)
	if ULib || maestro then
		return ply:GetUserGroup() or "user"
	elseif evolve then
		return ply:GetProperty("Rank", "user")
	elseif moderator then
		return moderator.GetGroup(ply) or "user"
	elseif ASS_VERSION then
		return ply:GetLevel() or "user"
	elseif serverguard then
		return serverguard.player:GetRank(ply) or "user"
	elseif NM then // Yes, I know the following else will never run.
		return ply:GetUserGroup() or "user"
	else
		if ply:IsSuperAdmin() then
			return "superadmin"
		elseif ply:IsAdmin() then
			return "admin"
		else
			return "user"
		end
	end
end

function NM.overrides.Initialize()
    // PermaProps
    if (PermaProps) then
        function PermaProps.HasPermission(ply, name)
            if (!ply.NM_Permission_Has) then return false end
            return ply:NM_Permission_Has("permaprops."..name)
        end
    end

    if (OpenPermissions) then
        // Why the hell does this exist? CAMI already fills it's job, significantly better
        function OpenPermissions:IsUserGroup(ply, usergroup)
            local ranks = {}
            for k,v in pairs(ply:NM_Rank_GetRanks()) do
                ranks[v] = true
            end
            return ranks
        end
        function OpenPermissions:GetUserGroups(ply)
            return ply:NM_Rank_GetRank()
        end
        function OpenPermissions:IsOperator(ply)
            return ply:NM_Permission_Has("openpermissions.operator")
        end
        function OpenPermissions:HasPermission(ply, permission_id)
            return ply:NM_Permission_Has(permission_id)
        end
    end

	// FAdmin
	if (FAdmin) then
		function FAdmin.Log() end // Fuck your logging mate
	end
	if (FAdmin && (!ulx && !evolve && !moderator && !ASS_VERSION && !serverguard)) then
		NM.overrides.fadmin = NM.overrides.fadmin or {}
		NM.overrides.fadmin.PlayerHasPrivilege = NM.overrides.fadmin.PlayerHasPrivilege or FAdmin.Access.PlayerHasPrivilege
		FAdmin.Access.PlayerHasPrivilege = function(ply, priv, target, ignoreImmunity)
			if (!ply.NM_Permission_Has) then return end
			local priv = "fadmin."..string.Replace(priv:lower(), " ", ".")
			local result = ply:NM_Permission_Has(priv, false)
			if (result && IsValid(target) && !ignoreImmunity) then
				result = ply:NM_CanTarget(target)
				if (!result) then
					return false
				end
			end
			return result
		end
		NM.overrides.fadmin.OnUsergroupRegistered = NM.overrides.fadmin.OnUsergroupRegistered or FAdmin.Access.OnUsergroupRegistered
		FAdmin.Access.OnUsergroupRegistered = function(usergroup, source)
			if (source == CAMI.NM_TOKEN) then return end
			return NM.overrides.fadmin.OnUsergroupRegistered(usergroup, source)
		end
	end

	// blogs
	if (blogs) then
		local function permissions_debug(str)
			if (GetConVar("blogs_permissions_debug")) then
				if (GetConVar("blogs_permissions_debug"):GetBool() == true) then
					bLogs:print(str)
				end
			end
		end

		function bLogs:IsMaxPermitted(ply)
			if (not ply and CLIENT) then ply = LocalPlayer() end
			for _,v in pairs(bLogs.Config.MaxPermitted) do
				if (v == ply:SteamID() or v == ply:SteamID64() or v == ply:Team() or ply:IsUserGroup(v)) then // changed to use IsUserGroup to support subgrouping
					permissions_debug("SteamID, SteamID64, job/team or usergroup is MaxPermitted.")
					return true
				elseif (type(v) == "function") then
					if (v(ply,to_what)) then
						permissions_debug("MaxPermitted function called and returned true.")
						return true
					end
				end
			end
			return false
		end
	end

	// Pointshop
	if (PS) then
		local Player = FindMetaTable("Player")

		function Player:PS_GetUsergroup()
			return NM.overrides.GetRank(self)
		end
	end

	// LibK
	if (PermissionInterface) then
		function PermissionInterface.query( ply, access )
			--ULX
			if ULib then
				return ULib.ucl.query( ply, access )
			end

			--Evolve
			if ply.EV_HasPrivilege then
				return ply:EV_HasPrivilege( access )
			end

			--Exsto
			if exsto then
				return ply:IsAllowed( access )
			end

			--Network Manager
			if (NM) then // including network manager to list
				return ply:NM_Permission_Has( access )
			end

			KLogf(4, "[KReport] No compatible admin mod detected. ULX, Evolve and Exsto are supported- Defaulting." )

			if ply:IsSuperAdmin() then
				return true
			end

			return false
		end

		function PermissionInterface.getRanks( )
			local ranks = { } // internalName: string, title: string

			if ULib then
				for internalName, rankInfo in pairs( ULib.ucl.groups ) do
					if v != ULib.ACCESS_ALL then
						table.insert( ranks, { internalName = internalName, title = internalName } )
					end
				end
				return ranks
			end

			if evolve then
				for internalName, rankInfo in pairs( evolve.ranks ) do
					table.insert( ranks, { internalName = internalName, title = rankInfo.Title } )
				end
				return ranks
			end

			if serverguard then
				for internalName, rankInfo in pairs( serverguard.ranks.stored ) do
					table.insert( ranks, { internalName = internalName, title = rankInfo.name } )
				end
				return ranks
			end

			if NM then // including network manager to list
				for k,v in pairs(NM.ranks.list) do
					table.insert( ranks, { internalName = k, title = v.displayName } )
				end
				return ranks
			end

			return ranks
		end
	end

	// Pointshop2 - Points over time
	if (Pointshop2) then
		function Pointshop2.UpdatePointsOverTime( )
			Pointshop2.StandardPointsBatch:begin( )

			local groupMultipliers = Pointshop2.GetSetting( "Pointshop 2", "PointsOverTime.GroupMultipliers" )
			local points = Pointshop2.GetSetting( "Pointshop 2", "PointsOverTime.Points" )
			for k, ply in pairs( player.GetAll( ) ) do
				for grp,bonus in pairs(groupMultipliers) do
					if (ply:IsUserGroup(grp)) then // changed to use IsUserGroup to support subgrouping
						// Try to find a nice rank name
						local titleLookup = {}
						for k, v in pairs( PermissionInterface.getRanks( ) ) do
							titleLookup[v.internalName] = v.title
						end

						local rank = titleLookup[grp] or grp
						// Give points only if player is not afk
						if not ( Pointshop2.GetSetting( "Pointshop 2", "BasicSettings.PotAfkCheck" ) and ply:GetNWBool( "playerafk", false ) ) then
							ply:PS2_AddStandardPoints( bonus, rank .. " Bonus", true )
						end
					end
				end

				// Give points only if player is not afk
				if not ( Pointshop2.GetSetting( "Pointshop 2", "BasicSettings.PotAfkCheck" ) and ply:GetNWBool( "playerafk", false ) ) then
					ply:PS2_AddStandardPoints( points, "Playing on the Server" )
				end
			end

			Pointshop2.StandardPointsBatch:finish( )
		end
	end

	// Neutron
	if (Neutron) then
		function Neutron.PlyRank(Ply)
			return NM.overrides.GetRank(Ply)
		end
	end

	// Prometheus version v1.6.3.28
	if (Prometheus) then // Fix the prometheus set rank to add our support.
		Prometheus.AddAction({
			name = "rank",
			runfunc = function(Ply, Values, TT)
				print("Prometheus SetRank values:")
				PrintTable(Values)
				local CurrentRank = false
				local UsePrefix = true
				local SetRank = Values.rank_when

				if Values.rank_prefix == "" || !Values.rank_prefix then
					UsePrefix = false
				end

				if (SetRank == "" ||  SetRank == nil) && !UsePrefix then
					Prometheus.Error("'rank' RUN action wasn't run, because there isn't a rank set to promote to! Player SteamID: " .. Ply:SteamID() )
					return
				end

				CurrentRank = NM.overrides.GetRank(Ply)

				if UsePrefix && CurrentRank != "" && !ASS_VERSION then
					SetRank = Values.rank_prefix .. CurrentRank
				end

				Prometheus.Debug("Setting rank '" .. SetRank ..  "' on player: " .. Ply:Nick() .. "{" .. Ply:SteamID() .. "}")

				if SetRank == CurrentRank then
					Prometheus.Info("'rank' RUN action will not run, because their current rank is the same as the new rank. " .. Ply:SteamID() )
				else
					local Succ, Err

					if ULib then
						Succ, Err = pcall(ULib.ucl.addUser, Ply:SteamID(), nil, nil, SetRank)
					elseif evolve then
						Succ, Err = pcall(Ply.EV_SetRank, Ply, SetRank)
					elseif moderator then
						Succ, Err = pcall(moderator.SetGroup, Ply, SetRank)
					elseif ASS_VERSION then
						Succ, Err = pcall(Ply.SetLevel, Ply, tonumber(SetRank) )
						ASS_SaveRankings()
					elseif serverguard then
						Succ, Err = pcall(serverguard.player.SetRank, serverguard.player, Ply, SetRank, false)
					elseif maestro then
						Succ, Err = pcall(Ply.SetUserGroup, Ply, SetRank)
					elseif NM then // including network manager to list
						Succ, Err = pcall(Ply.NM_Rank_SetRank, Ply, SetRank)
					else
						Err = "No compatible admin mods present!"
					end

					if !Succ then
						Prometheus.Error("Error while adding player: " .. Ply:Nick() .. "{" .. Ply:SteamID() .. "} to rank: " .. SetRank .. " Error info: " .. Err)
					end
				end

				if tonumber(Values.rank_before) == 1 then
					if CurrentRank == "" then
						CurrentRank = false
						Prometheus.Error("'rank' RUN actions current rank couldn't be properly fetched, after this players package runs out, they will not be demoted! " .. Ply:SteamID() )
					end
					Values.rank_after = CurrentRank
				end
				return true
			end,
			endfunc = function(Ply, Values)
				print("Prometheus RemoveRank values:")
				PrintTable(Values)
				local CurrentRank = false
				local Prefix = Values.rank_prefix or ""

				if (Values.rank_after == "" || Values.rank_after == false) && NM then
					Values.rank_after = Values.rank_when // Network manager simply removes ranks(if change not specified)
				end

				Prometheus.Debug("Setting rank '" .. Values.rank_after ..  "' on player: " .. Ply:Nick() .. "{" .. Ply:SteamID() .. "}")

				if Values.rank_after == "" ||  Values.rank_after == nil then
					Prometheus.Error("'rank' END action wasn't run, because there isn't a rank set to promote to! Player SteamID: " .. Ply:SteamID() )
					return
				end

				if Values.rank_after == false then
					Prometheus.Error("'rank' END action couldn't be run, because the after rank couldn't be found, this could be because when their new rank was set, their old rank couldn't be fetched.")
					return
				end

				CurrentRank = NM.overrides.GetRank(Ply)

				if (!NM) then
					if Values.rank_prefix == "" || !Values.rank_prefix then
						if Values.rank_when != CurrentRank then
							Prometheus.Info("'rank' END action will not run, because the rank they were set to and their current rank don't match, which means their rank was changed while the action was active. " .. Ply:SteamID() )
							return
						end
					else
						if Prefix .. Values.rank_after != CurrentRank then
							Prometheus.Info("'rank' END action will not run, because the rank they were set to and their current rank don't match, which means their rank was changed while the action was active. " .. Ply:SteamID() )
							return
						end
					end
				end


				if Values.rank_after == CurrentRank && !NM then // CurrentRank COULD be the rank after if we're using network manager's systems
					Prometheus.Info("'rank' END action will not run, because their current rank is the same as the new rank. " .. Ply:SteamID() )
				else
					local Succ, Err

					if ULib then
						Succ, Err = pcall(ULib.ucl.addUser, Ply:SteamID(), nil, nil, Values.rank_after)
					elseif evolve then
						Succ, Err = pcall(Ply.EV_SetRank, Ply, Values.rank_after)
					elseif moderator then
						Succ, Err = pcall(moderator.SetGroup, Ply, Values.rank_after)
					elseif ASS_VERSION then
						Succ, Err = pcall(Ply.SetLevel, Ply, tonumber(Values.rank_after) )
						ASS_SaveRankings()
					elseif serverguard then
						Succ, Err = pcall(serverguard.player.SetRank, serverguard.player, Ply, Values.rank_after, false)
					elseif maestro then
						Succ, Err = pcall(Ply.SetUserGroup, Ply, SetRank)
					elseif NM then // including network manager to list
						Succ, Err = pcall(Ply.NM_Rank_RemoveRank, Ply, Values.rank_after)
					else
						Prometheus.Error("'rank' END action couldn't set players rank, because there are no compatible admin mods present! Player SteamID: " .. Ply:SteamID() )
					end

					if !Succ then
						Prometheus.Error("Error while adding player: " .. Ply:Nick() .. "{" .. Ply:SteamID() .. "} to rank: " .. Values.rank_after .. " Error info: " .. Err)
					end
				end
			end
		})
	end

	if (NM.hotfix && NM.hotfix.Overrides) then
		NM.hotfix.Overrides()
	end
end
