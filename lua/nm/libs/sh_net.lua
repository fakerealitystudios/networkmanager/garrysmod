// RAWR!

NM.net = NM.net or {}

if (SERVER) then
    function NM.net.BroadcastLib(lib)
        local brdcst = {}
    	if (istable(NM[lib])&&NM[lib].NetworkBroadcast) then
    		brdcst[lib] = NM[lib].NetworkBroadcast()
    	end
        net.Start("NM_Player.Network")
            net.WriteEntity(nil)
            net.WriteTable(brdcst)
        net.Broadcast()
    end
end
