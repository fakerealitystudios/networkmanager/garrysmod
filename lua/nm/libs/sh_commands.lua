// RAWR!

NM.commands = NM.commands or {}
NM.commands.cats = NM.commands.cats or {}
NM.commands.list = NM.commands.list or {}
NM.commands.concmds = NM.commands.concmds or {}

function NM.commands.Initialize()
	NM.util.IncludeInternalDir("commands")

	// After commands have been loaded
	local concmds = {}
	for cmd,cmdTbl in pairs(NM.commands.list) do
		for _,concmd in pairs(cmdTbl.concmd) do
			table.insert(concmds, concmd)
		end
	end
	local skip = {}
	for k,concmd in pairs(NM.commands.concmds) do // Hot reloading trick
		if (!table.HasValue(concmds, concmd)) then
			concommand.Remove(concmd)
		else
			table.insert(skip, concmd)
		end
	end
	NM.commands.concmds = concmds
	for _,concmd in pairs(NM.commands.concmds) do
		if (!table.HasValue(skip, concmd)) then
			concommand.Add(concmd, function(ply, cmd, args, argStr)
				NM.commands.Parse(ply, args)
			end, NM.commands.AutoComplete)
		end
	end
end

function NM.commands.AutoComplete(concmd, stringargs)
	local args = NM.util.ExtractArgs(stringargs, true)
	local possibilities = {}

	local commands = table.Copy(NM.commands.list)
	for cmd,cmdTbl in pairs(commands) do
		if (cmdTbl.noConsole) then
			commands[cmd] = nil
			continue
		end
		if (!LocalPlayer():NM_Permission_Has(cmdTbl.permission)) then
			commands[cmd] = nil
			continue
		end
		if (!table.HasValue(cmdTbl.concmd, concmd)) then
			commands[cmd] = nil
			continue
		end
		if (table.Count(args) >= 1) then
			if (!cmd:lower():find(args[1]:lower(), 1, true)) then // Find what commands we could be attempting to use
				commands[cmd] = nil
			end
			if (cmd:lower() == args[1]:lower()) then
				commands = {}
				commands[cmd] = cmdTbl
			end
		end
	end

	// We know the possible commands, if there is only one, we continue to the arguments
	if (table.Count(commands) == 1) then
		local cmdArgCount = table.Count(args) - 1
		if (string.sub(stringargs, -1) == " ") then
			cmdArgCount = cmdArgCount + 1
		end
		for cmd,cmdTbl in SortedPairsByMemberValue(commands, "cmd", false) do
			local text = " "..cmd
			if (cmdArgCount >= 1) then
				for i=2,cmdArgCount do
					if (args[i]) then
						text = text.." "..args[i]
					end
				end
			end
			local argCount = 0
			for k,arg in pairs(cmdTbl.args) do
				argCount = argCount + 1
				if (argCount != cmdArgCount) then
					continue
				end
				if (arg[3].hidden) then // When a argument is hidden, means it probably has an opposite command that we won't use
					continue
				end

				if (arg[4].autocomplete) then
					for _,v in pairs(arg[4].autocomplete(string.Replace(args[cmdArgCount + 1] or "", "\"", ""))) do
						table.insert(possibilities, concmd..text.." \""..v.."\"")
					end
				else
					if (arg[2]) then
						text = text.." <" // Required
					else
						text = text.." [" // Optional
					end
					text = text..arg[1]
					if (arg[2]) then
						text = text..">"
					else
						text = text.."]"
					end
				end
			end
			if (table.Count(possibilities) == 0) then
				table.insert(possibilities, concmd..text)
			end
		end
	// We have multiple possiblities of commands, just list them
	else
		for cmd,cmdTbl in SortedPairsByMemberValue(commands, "cmd", false) do
			local text = " "..cmd
			table.insert(possibilities, concmd..text)
		end
	end

	return possibilities
end

function NM.commands.Add(cat, cmd, func, aliases)
	if (!func||!isfunction(func)) then
		ErrorNoHalt("The command '"..cmd.."' has no function!")
		return
	end

	NM.commands.list[cmd] = nil
	for cat,cmds in pairs(NM.commands.cats) do
		for k, cmdTbl in pairs(cmds) do
			if (cmdTbl.cmd == cmd) then
				NM.commands.cats[cat][k] = nil
				if (table.Count(NM.commands.cats[cat]) <= 0) then
					NM.commands.cats[cat] = nil
				end
			end
		end
	end

	NM.dev.Log(7, "Adding command '"..cmd.."'")
	local command = NM.commands.list[cmd] or {}
	if (!aliases) then
		command.aliases = {}
	elseif (type(aliases) == "table") then
		command.aliases = aliases
	else
		command.aliases = {aliases}
	end
	cat = string.lower(cat)
	command.cat = cat
	command.cmd = cmd
	command.concmd = {"nm"}

	command.func = func
	command.args = {}
	command.defaultRank = "superadmin"
	command.help = "Unknown"
	command.prefix = NM.config.Get("administration_commandChatPrefix")

	/*local rank = NM.permissions.Get(cmd)
	if (!rank||!NM.ranks.Get(rank)) then
		if (!NM.ranks.Get(rank)) then
			NM.permissions.Unregister(cmd)
		end
		NM.permissions.Register(cmd, command.defaultRank)
		command.canResetRank = true
	else
		command.canResetRank = false
		command.defaultRank = rank
	end*/
	command.permission = "nm"..NM.permissionDelimiter.."commands"..NM.permissionDelimiter..cmd

	setmetatable(command, NM.meta.Command)
	NM.commands.list[cmd] = command

	NM.commands.cats[cat] = NM.commands.cats[cat] or {}
	table.insert(NM.commands.cats[cat], NM.commands.list[cmd])

	return command
end

if (CLIENT) then
	function NM.commands.Parse(ply, args, isChat)
		net.Start("NM_SendCommand")
			net.WriteTable(args or {})
			net.WriteBool(isChat or false)
		net.SendToServer()
	end
end

if (SERVER) then
	util.AddNetworkString("NM_SendCommand")

	net.Receive("NM_SendCommand", function(len, ply)
		NM.commands.Parse(ply, net.ReadTable(), net.ReadBool())
	end)

	function NM.commands.Parse(ply, args, isChat)
		if (type(args)!="table") then
			ErrorNoHalt("Command args expected to be a table")
			return false
		end

		if (!args[1]) then
			ErrorNoHalt("Arguments seem to be missing?")
			return false
		end

		local justincase = args[1]
		local phrase = args[1]:lower()
		table.remove(args, 1)

		local cmd = nil
		local data = nil
		local found = false
		local opposite = false

		for index,value in pairs(NM.commands.list) do
			if ((isChat && value.noChat) || (!isChat && value.noConsole)) then
				continue
			end
			local text = phrase
			if (isChat && value.prefix != "") then
				if (value.prefix != text:sub(1, 1)) then
					if (value.prefix != "!") then
						//print(index.." failed prefix check for '"..value.prefix.."'")
					end
					continue
				else
					text = text:sub(2)
				end
			end
			if (value.prefix == "" && text:sub(1,1) == index) then // Quick fix for asay(@ command in chat)
				cmd = index
				data = value
				found = true
				table.insert(args, 1, justincase:sub(2))
				break
			end
			if (text == index) then
				cmd = index
				data = value
				found = true
				break
			end
			if (value.opposite&&text == value.opposite[1]) then
				cmd = index
				data = value
				found = true
				opposite = true
				break
			end
			if (table.HasValue(value.aliases, text)) then
				cmd = index
				data = value
				found = true
				break
			end
		end
		if (!found) then
			if (!isChat) then
				if (IsValid(ply)) then
					ply:NM_SendConsoleMessage(Color(32,178,200), "[MK-A] ", Color(255,255,255), "Couldn't find the specified command!")
				else
					print("[MK-A] ".."Couldn't find the specificed command!")
				end
			else
				//ply:NM_SendChat(Color(255,255,255), "Couldn't find the specified command!")
			end
			return false -- We couldn't find the specified command
		end
		if (!IsValid(ply) && !data.canConsole) then
			print("[MK-A] ".."Sorry, the ethereal console can not run this command!")
			return false
		end
		NM.log.Log(NM.log.LogType.PLAYER, "command", nil, {ply, args})

		local permission = data.permission
		if (!IsValid(ply)||ply:IsListenServerHost()||ply:NM_Permission_Has(permission)) then
			required = 0
			for k,v in pairs(data.args) do
				if (v[2]) then
					required = required + 1
				else
					break
				end
			end
			if (table.Count(args) >= required) then
				local newArgs = {}
				local run = true
				for k,v in pairs(data.args) do
					if (opposite&&data.opposite[2][k] != nil) then
						table.insert(newArgs, data.opposite[2][k])
						continue
					end
					local arg = args[k]
					if ((v[4].name == NM.commands.args.String.name || v[4].name == NM.commands.args.Text.name)&&#data.args==k) then
						//print("last arg is string")
						arg = table.concat(args, " ", k, #args)
					end
					local success, result = v[4].validation(ply, arg or nil, v[3], v[2])
					if (success) then
						table.insert(newArgs, result)
					else
						run = false
						if (IsValid(ply)) then
							ply:NM_SendChat(Color(32,178,200), "[MK-A] ", Color(255,255,255), result)
						else
							print("[MK-A] "..result)
						end
						break
					end
				end
				if (run) then
					local result, str = NM.commands.list[cmd].func(ply, unpack(newArgs or {}))
                    //hook.Run("NM_Command", ply, cmd, newArgs, result)
					if (str) then
						if (IsValid(ply)) then
							ply:NM_SendChat(Color(32,178,200), "[MK-A] ", Color(255,255,255), str)
						else
							print("[MK-A] "..str)
						end
					end
				end
			else
				local text = "That command requires more arguments!"
				if (IsValid(ply)) then
					ply:NM_SendChat(Color(32,178,200), "[MK-A] ", Color(255,255,255), text)
				else
					print("[MK-A] "..text)
				end
			end
		elseif (IsValid(ply)) then
			hook.Run("NM_Permissions.NotAllowed", ply, cmd, permission)
			ply:NM_SendChat(Color(32,178,200), "[MK-A] ", Color(255,255,255), "You don't have permission to that command!")
		end
		return true -- We found the specified command
	end
end

NM.commands.args = {}
local function stringParsePlayer(str, ply)
	if (str == "") then
		return {}
	end
	if (str == "*") then
		return player.GetAll()
	end
	if (str == "^") then
		return {ply}
	end
	if (str == "@") then
		local trace = ply:GetEyeTrace()
		if (trace.Entity&&trace.Entity:IsPlayer()) then
			return {trace.Entity}
		end
	end
	/*if (str == "#last") then
		if (ply.nm.latest.targets) then
			return ply.nm.latest.targets
		end
	end*/
	local target = player.GetBySteamID(str:upper())
	if (IsValid(target)) then
		return {target}
	end
	local target = player.GetBySteamID64(str)
	if (IsValid(target)) then
		return {target}
	end
	local targets = NM.util.FindPlayer(str, ply)
	return targets
end
local function removeUnableTargets(ply, param, possibleTargets)
	local targets = {}
	for k,target in pairs(possibleTargets) do
		if ((!IsValid(ply)||(ply:NM_CanTarget(target)||param.canTargetHigherRank))&&!table.HasValue(targets, target)) then
			table.insert(targets, target)
		end
	end
	return targets
end

NM.commands.args.Player = {
	name = "player",
	prep = function(param)
		param.canTargetHigherRank = param.canTargetHigherRank or false
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end

		if (!isstring(str)) then
			local tbl = istable(str) and str or {str}
			local possibleTargets = {}
			for k,v in pairs(tbl) do
				if (v && isentity(v) && IsValid(v) && v:IsPlayer()) then
					table.insert(possibleTargets, v)
				end
			end
			if (table.Count(possibleTargets) == 0) then
				return false, "Players were not provided"
			end
			if (table.Count(possibleTargets) > 1) then
				return false, "This command doesn't support multiple targets."
			end
			local targets = removeUnableTargets(ply, param, possibleTargets)
			if (table.Count(targets) != 1) then
				return false, "The target(s) are immune to you."
			end
			return true, targets
		end
		local str = str:lower()

		if (str == "") then
			return false, "Argument not provided!"
		end

		local args = string.Explode("&", str)
		local bool, possibleTargets = false, {}
		if (table.Count(args) <= 1) then
			possibleTargets = stringParsePlayer(str, ply)
		else
			//BroadcastLua("print('args multi: "..table.concat(args, " | ").."')")
			for k,arg in pairs(args) do
				local targets = stringParsePlayer(arg, ply)
				for k,target in pairs(targets) do
					table.insert(possibleTargets, target)
				end
			end
		end

		local targets = removeUnableTargets(ply, param, possibleTargets)
		if (table.Count(targets) != 1) then
			if (table.Count(possibleTargets) > 1) then
				return false, "This command doesn't support multiple targets."
			end
			if (table.Count(possibleTargets) != 0) then
				return false, "The target(s) are immune to you."
			end
			return false, "Couldn't find the specified player(s)."
		end
		return true, targets
	end,
	autocomplete = function(str)
		local plys = {}
		table.insert(plys, "^")
		for _,v in pairs(player.GetAll()) do
			table.insert(plys, v:Name())
		end
		if (str != "") then
			return NM.util.FindStringInTable(str, plys, true)
		end
		return plys
	end
}
NM.commands.args.Players = {
	name = "players",
	prep = function(param)
		param.canTargetHigherRank = param.canTargetHigherRank or false
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end

		if (!isstring(str)) then
			local tbl = istable(str) and str or {str}
			local possibleTargets = {}
			for k,v in pairs(tbl) do
				if (v && isentity(v) && IsValid(v) && v:IsPlayer()) then
					table.insert(possibleTargets, v)
				end
			end
			if (table.Count(possibleTargets) == 0) then
				return false, "Players were not provided"
			end
			local targets = removeUnableTargets(ply, param, possibleTargets)
			if (table.Count(targets) == 0) then
				return false, "The target(s) are immune to you."
			end
			return true, targets
		end
		local str = str:lower()
		if (str == "") then
			return false, "Argument not provided!"
		end

		local args = string.Explode("&", str)
		local bool, possibleTargets = false, {}
		if (table.Count(args) <= 1) then
			possibleTargets = stringParsePlayer(str, ply)
		else
			//BroadcastLua("print('args multi: "..table.concat(args, " | ").."')")
			for k,arg in pairs(args) do
				local targets = stringParsePlayer(arg, ply)
				for k,target in pairs(targets) do
					table.insert(possibleTargets, target)
				end
			end
		end
		local targets = removeUnableTargets(ply, param, possibleTargets)
		if (table.Count(targets) == 0) then
			if (table.Count(possibleTargets) != 0) then
				return false, "The target(s) are immune to you."
			end
			return false, "Couldn't find the specified player(s)."
		end
		return true, targets
	end,
	autocomplete = function(str)
		local plys = {}
		table.insert(plys, "^")
		for _,v in pairs(player.GetAll()) do
			table.insert(plys, v:Name())
		end
		if (str != "") then
			return NM.util.FindStringInTable(str, plys, true)
		end
		return plys
	end
}
NM.commands.args.Rank = {
	name = "rank",
	prep = function(param)
		param.canTargetHigherRank = param.canTargetHigherRank or false
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end

		local str = str:lower()
		local rank = param.default or nil
		if (NM.ranks.list[str]) then
			rank = str
		else
			for k,v in pairs(NM.ranks.list) do
				if (string.find(k:lower(), str)||string.find(v.displayName:lower(), str)) then
					rank = k
					break
				end
			end
		end
		if (rank) then
			if (ply:IsPlayer()&&!ply:IsListenServerHost()&&!param.canTargetHigherRank&&!ply:NM_CanTarget(rank)) then
				return false, "Specified rank is higher then you."
			end
			return true, rank
		end
		return false, "Couldn't find specified rank '"..str.."'."
	end,
	autocomplete = function(str)
		local ranks = {}
		for rank,v in pairs(NM.ranks.list) do
			table.insert(ranks, rank)
		end
		if (str != "") then
			return NM.util.FindStringInTable(str, ranks, true)
		end
		return ranks
	end
}
NM.commands.args.Team = {
	name = "team",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end

		local id = tonumber(str)
		if (id == nil) then // Treat as string
			for k, v in pairs(team.GetAllTeams()) do
				if (v.Name:lower() == str:lower()) then
					return true, k
				end
			end
			return false, "Couldn't find team by name of "..str
		end
		if (!team.Valid(id)) then
			return false, "Invalid team Id!"
		end
		return true, id
	end,
	autocomplete = function(str)
		local names = {}
		for k, v in SortedPairsByMemberValue(team.GetAllTeams(), "Name") do
			table.insert(names, v.Name)
		end
		if (str != "") then
			return NM.util.FindStringInTable(str, names, true)
		end
		return names
	end
}
NM.commands.args.Map = {
	name = "map",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end

		for k, v in pairs(NM.maps) do
			if (v:lower() == str:lower()) then
				return true, v
			end
		end
		return true, (str != "" && str) or param.default or ""
	end,
	autocomplete = function(str)
		if (str != "") then
			return NM.util.FindStringInTable(str, NM.maps, true)
		end
		return NM.maps
	end
}
NM.commands.args.String = {
	name = "string",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or ""
		end

		return true, (str != "" && str) or param.default or ""
	end,
}
NM.commands.args.Text = {
	name = "text",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or ""
		end

		return true, (str != "" && str) or param.default or ""
	end,
}
NM.commands.args.Json = {
	name = "json",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end

		local tbl = util.JSONToTable(str)
		if (!tbl) then
			return false, "Invalid JSON format."
		end
		return true, tbl or param.default or nil
	end,
}
NM.commands.args.Time = {
	name = "time",
	prep = function(param)
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default or nil
		end

		return true, str or param.default or nil
	end,
	autocomplete = function(str)
		if (tonumber(str) != nil) then
			local num = tonumber(str)
			return {num.."m", num.."h", num.."d", num.."w", num.."mo", num.."y"}
		end
		local times = {"30m", "1h", "6h", "12h", "1d", "1w", "1mo"}
		return times
	end
}
NM.commands.args.Amount = {
	name = "amount",
	prep = function(param)
		param.default = param.default or 0
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default
		end

		str = tonumber(str)
		if (isnumber(str)) then
			if (!param.min||str >= param.min) then
				if (!param.max||str <= param.max) then
					return true, str
				else
					return false, "Number must be smaller then "..param.max.."."
				end
			else
				return false, "Number must be greater then "..param.min.."."
			end
		else
			return false, "No number found."
		end
	end,
}
NM.commands.args.Boolean = {
	name = "boolean",
	prep = function(param)
		param.default = param.default or nil
		return param
	end,
	validation = function(ply, str, param, required)
		if (!required&&!str) then
			return true, param.default
		end

		if (isnumber(str)) then
			if (str=="0") then
				return true, false
			elseif (str=="1") then
				return true, true
			else
				return false, "Number form must be 0(false) or 1(true)."
			end
		else
			str = str:lower()
			if (str=="false"||str=="f") then
				return true, false
			elseif (str=="true"||str=="t") then
				return true, true
			else
				return false, "String form must be 'true' or 'false'."
			end
		end
		return false, "Unknown failure"
	end,
	autocomplete = function(str)
		if (str != ""&&string.find("true", str:lower())) then
			return {"true"}
		elseif (str != ""&&string.find("false", str:lower())) then
			return {"false"}
		end
		return {"true", "false"}
	end
}
NM.commands.args.Bool = NM.commands.args.Boolean
