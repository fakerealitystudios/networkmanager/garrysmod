// RAWR!

NM.players = NM.players or {}
NM.players.external = NM.players.external or {}
NM.players.external.list = NM.players.external.list or {}
NM.players.external.requests = NM.players.external.requests or {}

net.WriteVars[TYPE_DAMAGEINFO] = function(t,v) net.WriteUInt( t, 8 ) end
net.ReadVars[TYPE_DAMAGEINFO] = function(t,v) return nil end

function NM.players.GetByNMID(nmid)
    for k,v in pairs(player.GetAll()) do
        if (tostring(v:NM_GetId()) == tostring(nmid)) then
            return v
        end
    end
    return nil
end