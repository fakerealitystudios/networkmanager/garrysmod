// RAWR!

function player.GetSuperAdmins()
	local players = {}
	for k,v in pairs(player.GetAll()) do
		if (v:IsSuperAdmin()) then
			table.insert(players, v)
		end
	end
	return players
end

function player.GetAdmins()
	local players = {}
	for k,v in pairs(player.GetAll()) do
		if (v:IsAdmin()) then
			table.insert(players, v)
		end
	end
	return players
end

function player.GetByRank(rank)
	local players = {}
	for k,v in pairs(player.GetAll()) do
		if (v:NM_Rank_CheckUserGroup(rank, true)) then
			table.insert(players, v)
		end
	end
	return players
end

function player.GetByPermission(permission)
	local players = {}
	for k,v in pairs(player.GetAll()) do
		if (v:NM_Permission_Has(permission)) then
			table.insert(players, v)
		end
	end
	return players
end
