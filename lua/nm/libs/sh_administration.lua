// RAWR!

NM.administration = NM.administration or {}
NM.administration.version = "1.1.6"
NM.administration.commands = NM.administration.commands or {}

local function iscolor(tab)
	if (tab&&istable(tab)&&tab.r&&tab.g&&tab.b&&tab.a) then
		return true
	end
	return false
end

local function parser(...)
	local text = {}
	for k,v in pairs({...}) do
		local varType = type(v):lower()
		if (varType == "string") then
			if (v:lower() == "console") then
				if (!iscolor(text[#text])) then
					table.insert(text, Color(0, 102, 102))
				end
				table.insert(text, "Console")
				continue
			end
			if (!iscolor(text[#text])) then
				table.insert(text, Color(255, 255, 255))
			end
			table.insert(text, v)
			continue
		elseif (varType == "number") then
			if (!iscolor(text[#text])) then
				table.insert(text, Color(84, 214, 453))
			end
			table.insert(text, tostring(v))
			continue
		end
		if (varType == "table"&&v.__type) then
			varType = v.__type:lower()
		end
		if (varType == "player") then
			if (IsValid(v)&&v:NM_TempData_Get("disguised", false)) then
				table.insert(text, (Color(153, 51, 255)))
				table.insert(text, "(Disguised)")
			else
				table.insert(text, (IsValid(v) and (team.GetColor(v:Team())) or Color(86, 86, 86)))
				table.insert(text, (IsValid(v) and v:Name()) or "Console")
			end
			continue
		elseif (varType == "nm_player") then
			table.insert(text, Color(0, 102, 102))
			table.insert(text, (v:Name()) or "Console")
			continue
		elseif (varType == "table") then
			if (type(v[1]):lower() == "player") then
				local plycount = table.Count(player.GetAll())
				if ((table.Count(v)/plycount)>=.8&&plycount > 1) then
					table.insert(text, Color(150, 0, 255))
					table.insert(text, "Everyone")
				else
					for _,ply in pairs(v) do
						if (type(ply):lower() == "player") then
							table.insert(text, team.GetColor(ply:Team()) or Color(255, 255, 255))
							table.insert(text, ((IsValid(ply) and ply:Name()) or "Console"))
							table.insert(text, Color(255, 255, 255))
							table.insert(text, ", ")
						end
					end
					table.remove(text, table.Count(text))
					table.remove(text, table.Count(text))
				end
				continue
			elseif (v&&iscolor(v)) then
				table.insert(text, v)
			elseif (table.Count(v) == 3&&type(v[1]):lower()=="boolean") then
				if (v[1]) then
					local textTable = istable(v[2]) and v[2] or {v[2]}
					local parsed = parser(unpack(textTable))
					table.Add(text, parsed)
				else
					local textTable = istable(v[3]) and v[3] or {v[3]}
					local parsed = parser(unpack(textTable))
					table.Add(text, parsed)
				end
			end
			continue
		elseif (varType == "entity"&&!IsValid(v)) then
			if (!iscolor(text[#text])) then
				table.insert(text, Color(0, 102, 102))
			end
			table.insert(text, "Console")
			continue
		end
		if (!text[#text]||!iscolor(text[#text])) then
			table.insert(text, Color(255, 255, 255))
		end
		table.insert(text, v)
	end
	return text
end

function NM.administration.Broadcast(...)
	NM.administration.SelectiveBroadcast(player.GetAll(), ...)
end

function NM.administration.SelectiveBroadcast(targets, ...)
	local targets = targets or player.GetAll()
	local textTable = parser(...)
	MsgC(unpack(textTable))
	Msg("\n")
	local txt = ""
	for k,v in pairs(textTable) do
		if (isstring(v)) then
			txt = txt..v
		end
	end
	NM.log.Log(NM.log.LogType.SERVER, "print", nil, {txt})
	player.SendChat(targets, Color(32,178,200), "[MK-A] ", unpack(textTable))
end
