// RAWR!

net.Receive("NM_ExternalPlayersRequest", function(len)
	local Player = net.ReadTable()
	setmetatable(Player, NM.meta.Player)
	for _,callback in pairs(NM.players.external.requests[Player:SteamID64()]) do
		callback(Player)
		NM.players.external.requests[_] = nil
	end
	NM.players.external.list[Player:SteamID64()] = Player
end)

function NM.players.external.GetPlayer(data, callback)
	if (!data||!callback) then return end
	local Player = {
		steam = "",
		steamid = "",
		name = "",
		ip = "",
	}
	setmetatable(Player, NM.meta.Player)
	Player:NM_InitializePlayer()
	if (isstring(data) && data:lower() == "console") then // Fuck console
		Player.steam = "Console"
		Player.steamid = "Console"
		Player.name = "Console"
		Player.ip = "Console"
		Player.nm.id = 1
		NM.players.external.list[Player:SteamID64()] = Player
		if (callback) then
			callback(Player)
		end
		return true
	elseif (type(data)=="Player") then
		Player.steam = data:SteamID64()
		Player.steamid = data:SteamID()
	elseif (isstring(data) && data:find("STEAM_")) then
		Player.steam = util.SteamIDTo64(data)
		Player.steamid = data
	elseif (string.gmatch(tostring(data), "^[0-9]{17}$")) then
		Player.steam = data
		Player.steamid = util.SteamIDFrom64(data)
	else
		Player.nm.id = data
	end
	if (NM.players.external.list[Player:SteamID64()]) then
		callback(NM.players.external.list[Player:SteamID64()])
		return true
	end
	if (NM.players.external.requests[Player:SteamID64()]) then
		table.insert(NM.players.external.requests[Player:SteamID64()], callback)
	else
		NM.players.external.requests[Player:SteamID64()] = {}
		table.insert(NM.players.external.requests[Player:SteamID64()], callback)
		net.Start("NM_ExternalPlayersRequest")
			net.WriteString(Player:SteamID())
		net.SendToServer()
	end
	return false
end