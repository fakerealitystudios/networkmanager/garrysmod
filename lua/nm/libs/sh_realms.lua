// RAWR!

NM.realms = NM.realms or {}

function NM.realms.get(lrealm, fullRealm)
    local realmSplit = string.Explode(NM.realmDelimiter, fullRealm)
    local i = 0
    local count = table.Count(realmSplit)
    for _,realmPart in pairs(realmSplit) do
        if (!lrealm[realmPart]) then
            lrealm[realmPart] = NM.meta.Realm.new()
        end
        i = i + 1
        if (i == count) then
            return lrealm[realmPart]
        end
        lrealm = lrealm[realmPart]
    end
    return nil // Should never return nil honestly
end

function NM.realms.getInclusive(lrealm, fullRealm)
    local result = NM.meta.Realm.new()
    //return NM.meta.Realm.new()
    local realmSplit = string.Explode(NM.realmDelimiter, fullRealm)
    local i = 0
    local count = table.Count(realmSplit)
    for _,realmPart in pairs(realmSplit) do
        if (lrealm["*"]) then
            local tbl = table.Copy(lrealm["*"])
            for k,v in pairs(lrealm["*"]) do
                table.Add(result[k], v)
            end
        end
        if (!lrealm[realmPart]) then
            lrealm[realmPart] = NM.meta.Realm.new()
        end
        i = i + 1
        lrealm = lrealm[realmPart]
        if (i == count) then
            for k,v in pairs(lrealm) do
                table.Add(result[k], v)
            end
            return result
        end
    end
    return result
end
