// RAWR!

NM.players.map = NM.players.map or {}
NM.players.nm_map = NM.players.nm_map or {}

function NM.players.GetNMId(data, callback)
	local sid = ""
	local sid64 = ""
	if (isstring(data)) then
		if (data:lower() == "console") then // Fuck console
			callback(1)
			return
		elseif (data:find("STEAM_")) then
			sid = data
			sid64 = util.SteamIDTo64(data)
		elseif (string.gmatch(data, "^[0-9]{17}$")) then
			sid = util.SteamIDFrom64(data)
			sid64 = data
		end
	elseif (type(data)=="Player") then
		sid = data:SteamID()
		sid64 = data:SteamID64()
	else
		// Unknown data type!?
		return
	end

	if (NM.players.map[sid64]) then
		callback(NM.players.map[sid64])
		return
	end

    NM.api.get("/member&type=steamid64&token="..sid64, nil, function(tbl)
        NM.players.map[sid64] = tbl.member.nmid
        callback(tbl.member.nmid)
    end, function(tbl)
        callback(-1)
    end)
end

function NM.players.GetSteamId(nmid, callback)
	if (NM.players.nm_map[nmid]) then
		callback(NM.players.nm_map[nmid])
		return
	end

    NM.api.get("/member/"..nmid, nil, function(tbl)
        if (tbl.member.linked["steamid64"]) then
            NM.players.nm_map[nmid] = tbl.member.linked["steamid64"]
			callback(tbl.member.linked["steamid64"])
        else
            callback(-1)
        end
    end, function(tbl)
        callback(-1)
    end)
end

util.AddNetworkString("NM_ExternalPlayersRequest")

net.Receive("NM_ExternalPlayersRequest", function(len, ply)
	if (!ply:NM_Rank_IsAdmin()) then return end // Avoid spammy cheaters
	local function callback(Player)
		if (IsValid(ply)) then
			local plyTbl = table.Copy(Player)
			plyTbl.nm.data = nil
			net.Start("NM_ExternalPlayersRequest")
				net.WriteTable(plyTbl)
			net.Send(ply)
		end
	end
	NM.players.external.GetPlayer(net.ReadString(), callback, false, false)
end)

local function getPlayerExternalData(Player)
	if (Player:NM_GetId() == -1) then
		local sid64 = Player:SteamID64()
		hook.Add("NM_Player.Loaded", "NM_Player.Loaded."..sid64, function(ply)
			if (ply:SteamID64() == sid64) then
				hook.Remove("NM_Player.Loaded", "NM_Player.Loaded."..sid64)

				NM.players.external.list[ply:SteamID64()] = ply
				if (istable(NM.players.external.requests[ply:SteamID64()])) then
					for _,callback in pairs(NM.players.external.requests[ply:SteamID64()]) do
						callback(ply)
						NM.players.external.requests[ply:SteamID64()][_] = nil
					end
				end
				NM.players.external.requests[ply:SteamID64()] = nil
			end
		end)
		Player:NM_DB_LoadData(true)
	end
end

function NM.players.external.GetPlayerByID(nmid, callback, notPly)

end

function NM.players.external.GetPlayer(data, callback, notPly, name)
	if (!data||!callback) then return end
	local Player = {
		steam = "",
		steamid = "",
	}
    if (name) then
        Player.name = name
    end
	setmetatable(Player, NM.meta.Player)
	Player:NM_InitializePlayer()

	if (isstring(data) && data:lower() == "console") then // Fuck console
		Player.steam = "Console"
		Player.steamid = "Console"
		Player.name = "Console"
		Player.ip = "Console"
		Player.nm.id = 1
		NM.players.external.list[Player:SteamID64()] = Player
		if (callback) then
			callback(Player)
		end
		return true
	elseif (type(data)=="Player") then // Just setup to have the steamid for the rest
		Player.steam = data:SteamID64()
		Player.steamid = data:SteamID()
	elseif (isstring(data) && data:find("STEAM_")) then
		Player.steam = util.SteamIDTo64(data)
		Player.steamid = data
	elseif (string.gmatch(tostring(data), "^[0-9]{17}$")) then
		Player.steam = data
		Player.steamid = util.SteamIDFrom64(data)
	else
		Player.nm.id = data
	end
	if (Player.steam != "") then
		local target = player.GetBySteamID(Player:SteamID())
		if (IsValid(target)) then
			data = target
		end
	end
	if (type(data)=="Player"&&!notPly) then
		Player.steam = data:SteamID64()
		Player.steamid = data:SteamID()
		Player.ip = data:IPAddress()
		Player.name = data:Name()
		Player.nm = data.nm
		NM.players.external.list[Player:SteamID64()] = Player
		if (callback) then
			callback(Player)
		end
		return true
	end
	if (NM.players.external.list[Player:SteamID64()]) then
		if (callback) then
			callback(NM.players.external.list[Player:SteamID64()])
		end
		return true
	end
	if (NM.players.external.requests[Player:SteamID64()]) then
		table.insert(NM.players.external.requests[Player:SteamID64()], callback)
	else
		NM.players.external.requests[Player:SteamID64()] = {}
		table.insert(NM.players.external.requests[Player:SteamID64()], callback)
		getPlayerExternalData(Player)
	end
	return false
end