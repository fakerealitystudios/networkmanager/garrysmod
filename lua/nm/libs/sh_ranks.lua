// RAWR!

NM.ranks = NM.ranks or {}
NM.ranks.version = NM.ranks.version or "1.0.0"
NM.ranks.defaultRank = NM.ranks.defaultRank or "user"
NM.ranks.list = NM.ranks.list or {}
NM.ranks.children = NM.ranks.children or {}
NM.ranks.parents = NM.ranks.parents or {}

function NM.ranks.ShInitialize()
	
end

function NM.ranks.Get(rank)
	return NM.ranks.list[rank] or nil
end

function NM.ranks.GetSubGroup(subgroupid)
	local ranks = {}
	for k,v in pairs(NM.ranks.list) do
		if (v.subgrouping == subgroupid) then
			table.insert(ranks, v)
		end
	end
	return ranks
end

// Returns the higher-level class
// i.e. admin would return superadmin
function NM.ranks.GetChild(rank)
	local children = NM.ranks.GetChildren(rank)
	if (table.Count(children) > 0) then
		return children[#children]
	end
	return false
end

function NM.ranks.GetChildren(rank, inner)
	if (!NM.ranks.list[rank]) then return {} end
	if (!NM.ranks.children[rank]||inner) then
		local children = {}
		for k,v in pairs(NM.ranks.list) do
			if (v.inherit == rank) then
				table.Add(children, NM.ranks.GetChildren(k, true))
				table.insert(children, k)
			end
		end
		if (inner) then
			return children
		end
		NM.ranks.children[rank] = children
	end
	return table.Copy(NM.ranks.children[rank])
end

// Returns the lower-level class
// i.e. admin would return operator
function NM.ranks.GetParent(rank)
	local rankData = NM.ranks.Get(rank)
	if (rankData&&rankData.inherit) then
		return rankData.inherit
	end
	return false
end

function NM.ranks.GetParents(rank, inner)
	if (!NM.ranks.list[rank]) then return {} end
	if (!NM.ranks.parents[rank]||inner) then
		local parents = {}
		local parent = NM.ranks.GetParent(rank)
		if (parent) then
			table.Add(parents, NM.ranks.GetParents(parent, true))
			table.insert(parents, parent)
			/*for k,v in pairs(NM.ranks.list) do
				if (NM.ranks.GetParent(rank) == k) then
					table.Add(parents, NM.ranks.GetParents(k, true))
					table.insert(parents, k)
					if (inner) then
						return parents
					end
				end
			end*/
		end
		if (inner) then
			return parents
		end
		NM.ranks.parents[rank] = parents
	end
	return table.Copy(NM.ranks.parents[rank])
end

function NM.ranks.IsRank(rank1, rank2)
	if (!NM.ranks.list[rank1]||!NM.ranks.list[rank2]) then
		NM.dev.Log(3, "One of the requested groups ("..rank1.." or "..rank2..") to compare do not exist!")
		return false
	end
	local ranks = NM.ranks.GetParents(rank1)
	table.insert(ranks, rank1)
	return table.HasValue(ranks, rank2) or (NM.config.Get("development") and rank1 == "dev") or false
end

function NM.ranks.Message(rank, ...)
	for k,v in pairs(player.GetByRank(rank)) do
		v:NM_SendChat(...)
	end
end

function NM.ranks.CanTarget(group1, group2)
	if (!NM.ranks.list[group1]||!NM.ranks.list[group2]) then
		return false
	end
	if (NM.ranks.Permission_Has(group1, "nm.target."..group2) == true) then
		return true
	end
	return false
end

function NM.ranks.Permission_Has(rank, fullPerm, fullRealm)
	local fullRealm = fullRealm or NM.realm
	if (!NM.ranks.list[rank]) then return false end
	local realm = NM.realms.getInclusive(NM.ranks.list[rank].realms, fullRealm).__perms
	local permSplit = string.Explode(".", fullPerm)
	//PrintTable(realm)

	// Check if any perms are negated first, they take priority
	if (table.HasValue(realm, "!" .. fullPerm)) then
	return false
	elseif (string.sub(fullPerm, -1) == "%") then
        local perm = "!" .. string.sub(fullPerm, 1, -2) -- Remove the wildcard

        for _, realmPerm in pairs(realm) do
            if (string.sub(realmPerm, 1, string.len(perm)) == perm) then
                return false
            end
        end
    else
        local perm = "*"

        for _, permPart in pairs(permSplit) do
            if (table.HasValue(realm, "!" .. perm)) then
                return false
            else
                perm = string.sub(perm, 1, -2)
                perm = perm .. permPart .. NM.permissionDelimiter .. "*"
            end
        end
    end

    // Now check if we do have the perm
    if (table.HasValue(realm, fullPerm)) then
        return true
    elseif (string.sub(fullPerm, -1) == "%") then
        local perm = string.sub(fullPerm, 1, -2) -- Remove the wildcard

        for _, realmPerm in pairs(realm) do
            if (string.sub(realmPerm, 1, string.len(perm)) == perm) then
                return true
            end
        end
    else
        local perm = "*"

        for _, permPart in pairs(permSplit) do
            if (table.HasValue(realm, perm)) then
                return true
            else
                perm = string.sub(perm, 1, -2)
                perm = perm .. permPart .. NM.permissionDelimiter .. "*"
            end
        end
    end
    if (NM.ranks.GetParent(rank)) then
        return NM.ranks.Permission_Has(NM.ranks.GetParent(rank), fullPerm, fullRealm)
    end
    return nil
end
