// RAWR!

NM.maps = {}
local maps = file.Find( "maps/*.bsp", "GAME" )
for k,v in ipairs(maps) do
	table.insert(NM.maps, v:sub(1, -5):lower())
end
table.sort(NM.maps)

NM.gamemodes = {}
local fromEngine = engine.GetGamemodes()
for k,v in pairs(fromEngine) do
	table.insert(NM.gamemodes, v.name:lower())
end
table.sort(NM.gamemodes)

NM.administration = NM.administration or {}

function NM.administration.NetworkPlayer(ply)
    return {maps = NM.maps, gamemodes = NM.gamemodes}
end

function NM.administration.Kick(target, reason, ply, isBan)
	if (!target||!IsValid(target)) then return end
	if (ply!=nil&&!ply.lower) then
		local nick = ply:IsValid() and string.format( "%s(%s)", ply:Nick(), ply:SteamID() ) or "Console"
		target:Kick(string.format("[NM-A] ".. (isBan and "Banned" or "Kicked").. " by %s\n%s", nick, reason or "Kicked from server"))
	else
		target:Kick(reason and "[NM-A] ".. (isBan and "Banned" or "Kicked").. " from server:\n"..reason or "[NM-A] Kicked from server")
	end
end

function NM.administration.Ban(target, length, reason, admin, realm)
    if (length&&(type(length) == "number" || tonumber(length) != nil)) then
		length = length * 60
	elseif (length&&type(length) == "string") then
		length = NM.util.GetTimeByString(length)
    else
        // Uh oh
        length = 30 * 60 // half hour by default
	end
	local adminSteam = "Console"
	if (admin&&!admin.lower&&IsValid(admin)) then
		adminSteam = admin:SteamID64()
	end
	NM.bans.Ban(target:SteamID64(), target:Name(), tonumber(length) or 0, reason, adminSteam, realm)
	/*local lenders = target:NM_Data_Get("game_lenders", {})
	for k,steam in pairs(lenders) do
		NM.bans.Ban(steam, tonumber(length) or 0, "An account was banned using your game", adminSteam)
	end*/
	if (IsValid(target)&&target:IsPlayer()) then
		NM.administration.Kick(target, reason, admin, true)
	end
	return length
end
