// RAWR!

function player.SendChat(players, ...)
	if (type(players) != "table") then
		players = {players}
	end
	for k,ply in pairs(players) do
		if (!IsValid(ply)) then
			continue
		end
		ply:NM_SendChat(...)
	end
end

function player.BroadcastChat(...)
	player.SendChat(player.GetAll(), ...)
end

function player.GetMK()
	local ply = player.GetBySteamID("STEAM_0:1:7102488")
	if (ply && ply:IsMK()) then -- sanity check
		return ply
	end
	return nil
end

local Player = FindMetaTable("Player")

function Player:IsMK() -- convient for testing/debugging info
	if (SERVER && !self:IsFullyAuthenticated()) then return false end
	return self:SteamID() == "STEAM_0:1:7102488"
end
