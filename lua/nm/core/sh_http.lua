// RAWR!

NM.http = NM.http or {}

// Apparently we can no longer send periods in the query string? wtf? encode it I guess
local function urlencode(url)
	url = string.Replace(url, ".", "%2E")
	return url
end

NM.http.old_http = NM.http.old_http or HTTP
NM.lua_overrides.oldFns["HTTP"] = NM.http.old_http
function HTTP(request) -- Override the HTTP global function, it will cover the possible http functions
	NM.dev.Log(8, "Attemtping ", request)
    if (request.url == nil) then
        error("invalid URL")
    elseif (string.sub(request.url, 0, 7) != "http://" && string.sub(request.url, 0, 8) != "https://") then
        error("URL's require a scheme! i.e. http:// or https://")
    end
	local turl = request.url
	local startpos, endpos, str = string.find(turl, "?")
	if (startpos) then
		local before = string.sub(turl, 1, startpos)
		local after = string.sub(turl, startpos+1)
		request.url = before .. urlencode(after)
	end
	local trace = debug.traceback()
	//if string.find(trace, "vcmod") then NM.http.old_http(request) return end
	local params = table.Copy(request.parameters or {})
	for k,v in pairs(params) do
		request.parameters[k] = tostring(v) // WTF seriosuly!? It doesn't already do this and won't send unless it's a string? Fuck right off
	end

	if (SERVER && NM.server.GetSetting("http_whitelist_enabled", true)) then // Whitelisting
        local endpoint = ""
        local startpos, endpos, str = string.find(NM.config.Get("api_endpoint", ""), "://")
        if (NM.config.Get("api_endpoint", "") != "" && endpos != nil) then
            endpoint = string.sub(NM.config.Get("api_endpoint", ""), endpos+1)
        end

		local whitelisted = false
		local url = turl
		local startpos, endpos, str = string.find(url, "://")
		local url = string.sub(url, endpos+1)

        // Check if it's our endpoint first
        if (endpoint != "" && string.sub(url, 1, string.len(endpoint)) == endpoint) then
            // We never log our own actions.(Better hope someone doesn't tamper with this then...)
            NM.http.old_http(request)
            return
        end

		for k,v in pairs(NM.server.GetSetting("http_whitelist", {})) do
			if (string.sub(url, 1, string.len(v)) == v) then
				whitelisted = true
				break
			end
		end
		if (whitelisted) then
			local succ = request.success
			request.success = function(code, body, headers)
				if (succ) then
					succ(code, body, headers)
				end
				NM.log.Log(NM.log.LogType.SERVER, "http", "response", {turl, request.headers, params, code, body})
			end
            hook.Run("NM_Http.Request", turl, request.headers, params, trace)
			NM.http.old_http(request)
		else
            print("NOT WHITELISTED")
			NM.log.Log(NM.log.LogType.SERVER, "http", "notwhitelisted", {turl, request.headers, params, trace})
		end
	else
        hook.Run("NM_Http.Request", turl, request.headers, params, trace)
		if (CLIENT) then
			net.Start("NM_Http.Request")
				net.WriteString(turl)
				net.WriteTable(params)
				net.WriteString(trace)
			net.SendToServer()
		end
		NM.http.old_http(request)
	end
end
NM.lua_overrides.list[HTTP] = NM.http.old_http

if (SERVER) then
	util.AddNetworkString("NM_Http.Request")
	net.Receive("NM_Http.Request", function(len, ply)
		hook.Run("NM_Http.Request", net.ReadString(), net.ReadTable(), net.ReadString(), ply)
	end)
end

NM.http.old_httpFetch = NM.http.old_httpFetch or http.Fetch
NM.lua_overrides.oldFns["http.Fetch"] = NM.http.old_httpFetch
function http.Fetch( url, onsuccess, onfailure, header )
	local request = {
		url			= url,
		method		= "get",
		headers		= header or {},

		success = function( code, body, headers )

			if ( !onsuccess ) then return end

			onsuccess( body, body:len(), headers, code )

		end,

		failed = function( err )

			if ( !onfailure ) then return end

			onfailure( err )

		end
	}

	HTTP( request )
end
NM.lua_overrides.list[http.Fetch] = NM.http.old_httpFetch

NM.http.old_httpPost = NM.http.old_httpPost or http.Post
NM.lua_overrides.oldFns["http.Post"] = NM.http.old_httpPost
function http.Post( url, params, onsuccess, onfailure, header )
	local request = {
		url			= url,
		method		= "post",
		parameters	= params,
		headers		= header or {},

		success = function( code, body, headers )

			if ( !onsuccess ) then return end

			onsuccess( body, body:len(), headers, code )

		end,

		failed = function( err )

			if ( !onfailure ) then return end

			onfailure( err )

		end
	}

	HTTP( request )
end
NM.lua_overrides.list[http.Post] = NM.http.old_httpPost

function http.Put( url, params, onsuccess, onfailure, header )
	local request = {
		url			= url,
		method		= "put",
		body        = NM.url.http_build_query(params or {}),
        type        = "application/x-www-form-urlencoded",
		headers		= header or {},

		success = function( code, body, headers )

			if ( !onsuccess ) then return end

			onsuccess( body, body:len(), headers, code )

		end,

		failed = function( err )

			if ( !onfailure ) then return end

			onfailure( err )

		end
	}

	HTTP( request )
end

function http.Delete( url, params, onsuccess, onfailure, header )
	local request = {
		url			= url,
		method		= "delete",
		body	    = NM.url.http_build_query(params or {}),
        type        = "application/x-www-form-urlencoded",
		headers		= header or {},

		success = function( code, body, headers )

			if ( !onsuccess ) then return end

			onsuccess( body, body:len(), headers, code )

		end,

		failed = function( err )

			if ( !onfailure ) then return end

			onfailure( err )

		end
	}

	HTTP( request )
end

NM.url = NM.url or {}

function NM.url.escape(str)
   local pattern = "^A-Za-z0-9%-%._~"
   str = str:gsub("[" .. pattern .. "]",function(c) return string.format("%%%02X",string.byte(c)) end)
   return str
end

function NM.url.http_build_query(tb)
    assert(type(tb)=="table","tb must be a table")
    local t = {}
    for k,v in pairs(tb) do
        if (istable(v)) then
            print("table")
            //table.insert(t,NM.url.escape(tostring(k)) .. "=" .. NM.url.http_build_query(v))
        else
            table.insert(t,NM.url.escape(tostring(k)) .. "=" .. NM.url.escape(tostring(v)))
        end
    end
    return table.concat(t,'&')
end
http_build_query = NM.url.http_build_query