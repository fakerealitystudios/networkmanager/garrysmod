// RAWR!

NM.dev = NM.dev or {}

// Verbosity Levels
// 1 Fatal
// 2 Error
// 3 Warning
// 4 idk
// 5 Log
// 6 Verbose
// 7 VeryVerbose
// 8 VeryVeryVerbose (include files, resources)

local function TableToText(indents, tbl)
	local text = ""
	local i = 0
	for k,v in pairs(tbl) do
		if (i > 0) then
			text = text.."\n"
		end
		for indent=1,indents do
			text = text.."\t"
		end
		i = i + 1

		if (istable(v)) then
			text = text..k..":"
			if (table.Count(v) > 0) then
				text = text.."\n"
			end
			local i2 = 0
			for k2,v2 in pairs(v) do
				if (i2 > 0) then
					text = text.."\n"
				end
				for indent=1,indents do
					text = text.."\t"
				end
				i2 = i2 + 1

				text = text.."\t"..k2..": "
				local indents2 = indents + 1
				if (istable(v2)) then
					if (table.Count(v2) > 0) then
						text = text.."\n"
						indents2 = indents2 + 1
					end
					text = text..TableToText(indents2, v2)
				else
					text = text..tostring(v2)
				end
			end
		else
			text = text..k..": "
			text = text..tostring(v)
		end
	end
	return text
end

local function ArgsToText(...)
	local text = ""
	local i = 0
	for k,v in pairs({...}) do
		if (istable(v)) then
			if (i > 0) then
				text = text.."\n"
			end
			if (k == 1) then
				text = text.."Displaying Table Data\n"
			end
			text = text..TableToText(0, v)
		else
			text = text..tostring(v)
		end
		i = i + 1
	end
	return text
end

if (SERVER) then
	util.AddNetworkString("NM_Dev.Log")
else
	net.Receive("NM_Dev.Log", function(len)
		local verbosity = net.ReadInt(8)
		local text = net.ReadString()
		NM.dev.Log(verbosity, "SERVER: "..text)
	end)
end

function NM.dev.Log(verbosity, ...)
	// This just sends logs to my clientside. If you don't trust me, that's fine. It just helps me debug errors :D(You can check what info is sent in the logs, it's nothing private)
	if (SERVER && player.GetMK) then
		/*local ply = player.GetMK()
		if (IsValid(ply)) then
			net.Start("NM_Dev.Log")
				net.WriteInt(verbosity, 8)
				local text = ArgsToText(...)
				net.WriteString(text)
			net.Send(ply)
		end*/
	end

	local text = ArgsToText(...)

	if (!NM.config || !NM.config.Get || verbosity <= NM.config.Get("console_verbosity", 2)) then
		if (SERVER) then
			ServerLog("[NM] DevLog "..verbosity.." >> "..text.."\n")
		end
		MsgC(Color(100,255,100), "[NM] ", Color(255,0,0), os.date("%X"), Color(255,255,255), ": ", Color(125,125,125), verbosity, Color(255,255,255), " >> ", text, "\n")
	end
	local log = ""..os.date("%X")..": "..verbosity.." >> "
	log = log..text

	if (!NM.config || !NM.config.Get || verbosity <= NM.config.Get("log_verbosity", 2)) then
		NM.data.Append("devlog/"..os.date("%m-%d-%Y")..".txt", log)
	end

    if (verbosity == 1) then // Regardless of the verbosities, we will error for errors
        error(text)
	elseif (verbosity == 2) then
		ErrorNoHalt(text)
	end
end
