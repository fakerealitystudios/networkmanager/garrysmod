// RAWR!

NM.config = NM.config or {}
NM.config.fileHandle = "data/configurations.txt"
NM.config.defaultFileHandle = "data/configurations_defaults.txt"
NM.config.list = NM.config.list or {}
NM.config.buffer = NM.config.list

if (!NM.meta.Config) then
	NM.util.IncludeInternal("meta/sh_config.lua")
	if (NM.meta.Config.SetValue) then
		print("Config Meta")
	else
		print("No Config Meta")
	end
end
if (CLIENT) then
	NM.config.networked = NM.config.networked or false
	function NM.config.NetworkPlayer(ply, configs)
		NM.config.networked = true // Set beforehand so hooks can use this value
		for k,tbl in pairs(configs) do
			NM.config.Set(tbl[1], tbl[2], tbl[3], tbl[4], tbl[5])
		end
	end
else
	function NM.config.NetworkPlayer(ply)
		local configs = {}
		for k,v in pairs(NM.config.list) do
			if (v.noNetworking) then continue end
			table.insert(configs, {k, v:GetValue(), false, true, v.data})
		end
		return configs
	end
end

function NM.config.Get(parameter, default)
    if (NM.config.list[parameter]) then
        return NM.config.list[parameter]:Get()
    end
	return default
end

function NM.config.PassiveSet(parameter, value, save, force, data)
	if (!NM.config.list[parameter]) then
		ErrorNoHalt("Couldn't find config parameter: "..parameter..":"..tostring(value).."\n")
		return
	end
	// Only set when the value is the default(Use saved values rather then hard coded ones)
	if (NM.config.list[parameter]:IsDefault()) then
		NM.config.Set(parameter, value, save, force, data)
	end
end

function NM.config.Set(parameter, value, save, force, data)
	if (!NM.config.list[parameter]) then
		ErrorNoHalt("Couldn't find config parameter: "..parameter..":"..tostring(value).."\n")
		return
	end
	if (value == NM.config.list[parameter].value && (data == nil || data == NM.config.list[parameter].data)) then // already the same, leave it
		return
	end
	NM.dev.Log(7, "Updating parameter: "..parameter..":"..tostring(value))
	if (data) then
		NM.config.list[parameter]:SetData(data)
	end
	for k,v in pairs(NM.config.list[parameter].hooks) do
		v(value, NM.config.list[parameter]:GetValue(), NM.config.list[parameter]:GetData())
	end
	NM.config.list[parameter]:SetValue(value, force)
	NM.config.list[parameter].isdefault = false
	if (SERVER) then
		if (save) then
			local tbl = {}
			tbl.value = NM.config.list[parameter].value
			tbl.data = NM.config.list[parameter].data
			NM.data.SetData(NM.config.fileHandle, parameter, tbl)
		end
		if (!NM.config.list[parameter].noNetworking && NM.net) then
			NM.net.BroadcastLib("config")
		end
	else
		if (save && NM.config.list[parameter]:GetData().clientMalleable) then
			local tbl = {}
			tbl.value = NM.config.list[parameter].value
			tbl.data = NM.config.list[parameter].data
			NM.data.SetData(NM.config.fileHandle, parameter, tbl)
		end
	end
end

function NM.config.Add(parameter, value, callback, data, noNetworking)
	if (NM.config.list[parameter]) then
		if (SERVER && !NM.initialized) then
			error("Configuration parameter already created! Use Set to update its value! '"..parameter.."'")
		end
		return
	end
	NM.dev.Log(7, "Adding config: "..parameter..":"..tostring(value))
	if (parameter==nil||value==nil) then
		ErrorNoHalt("Couldn't add configuration, missing parameter or value! '"..parameter.."'")
		return
	end
	local config = {}
	config.value = value
	config.default = {value, data or {}}
	config.isdefault = true
	config.callback = callback or function() return true end
	config.data = data or {}
	config.noNetworking = noNetworking or false
	config.hooks = {}
	setmetatable(config, NM.meta.Config)
	config:SetValue(value, true)
	NM.config.list[parameter] = config

	local oldDefaultConfig = NM.data.GetData(NM.config.defaultFileHandle, nil, parameter)
	local needsUpdate = false
	if (oldDefaultConfig) then
		if (oldDefaultConfig.value != NM.config.list[parameter].default[1] || oldDefaultConfig.data == NM.config.list[parameter].default[2]) then
			needsUpdate = true
			local tbl = {}
			tbl.value = NM.config.list[parameter].default[1]
			tbl.data = NM.config.list[parameter].default[2]
			NM.data.SetData(NM.config.defaultFileHandle, parameter, tbl)
		end
	elseif (SERVER || (CLIENT && (noNetworking || (data && data.clientMalleable)))) then
		local tbl = {}
		tbl.value = NM.config.list[parameter].default[1]
		tbl.data = NM.config.list[parameter].default[2]
		oldDefaultConfig = tbl
		NM.data.SetData(NM.config.defaultFileHandle, parameter, tbl)
	end

	local savedConfig = NM.data.GetData(NM.config.fileHandle, nil, parameter)
	if (savedConfig) then
		local tbl = {}
		tbl.value = savedConfig.value
		tbl.data = savedConfig.data
		if ((savedConfig.default != nil || needsUpdate) && oldDefaultConfig) then
			if (savedConfig.value == oldDefaultConfig.value) then
				tbl.value = NM.config.list[parameter].value
			end
			if (savedConfig.data == oldDefaultConfig.data) then
				tbl.data = NM.config.list[parameter].data
			end
			NM.data.SetData(NM.config.fileHandle, parameter, tbl)
		end
		NM.config.Set(parameter, tbl.value, false, false, tbl.data) // Don't force, if the config has an issue, it should stop the config setting
	elseif (SERVER || (CLIENT && (noNetworking || (data && data.clientMalleable)))) then
		// Has no saved data yet! Let's store it so they can edit it
		local tbl = {}
		tbl.value = NM.config.list[parameter].value
		tbl.data = NM.config.list[parameter].data
		NM.data.SetData(NM.config.fileHandle, parameter, tbl)
	end
end

function NM.config.Hook(parameter, uid, callback)
	if (!NM.config.list[parameter]) then
		error("Couldn't find config parameter: '"..parameter.."' to create hook '"..uid.."'\n")
	end
	callback(NM.config.list[parameter]:GetValue())
	NM.config.list[parameter].hooks[uid] = callback
end

function NM.config.RemoveHook(parameter, uid)
	if (!NM.config.list[parameter]) then
		error("Couldn't find config parameter: "..parameter..":"..tostring(value).."\n")
	end
	NM.config.list[parameter].hooks[uid] = nil
end