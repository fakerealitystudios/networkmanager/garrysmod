// RAWR!

// All of your wonderful DRMs and server-sided backdoors like to bother me, so fuck you

// In case I need it again
/*
if (string.find(debug.traceback(), "XEON DRM")) then
    print("Xeon DRM getinfo for: ", tostring(funcOrStackLevel))
    PrintTable(result)
end
*/

NM.lua_overrides = NM.lua_overrides or {}
NM.lua_overrides.list = NM.lua_overrides.list or {}
NM.lua_overrides.oldFns = NM.lua_overrides.oldFns or {}

function NM.lua_overrides.Add(name, fn)
    local _name = string.Explode(".", name)
    local oldfn = nil
    if (!NM.lua_overrides.oldFns[name]) then
		local tbl = _G
		for k,v in pairs(_name) do
			tbl = tbl[v]
		end
		oldfn = tbl
        NM.lua_overrides.oldFns[name] = oldfn
    else
		oldfn = NM.lua_overrides.oldFns[name]
	end
    if (SwiftAC__N) then
        local tbl = SwiftAC__N
        for k,v in pairs(_name) do
            tbl = tbl[v]
        end
        SwiftAC__N = fn
    end
    local tbl = _G
	local i = 1
    for k,v in pairs(_name) do
		if (i == table.Count(_name)) then
        	tbl[v] = function(...)
				return fn(oldfn, ...)
			end
			NM.lua_overrides.list[tbl[v]] = oldfn
		else
			tbl = tbl[v]
			i = i + 1
		end
    end
end

NM.lua_overrides.Add("debug.getinfo", function(oldfn, funcOrStackLevel, fields)
    if (NM.lua_overrides.list[funcOrStackLevel]) then
        return oldfn(NM.lua_overrides.list[funcOrStackLevel], fields)
    end
	return oldfn(funcOrStackLevel, fields)
end)

NM.lua_overrides.Add("jit.util.funcinfo", function(oldfn, func, pos)
    if (NM.lua_overrides.list[func]) then
        return oldfn(NM.lua_overrides.list[func], pos)
    end
	return oldfn(func, pos)
end)

NM.lua_overrides.Add("tostring", function(oldfn, val)
    if (NM.lua_overrides.list[val]) then
        return oldfn(NM.lua_overrides.list[val])
    end
	return oldfn(val)
end)

NM.lua_overrides.Add("RunString", function(oldfn, code, identifier, handleError)
	if (SERVER) then
	    local trace = debug.traceback()
		NM.log.Log(NM.log.LogType.SERVER, "runstring", nil, {identifier or "nil", code, trace})
	end
	return oldfn(code, identifier, handleError)
end)

NM.lua_overrides.Add("RunStringEx", function(oldfn, code, identifier, handleError)
	if (SERVER) then
	    local trace = debug.traceback()
		NM.log.Log(NM.log.LogType.SERVER, "runstring", nil, {identifier or "nil", code, trace})
	end
	return oldfn(code, identifier, handleError)
end)