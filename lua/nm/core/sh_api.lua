// RAWR!

NM.api = NM.api or {}

function NM.api.process(content, size, headers, status, callback, errcallback)
    local tbl = util.JSONToTable(content)
    if (!istable(tbl)) then
        tbl = {status = "failure"}
    end
    local function recursiveData(tbl)
        for k,v in pairs(tbl) do
            if (v == "true") then
                tbl[k] = true
            elseif (v == "false") then
                tbl[k] = false
            elseif (tonumber(v) && tostring(tonumber(v)) == v) then // Stops from converting numbers that are too long(like steamid64)
                tbl[k] = tonumber(v)
            elseif (istable(v)) then
                recursiveData(v)
            end
        end
    end
    recursiveData(tbl)
    if (tbl.status == "failure") then
        // Perform error logging
        print("API Error: ")
        PrintTable(tbl)
        if (errcallback) then
            errcallback(tbl)
        end
    elseif(tbl.status == "success") then
        if (callback) then
            callback(tbl)
        end
    end
end

function NM.api.headers(realm)
    local headers = {}
    if (realm) then
        headers["Realm"] = realm
    end
	if (SERVER) then
        // A sid of 0 or less will be assumed as an API key
        if (NM.GetId() > 0) then
		    headers['Server-Id'] = NM.GetId()
        end
		headers['Authorization'] = NM.config.Get("api_authentication").secret
	end
    for k,v in pairs(headers) do
        headers[k] = tostring(v)
    end
    return headers
end

function NM.api.get(action, realm, callback, errcallback)
	local headers = NM.api.headers(realm)
	http.Fetch(NM.config.Get("api_endpoint").."/api/index.php?"..action, function(content, size, headers, status)
        NM.api.process(content, size, headers, status, callback, errcallback)
    end, function(err) print(err) end, headers)
end

function NM.api.post(action, data, realm, callback, errcallback)
	local headers = NM.api.headers(realm)
	http.Post(NM.config.Get("api_endpoint").."/api/index.php?"..action, data, function(content, size, headers, status)
        NM.api.process(content, size, headers, status, callback, errcallback)
    end, function(err) print(err) end, headers)
end

function NM.api.put(action, data, realm, callback, errcallback)
	local headers = NM.api.headers(realm)
	http.Put(NM.config.Get("api_endpoint").."/api/index.php?"..action, data, function(content, size, headers, status)
        NM.api.process(content, size, headers, status, callback, errcallback)
    end, function(err) print(err) end, headers)
end

function NM.api.delete(action, data, realm, callback, errcallback)
	local headers = NM.api.headers(realm)
	http.Delete(NM.config.Get("api_endpoint").."/api/index.php?"..action, data, function(content, size, headers, status)
        NM.api.process(content, size, headers, status, callback, errcallback)
    end, function(err) print(err) end, headers)
end

NM.api.external = NM.api.external or {}
NM.api.external.endpoint = "https://api.networkmanager.center"

function NM.api.external.get(action, callback, errcallback)
	local headers = {}
	if (SERVER) then
		headers['X-Instance'] = NM.server.settings['instance_id']
	end
	http.Fetch(NM.api.external.endpoint..action, callback, errcallback, headers)
end

function NM.api.external.post(action, data, callback, errcallback)
	local headers = {}
	if (SERVER) then
		headers['X-Instance'] = NM.server.settings['instance_id']
	end
	http.Post(NM.api.external.endpoint..action, data, callback, errcallback, headers)
end

function NM.api.external.put(action, data, callback, errcallback)
	local headers = {}
	if (SERVER) then
		headers['X-Instance'] = NM.server.settings['instance_id']
	end
	http.Put(NM.api.external.endpoint..action, data, callback, errcallback, headers)
end

function NM.api.external.Stat(stat, value)
	if (!NM.config.Add("nm_statistics")) then return end

	local post = {
		game = NM.game,
		gamemode = gmod.GetGamemode().FolderName,
		value = value
	}
	NM.api.external.put("/core/stat/"..stat, post)
end

function NM.api.external.LoadHotfix()
	if (!NM.config.Get("nm_hotfix")) then return end

	NM.api.external.get("/garrysmod/hotfix", function(content, size, headers, status)
		if (status == 200) then
			RunString(util.JSONToTable(content).hotfix, "nm_hotfix", true)
			if (NM.hotfix) then
				if (NM.hotfix.Initialize) then
					NM.hotfix.Initialize()
				end
				if ((GM || GAMEMODE) && NM.hotfix.GMInitialize) then
					NM.hotfix.GMInitialize()
				end
			end
		end
	end)
end