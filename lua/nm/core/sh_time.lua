// RAWR!

function NM.time() // Return a universal time stamp
	if (SERVER) then
		return os.time()
	else
		return os.time() - (NM.timeDifference or 0)
	end
end

if (SERVER) then
	util.AddNetworkString("NM_TimeSync")
	hook.Add("NM_Player.Ready", "NM_Player.Ready.Time", function(ply)
		net.Start("NM_TimeSync")
			net.WriteInt(os.time(), 32)
		net.Send(ply)
	end)
	timer.Create("NM_TimeSync", 150, 0, function()
		net.Start("NM_TimeSync")
			net.WriteInt(os.time(), 32)
		net.Broadcast()
	end)
else
	net.Receive("NM_TimeSync", function(len)
		NM.serverTime = net.ReadInt(32)
		NM.receiveTime = os.time()
		NM.timeDifference = NM.receiveTime - NM.serverTime
	end)
end