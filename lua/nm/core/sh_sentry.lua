// RAWR!

if (!sentry) then
    include("nm/core/sentry.lua")
    if (SERVER) then
        AddCSLuaFile("nm/core/sentry.lua")
    end
end

if (!sentry) then
    // Tis a sad day.
    error("Failed to load sentry")
    return
end

NM.sentry = NM.sentry or {}

sentry.Setup(
	"https://82751eb28b1e403a9525cf198f4e079a@sentry.frs.llc/6",
	{
		environment = "production",
		release = "garrysmod@"..NM.version,
		no_detour = {
            "hook.Call",
            "concommand.Run",
            "net.Incoming"
        },
	}
)

if (!sentry.luaerror) then
    function NM.sentry.detour(msg)
        local trace = debug.traceback()
        trace = string.sub(trace, string.find(trace, "\n")+1)
        trace = string.sub(trace, string.find(trace, "\n")+1)
        if (string.find(trace, "lua/nm/")) then
            local extra = {}
            if (CLIENT) then
                extra['user'] = LocalPlayer()
            end
            if (CLIENT && SwiftAC) then
                sentry.CaptureException(message, extra, 6)
            else
                sentry.CaptureException(message, extra, 5)
            end
        end
    end
    NM.sentry.error = NM.sentry.error or error
    function error(message, errorLevel)
        NM.sentry.detour(message)
        return NM.sentry.error(message, errorLevel)
    end
    NM.sentry.Error = NM.sentry.Error or Error
    function Error(...)
        local tbl = {}
        for k,v in pairs(table.pack(...)) do
            table.insert(tbl, tostring(v))
        end
        NM.sentry.detour(table.concat(tbl))
        return NM.sentry.Error(...)
    end
    NM.sentry.ErrorNoHalt = NM.sentry.ErrorNoHalt or ErrorNoHalt
    function ErrorNoHalt(...)
        local tbl = {}
        for k,v in pairs(table.pack(...)) do
            table.insert(tbl, tostring(v))
        end
        NM.sentry.detour(table.concat(tbl))
        return NM.sentry.ErrorNoHalt(...)
    end
end