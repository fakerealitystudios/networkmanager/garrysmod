// RAWR!

NM.data = NM.data or {}
NM.data.mapHandle = "data/maps/"..game.GetMap().."/"

file.CreateDir(NM.folderName)

function NM.data.Append(handle, text)
	local folders = string.Explode("/", handle)
	folders[#folders] = nil
	local folder = NM.folderName
	for k,v in pairs(folders) do
		folder = folder.."/"..v
		file.CreateDir(folder)
	end
	local text = text.."\n"
	local handle = NM.folderName.."/"..handle
	if (file.Exists(handle, "DATA")) then
		file.Append(handle, text)
	else
		file.Write(handle, text)
	end
end

function NM.data.SetData(handle, index, value)
	local folders = string.Explode("/", handle)
	folders[#folders] = nil
	local folder = NM.folderName
	for k,v in pairs(folders) do
		folder = folder.."/"..v
		file.CreateDir(folder)
	end
	local arr = {}
	if (index) then
		arr = NM.data.GetData(handle, {})
	end
	if (index) then
		arr[index] = value
	else
		arr = value
	end
	file.Write(NM.folderName.."/"..handle, util.TableToJSON(arr, true))
end

function NM.data.GetData(handle, default, index)
	if (file.Exists(NM.folderName.."/"..handle, "DATA")) then
		local arr = util.JSONToTable(file.Read(NM.folderName.."/"..handle, "DATA") or "[]") or {}
		if (!istable(arr)) then
			return default
		end
		if (!index) then
			return arr
		elseif (arr[index]) then
			return arr[index]
		end
	end
	return default
end
