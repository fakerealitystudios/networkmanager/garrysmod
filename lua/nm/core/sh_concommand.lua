// RAWR!

NM.concommand = NM.concommand or {}
// List of commands to not send to server/hook
// Only do commands that are spammy/not important!
NM.concommand.blacklist = {
	// gmod
	"+menu",
	"-menu",
	"+menu_context",
	"-menu_context",
	// pac3
	"glib_request_pack",
	"pac_request_outfits",
	// wiremod
	"E2_StartChat",
	"E2_FinishChat",
	"EGP_ScrWH",
	"wire_expression2_friend_status",
	// Leyworkshopdls
	"leyworkshopdls_feedme",
	// sandbox
	"gmod_undo",
	"gmod_tool",
	"gm_spawn",
	"gm_spawnsent",
	// darkrp
	"_sendDarkRPvars",
	"_DarkRP_AnimationMenu",
	"_hobo_emitsound",
	// Jailbreak
	"jb_spec_changemode",
	"jb_spec_next",
	"jb_spec_prev",
	// fpp
	"FPP_sendblocked",
	// ulx
	"ulib_cl_ready",
	"ulib_update_cvar",
	"_xgui",
	"xgui",
	// SwiftAC shit
	"camper13",
	"external",
	"neko_setstatus",
	"exploits_open",
	"pp_texturize_scale"
}

// There is not much point in localizing it. If clients want to bypass it, they simply can. We're not an anti-cheat, we're just trying to provide good information and catch what we can.
// Even if we do localize it, this code will go public at some point and hacks will bypass it nonetheless. Not worth trying, as I said, this is no anticheat

NM.concommand.old_concommandRun = NM.concommand.old_concommandRun or concommand.Run
function concommand.Run(ply, cmd, args, argStr)
	if (!table.HasValue(NM.concommand.blacklist, cmd) && !(cmd:lower() == "darkrp" && string.find(argStr, "buy"))) then
		hook.Run("NM_Concommand.Run", ply, cmd, args, argStr, (CLIENT and true or false))
		if (CLIENT) then
			net.Start("NM_Concommand.Run")
				net.WriteString(cmd)
				net.WriteTable(args)
				net.WriteString(argStr)
			net.SendToServer()
		end
	end
	NM.concommand.old_concommandRun(ply, cmd, args, argStr)
end

if (SERVER) then
	util.AddNetworkString("NM_Concommand.Run")
	net.Receive("NM_Concommand.Run", function(len, ply)
		hook.Run("NM_Concommand.Run", ply, net.ReadString(), net.ReadTable(), net.ReadString(), true)
	end)
end

NM.concommand.old_gameConsoleCommand = NM.concommand.old_gameConsoleCommand or game.ConsoleCommand
function game.ConsoleCommand(str)
	NM.concommand.old_gameConsoleCommand(str)
end

NM.concommand.old_runConsoleCommand = NM.concommand.old_runConsoleCommand or RunConsoleCommand
function RunConsoleCommand(str, ...)
	NM.concommand.old_runConsoleCommand(str, ...)
end