// RAWR!

local Player = FindMetaTable("Player")

Player.old_SteamID = Player.old_SteamID or Player.SteamID
function Player:NM_RealSteamID()
	return self:old_SteamID()
end

function Player:SteamID()
	return self:NM_IsDisguised() and self:NM_TempData_Get("disguise", "steamid") or self:NM_RealSteamID()
end

Player.old_SteamID64 = Player.old_SteamID64 or Player.SteamID64
function Player:NM_RealSteamID64()
	return self:old_SteamID64()
end

function Player:SteamID64()
	return self:NM_IsDisguised() and self:NM_TempData_Get("disguise", "steamid64") or self:NM_RealSteamID64()
end

Player.old_UniqueID = Player.old_UniqueID or Player.UniqueID
function Player:NM_RealUniqueID()
	return self:old_UniqueID()
end

function Player:UniqueID()
	return self:NM_IsDisguised() and self:NM_TempData_Get("disguise", "uniqueid") or self:NM_RealUniqueID()
end

Player.old_SetPData = Player.old_SetPData or Player.SetPData
function Player:SetPData(key, value)
	if (!SERVER) then
		return self:old_SetPData(key, value)
	end
	if (self:NM_IsDisguised()) then
		self:SendLua([[error("Not allowed to set other player's data. That's rude.")]])
		return
	end
	return self:old_SetPData(key, value)
end