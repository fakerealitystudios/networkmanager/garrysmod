-- RAWR!
local Player = FindMetaTable("Player")

if (SERVER) then
    function Player:NM_Permission_Load(db_values)
        for k, v in pairs(db_values) do
            local realm = self:NM_Realm_Get(v.realm).__perms
            table.insert(realm, v.perm)
            self:NM_Realm_Get(v.realm).__perms = realm
        end
    end

    util.AddNetworkString("NM_Player.Permission")

    function Player:NM_Permission_Give(fullPerm, fullRealm)
        local fullRealm = fullRealm or NM.realm
        local realm = self:NM_Realm_Get(fullRealm).__perms

        if (not table.HasValue(realm, fullPerm)) then
            table.insert(realm, fullPerm)

            NM.api.put("/member/"..self:NM_GetId().."/permission", {perm = fullPerm}, realm)
        end
    end

    function Player:NM_Permission_Take(fullPerm, fullRealm)
        local fullRealm = fullRealm or NM.realm
        local realm = self:NM_Realm_Get(fullRealm).__perms

        if (table.HasValue(realm, fullPerm)) then
            table.RemoveByValue(realm, fullPerm)

            NM.api.delete("/member/"..self:NM_GetId().."/permission", {perm = fullPerm}, realm)
        end
    end
end

if (CLIENT) then
    net.Receive("NM_Player.Permission", function(len) end)
end

function Player:NM_Permission_Has(fullPerm, fullRealm)
    if (SERVER && !table.HasValue(NM.permissions, fullPerm)) then
        table.insert(NM.permissions, fullPerm)
        NM.api.put("/server/"..NM.GetId().."/permission", {perm = fullPerm, game = NM.game})
    end
    local fullRealm = fullRealm or NM.realm
    local realm = self:NM_Realm_GetInclusive(fullRealm).__perms
    local permSplit = string.Explode(".", fullPerm)

    -- Check if any perms are negated first, they take priority
    if (table.HasValue(realm, "!" .. fullPerm)) then
        return false
    elseif (string.sub(fullPerm, -1) == "%") then
        local perm = "!" .. string.sub(fullPerm, 1, -2) -- Remove the wildcard

        for _, realmPerm in pairs(realm) do
            if (string.sub(realmPerm, 1, string.len(perm)) == perm) then
                return false
            end
        end
    else
        local perm = "*"

        for _, permPart in pairs(permSplit) do
            if (table.HasValue(realm, "!" .. perm)) then
                return false
            else
                perm = string.sub(perm, 1, -2)
                perm = perm .. permPart .. NM.permissionDelimiter .. "*"
            end
        end
    end

    -- Now check if we do have the perm
    if (table.HasValue(realm, fullPerm)) then
        return true
    elseif (string.sub(fullPerm, -1) == "%") then
        local perm = string.sub(fullPerm, 1, -2) -- Remove the wildcard

        for _, realmPerm in pairs(realm) do
            if (string.sub(realmPerm, 1, string.len(perm)) == perm) then
                return true
            end
        end
    else
        local perm = "*"

        for _, permPart in pairs(permSplit) do
            if (table.HasValue(realm, perm)) then
                return true
            else
                perm = string.sub(perm, 1, -2)
                perm = perm .. permPart .. NM.permissionDelimiter .. "*"
            end
        end
    end

    if (NM.ranks.Permission_Has(self:NM_Rank_GetRealRank(), fullPerm, fullRealm)) then
        return true
    elseif (self.nm and table.Count(self.nm.subranks) > 0) then
        for k, plyrank in pairs(self.nm.subranks) do
            if (NM.ranks.Permission_Has(plyrank, fullPerm, fullRealm)) then return true end
        end
    end

    return nil
end