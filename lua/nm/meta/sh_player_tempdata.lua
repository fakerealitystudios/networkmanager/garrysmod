// RAWR!

if (SERVER) then
	util.AddNetworkString("NM_TempData_Set")
end

local Player = FindMetaTable("Player")

function Player:NM_TempData_Set(value, ...)
    local indexes = table.pack(...)
    local ldata = self.nm.tempdata
    local i = 0
    local count = table.Count(indexes)
    for _,index in pairs(indexes) do
        if (!ldata[index]) then
            ldata[index] = {}
        end
        i = i + 1
        if (i == count) then
            ldata[index] = value
        else
            ldata = ldata[index]
        end
    end
	if (SERVER) then
		net.Start("NM_TempData_Set")
			net.WriteEntity(self)
			net.WriteTable(self.nm.tempdata)
		net.Broadcast()
	end
end

if (CLIENT) then
	net.Receive("NM_TempData_Set", function(len)
		local ply = net.ReadEntity()
		if (!IsValid(ply)) then return end
		ply.nm = ply.nm or {}
		ply.nm.tempdata = net.ReadTable()
	end)
end

function Player:NM_TempData_Get(...)
    local indexes = table.pack(...)
    if (!self.nm) then return nil end
    local ldata = self.nm.tempdata
    for _,index in pairs(indexes) do
        if (ldata[index] != nil) then
            ldata = ldata[index]
        else
            return nil
        end
    end
    if (istable(ldata)) then
        return table.Copy(ldata) // That way we can see the difference when Set is run
    end
    return ldata
end
