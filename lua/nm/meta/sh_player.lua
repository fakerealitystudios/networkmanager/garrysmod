// RAWR!

NM.meta.Player = NM.meta.Player or {}
NM.meta.Player.__index = NM.meta.Player
NM.meta.Player.__type = "NM_Player"

local Player = FindMetaTable("Player")
for k,v in pairs(Player) do
	if (isfunction(v)&&k:find("NM_")) then
		NM.meta.Player[k] = v
	end
end
Player = nil
Player = NM.meta.Player

function Player:__tostring()
	return "NMPlayer ["..(self.steamid or 0).."]["..(self:Name()).."]"
end

function Player:__eq(other)
	return self:SteamID64() == other:SteamID64()
end

function Player:IPAddress()
	return self.ip or ""
end

function Player:IsBot()
	return false
end

function Player:IsPlayer()
	return false
end

function Player:IsExternalPlayer()
	return true
end

function Player:SteamID()
	return self.steamid or ""
end
Player.SteamID = Player.SteamID

function Player:SteamID64()
	return self.steam or ""
end
Player.SteamID64 = Player.SteamID64

function Player:IsFullyAuthenticated()
    return true
end

function Player:Name()
	return (self.nm and self.nm.name) or self.name or self.steamid or ""
end
Player.old_Nick = Player.Name
Player.GetName = Player.Name
Player.Nick = Player.Name

NM.meta.Player = Player
