// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:NM_DisplayVersions()
		self:SendLua("LocalPlayer():NM_DisplayVersions()")
	end
else
	function Player:NM_DisplayVersions()
		self:NM_SendChat(Color(100,255,100), "[NM-VC] ", Color(255,255,255), "NetworkManager version "..NM.version.." loaded!")

		for k,v in pairs(NM) do
			if (istable(v)&&v.version) then
				local title = v.title or k:sub(1,1):upper()..k:sub(2)
				self:NM_SendChat(Color(100,255,100), "[NM-VC] ", Color(255,255,255), "NM "..title.." "..v.version.." loaded!")
			end
		end
	end
end
