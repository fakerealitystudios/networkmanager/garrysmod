// RAWR!

local Player = FindMetaTable("Player")

function Player:NM_GetId()
    if (SERVER && !self:IsFullyAuthenticated()) then return -1 end
    return self.nm and self.nm.id or -1
end

function Player:NM_GetSessionId()
    return self.nm and self.nm.sessionid or -1
end

function Player:NM_GetName()
    return self.nm and self.nm.displayName or self:Name()
end
