// RAWR!

local Player = FindMetaTable("Player")

function Player:NM_Realm_Get(fullRealm)
    if (!self.nm) then return NM.meta.Realm.new() end
    return NM.realms.get(self.nm.realms, fullRealm)
end

// Used for permissions mostly... Will collect all data together, so don't try to set with it! You won't have references
function Player:NM_Realm_GetInclusive(fullRealm)
    if (!self.nm) then return NM.meta.Realm.new() end
    return NM.realms.getInclusive(self.nm.realms, fullRealm)
end
