// RAWR!

local META = NM.meta.Command or {}
META.__index = META
META.__type = "nmcommand"
META.canConsole = false

// Specifies that this command is safe to be used from console
function META:ConsoleSafe()
	self.canConsole = true
end

function META:NoChat()
	self.noChat = true
end

function META:NoConsole()
	self.noConsole = true
end

function META:AddParam(arg, hint, required, args)
	if (arg) then
		table.insert(self.args, {hint, required, arg.prep(args or {}), arg})
	end
end

function META:SetOpposite(cmd, args, aliases)
	self.opposite = {cmd, args, aliases}
end

function META:SetHelp(str)
	self.help = str
end

NM.meta.Command = META
