// RAWR!

NM.meta.Realm = NM.meta.Realm or {}
NM.meta.Realm.__index = NM.meta.Realm
NM.meta.Realm.__type = "nm_realm"

local Realm = NM.meta.Realm

function Realm.new()
    local obj = {}
    obj.__data = {}
    obj.__perms = {}
    setmetatable(obj, NM.meta.Realm)
    return obj
end

NM.meta.Realm = Realm
