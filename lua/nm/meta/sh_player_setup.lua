// RAWR!

local Player = FindMetaTable("Player")

function Player:NM_InitializePlayer()
	//NM.dev.Log(7, "<NM_Player.SetupVars> Setup player variables for "..tostring(self))
	self.nm = {}
	self.nm.id = -1
	self.nm.sessionid = -1
	self.nm.rankdata = {}
	self.nm.rank = NM.ranks.defaultRank
	self.nm.subranks = {}
	self.nm.realms = {}
	self.nm.data = {}
	self.nm.tempdata = {}
end

function Player:NM_TransferPlayer(ply)
	//NM.dev.Log(7, "<NM_Player.SetupVars> Setup player variables for "..tostring(self))
	self.nm.id = ply.nm.id
	self.nm.sessionid = ply.nm.sessionid
	self.nm.rankdata = ply.nm.rankdata
	self.nm.rank = ply.nm.rank
	self.nm.subranks = ply.nm.subranks
	self.nm.realms = ply.nm.realms
	self.nm.data = ply.nm.data
	self.nm.tempdata = ply.nm.tempdata
end
