// RAWR!

local Player = FindMetaTable("Player")

function Player:NM_DB_LoadData(externalLoad)
	if (!self.nm) then
		self:NM_InitializePlayer() // Run before the player's data is loaded
	end

	if (self:IsBot()) then
		hook.Run("NM_Player.Loaded", self)
		return
	end

	local ipaddress = string.Explode(":", self:IPAddress())
	table.remove(ipaddress) // removes the last element
	ipaddress = table.concat(ipaddress, ":") // This is in-case we ever use IPv6

    NM.api.put("/member", {
        type = "steamid64",
        token = self:SteamID64(),
        displayName = self:Name()
    }, nil, function(tbl)
        if (IsValid(self) || externalLoad) then
            self.nm.id = tbl.member.nmid
            self.nm.name = tbl.member.name
            self:NM_Rank_Load(tbl.member.ranks)
            //self:NM_Data_Load(tbl.member.data)
            self:NM_Permission_Load(tbl.member.permissions)
            if (!externalLoad) then
                NM.api.put("/member/"..tbl.member.nmid.."/session", {ip = ipaddress, displayName = displayName}, nil, function(tbl)
                    self.nm.sessionid = tbl.usid
                end)
            end
            hook.Run("NM_Player.Loaded", self, false)
        end
    end)
end