// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	local nilIdentifier = "_nil"
	local function encode(tbl, index, deleteIndexes)
		local result = {}
		local deleteIndexes = deleteIndexes or {}
		if (!istable(tbl)) then
			result[index] = tbl
		else
			if (table.Count(tbl) == 0) then
				table.insert(deleteIndexes, index)
			end
			for k,v in pairs(tbl) do
				local vid = index..NM.dataDelimiter..k
				if (istable(v)) then
					local result2, deleteIndexes2 = encode(v, vid, deleteIndexes)
					table.Merge(result, result2)
					table.Merge(deleteIndexes, deleteIndexes2)
				elseif (v == nil || v == nilIdentifier) then
					table.insert(deleteIndexes, vid)
				else
					result[vid] = v
				end
			end
		end
		return result, deleteIndexes
	end

	local function decode(vid, value)
		local result = {}
		local id = vid[1]
		table.remove(vid, 1)
		if (table.Count(vid) > 0) then
			result[id] = decode(vid, value)
		else
			if (value == "false") then
				value = false
			elseif (value == "true") then
				value = true
			elseif (tonumber(value)) then
				value = tonumber(value)
			end
			result[id] = value
		end
		return result
	end

	function Player:NM_Data_Load(db_values)
		for k,v in pairs(db_values) do
			local vid = string.Explode(NM.dataDelimiter, v.vid)
			local realm = self:NM_Realm_Get(v.realm).__data
			table.Merge(realm, decode(vid, v.value))
		end
	end

	util.AddNetworkString("NM_Player.Data")
	function Player:NM_Data_Set(fullRealm, value, ...)
		local indexes = table.pack(...)
		local realm = self:NM_Realm_Get(fullRealm).__data
		local data = {}
		local ldata = data
		local i = 0
		local count = table.Count(indexes)
		for _,index in pairs(indexes) do
			if (!ldata[index]) then
				ldata[index] = {}
			end
			i = i + 1
			if (i == count) then
				ldata[index] = value
			else
				ldata = ldata[index]
			end
		end
		local diffTbl = NM.util.GetDifference(data, realm)
		if (table.Count(diffTbl) == 0) then return false end
		table.Merge(realm, diffTbl)
        NM.api.put("/member/"..self:NM_GetId().."/data/"..string.Implode("/", indexes), {value = value})
		return true
	end
end

if (CLIENT) then
	net.Receive("NM_Player.Data", function(len)

	end)
end

function Player:NM_Data_Get(fullRealm, ...)
	local indexes = table.pack(...)
	local realm = self:NM_Realm_Get(fullRealm).__data
	if (table.Count(indexes) == 0) then
		return realm
	end
	local ldata = realm
	for _,index in pairs(indexes) do
		if (!ldata[index]) then
			return nil
		end
		ldata = ldata[index]
	end
	if (istable(ldata)) then
		return table.Copy(ldata) // That way we can see the difference when Set is run
	end
	return ldata
end
