// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	function Player:NM_Rank_Load(db_values)
		self.nm.rankdata = {}
		self.nm.subranks = {}
		self.nm.rank = "user"
		local rankrealm = "*"
		for k,row in pairs(db_values) do
			if (row.rank != "") then
				local rank = NM.ranks.Get(row.rank)
				if (rank) then
					table.insert(self.nm.rankdata, {rank = row.rank, subgrouping = rank.subgrouping, realm = row.realm, id = row.id})
					if (rank.subgrouping == 0) then
						if (row.realm == NM.realm || (row.realm == "*.garrysmod."..NM.server.gamemode && (rankrealm == "*.garrysmod" || rankrealm == "*")) || (row.realm == "*.garrysmod" && rankrealm == "*") || (row.realm == "*" && rankrealm == "*")) then
							self.nm.rank = row.rank
							rankrealm = row.realm
						end
					elseif (!table.HasValue(self.nm.subranks, row.rank)) then
						table.insert(self.nm.subranks, row.rank)
					end
				else
					NM.dev.Log(3, "Had invalid rank ", row.rank, " being given to player ", self:Name(), "!")
				end
			end
		end
	end

	Player.old_SetUserGroup = Player.SetUserGroup
	function Player:NM_Rank_SetRank(rank, realm)
		local realm = realm or NM.realm
		for k,v in pairs(self.nm.rankdata) do
			if (v.rank == rank) then
				return
			end
		end

		if (NM.ranks.Get(rank)) then
			local rankTbl = NM.ranks.Get(rank)

			local found = false
			for k,v in pairs(self.nm.rankdata) do
				if (v.subgrouping == rankTbl.subgrouping && v.realm == realm) then
					found = v.rank
					v.rank = rank
					break
				end
			end

			if (rankTbl.subgrouping == 0) then // Main rank
				self.nm.rank = rank
			else // Sub ranks
				table.insert(self.nm.subranks, rank)

				if (found) then
					for k,v in pairs(self.nm.subranks) do
						if (v == found) then
							self.nm.subranks[k] = nil
							break
						end
					end
				end
			end

            // API will perform its own checks and make the correct move, just add it :D
            NM.api.put("/member/"..self:NM_GetId().."/rank", {rank = rank}, realm, function(tbl)
                table.insert(self.nm.rankdata, {rank = rank, subgrouping = rankTbl.subgrouping, realm = realm, id = tbl.id})
            end)
		end
		return false
	end

	function Player:NM_Rank_RemoveRank(rankid, realm)
		local realm = realm or NM.realm
		if (rankid == nil) then
			return false
		elseif (tonumber(rankid) != nil) then // We got a database id, go ahead and KILL IT WITH FIRE
			for k,v in pairs(self.nm.rankdata) do
				if (v.id == rankid) then
					if (v.subgrouping == 0) then
						self.nm.rank = NM.ranks.defaultRank
					else
						table.RemoveByValue(self.nm.subranks, rank)
					end
					table.remove(self.nm.rankdata, k)
					break
				end
			end

            NM.api.delete("/member/"..self:NM_GetId().."/rank/"..rankid)
		elseif (NM.ranks.Get(rankid)) then
			// We got a rank name id, we'll have to do some figuring and logic to decide what we're removing...
			// We ONLY remove ranks if they have OUR realm in this case.(Or the game realm)
			local rank = rankid
			local rankTbl = NM.ranks.Get(rank)

			for k,v in pairs(self.nm.rankdata) do
				if (v.subgrouping == rankTbl.subgrouping && v.realm == realm) then
					if (rankTbl.subgrouping == 0) then
						self.nm.rank = NM.ranks.defaultRank
					else
						table.RemoveByValue(self.nm.subranks, rank)
					end
					table.remove(self.nm.rankdata, k)
					break
				end
			end

            NM.api.delete("/member/"..self:NM_GetId().."/rank", {rank = rankid}, realm)
		else
			error("Got invalid rank/rankid")
			return false
		end

		return true
	end
end

function Player:NM_CanTarget(other)
	if (self == other) then // You can always run commands on yourself
		return true
	end
	if (isstring(other)) then
		local userSpecific = self:NM_Permission_Has("nm.target."..other)
		if (userSpecific != nil) then // If we have a user specific rule, use that
			return userSpecific
		end
		return NM.ranks.CanTarget(self:NM_Rank_GetRank(), other)
	end
	local userSpecific = self:NM_Permission_Has("nm.target."..other:NM_GetId())
	if (userSpecific != nil) then // If we have a user specific rule, use that
		return userSpecific
	end
	local userSpecific = self:NM_Permission_Has("nm.target."..other:NM_Rank_GetRank())
	if (userSpecific != nil) then // If we have a user specific rule, use that
		return userSpecific
	end
	return NM.ranks.CanTarget(self:NM_Rank_GetRank(), other:NM_Rank_GetRank())
end

function Player:NM_Rank_GetRanks()
	if (self.fake) then return {} end
	return self:NM_Rank_GetRealRanks()
end

function Player:NM_Rank_GetRealRanks()
	local ranks = self:NM_Rank_GetRealSubRanks()
	table.insert(ranks, self:NM_Rank_GetRealRank())
	return ranks
end

Player.old_GetUserGroup = Player.GetUserGroup
function Player:NM_Rank_GetRank()
	if (SERVER && !self:IsFullyAuthenticated()) then return NM.ranks.defaultRank end
	if (self:NM_IsDisguised()) then return self:NM_TempData_Get("disguise", "rank") end

	return self:NM_Rank_GetRealRank()
end

function Player:NM_Rank_GetRealRank()
	if (SERVER && !self:IsFullyAuthenticated()) then return NM.ranks.defaultRank end

	if (self.nm && self.nm.rank != NM.ranks.defaultRank) then
		return self.nm.rank
	end
	return NM.ranks.defaultRank
end

function Player:NM_Rank_GetSubRanks()
	if (self:NM_IsDisguised()) then return {} end
	return table.Copy(self:NM_Rank_GetRealSubRanks())
end

function Player:NM_Rank_GetRealSubRanks()
	return self.nm && table.Copy(self.nm.subranks) or {}
end

Player.old_IsUserGroup = Player.IsUserGroup
function Player:NM_Rank_IsUserGroup(rank)
	if (self:NM_Rank_GetRank() == rank) then
		return true
	end
	if (table.HasValue(self:NM_Rank_GetSubRanks(), rank)) then
		return true
	end
	// The following is wrong, I know, but works in general... and helps with our system
	if (self:NM_Rank_CheckUserGroup(rank)) then
		return true
	end
	return false
end

function Player:NM_Rank_CheckUserGroups(ranks, real)
	if (istable(ranks)) then
		for k,v in pairs(ranks) do
			if (self:CheckGroup(v, real)) then
				return true
			end
		end
	else
		return self:CheckGroup(ranks, real)
	end
	return false
end
// Custom function, we don't need to hide it
Player.CheckGroups = Player.NM_Rank_CheckUserGroups
Player.CheckUserGroups = Player.NM_Rank_CheckUserGroups

Player.old_CheckGroup = Player.CheckGroup
function Player:NM_Rank_CheckUserGroup(rank, real)
	local plyrank = real and self:NM_Rank_GetRealRank() or self:NM_Rank_GetRank()

	if (NM.ranks.IsRank(plyrank, rank)) then
		return true
	end
	if (table.HasValue(self:NM_Rank_GetSubRanks(), rank)) then
		return true
	end
	return false
end

function Player:NM_Rank_IsModerator()
	return self:NM_Rank_IsAdmin() or self:NM_Permission_Has("garrysmod.moderator")
end
Player.IsModerator = Player.NM_Rank_IsModerator

Player.old_IsAdmin = Player.IsAdmin
function Player:NM_Rank_IsAdmin()
	return self:NM_Rank_IsSuperAdmin() or self:NM_Permission_Has("garrysmod.admin")
end

Player.old_IsSuperAdmin = Player.IsSuperAdmin
function Player:NM_Rank_IsSuperAdmin()
	return self:NM_Permission_Has("garrysmod.superadmin")
end

if (Player.old_GetUserGroup == Player.GetUserGroup) then -- We only take over when others don't
	if (SERVER) then
		--Player.SetUserGroup = Player.NM_Rank_SetRank
	end
	Player.GetUserGroup = Player.NM_Rank_GetRank
	Player.IsUserGroup = Player.NM_Rank_IsUserGroup
	Player.CheckUserGroup = Player.NM_Rank_CheckUserGroup
	Player.CheckGroup = Player.NM_Rank_CheckUserGroup
	Player.IsAdmin = Player.NM_Rank_IsAdmin
	Player.IsSuperAdmin = Player.NM_Rank_IsSuperAdmin
end

function Player:NM_IsDisguised()
	return self:NM_TempData_Get("disguise", "enabled")
end