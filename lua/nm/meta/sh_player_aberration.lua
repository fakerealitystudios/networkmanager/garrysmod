// RAWR!

local Player = FindMetaTable("Player")

function Player:IsManager()
	return self:NM_Permission_Has("nm.manager")
end

function Player:IsTrusted()
	return self:NM_Rank_IsSuperAdmin() or self:NM_Rank_CheckUserGroup("trustedadmin")
end

function Player:IsVIP()
	return self:NM_Rank_IsUserGroup("vip") or self:IsTrusted()
end

function Player:IsPremium()
	return self:NM_Rank_IsUserGroup("premium") or self:IsTrusted()
end