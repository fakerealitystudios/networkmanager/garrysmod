// RAWR!

local Player = FindMetaTable("Player")

function Player:NM_Alive()
    if (TEAM_GUARD_DEAD) then
        if (self:Team() == TEAM_GUARD_DEAD) then return false end
        if (self:Team() == TEAM_PRISONER_DEAD) then return false end
    end
    return self:Alive()
end
