// RAWR!

local Player = FindMetaTable("Player")

if (SERVER) then
	util.AddNetworkString("NM_Player.SendChat")
else
	local function sendChat(len)
		local tbl = net.ReadTable()
		if (!LocalPlayer().NM_SendChat) then return end
		LocalPlayer():NM_SendChat(unpack(tbl))
	end

	net.Receive("NM_Player.SendChat", sendChat)
end

function Player:NM_SendChat(...)
	local tbl = {...}
	if (table.Count(tbl) < 1) then return end
	if (SERVER) then
		net.Start("NM_Player.SendChat")
			net.WriteTable(tbl)
		net.Send(self)
	else
		local texttbl = {}
		for k,v in pairs(tbl) do
			if (!isstring(v) && IsValid(v) && v:IsPlayer()) then
				table.insert(texttbl, team.GetColor(v:Team()))
				table.insert(texttbl, v:Name())
				continue
			end
			table.insert(texttbl, v)
		end
		chat.AddText(unpack(texttbl))
	end
end

if (SERVER) then
	util.AddNetworkString("NM_Player.SendConsoleMessage")
else
	local function sendConsoleMessage(len)
		local tbl = net.ReadTable()
		if (!LocalPlayer().NM_SendConsoleMessage) then return end
		LocalPlayer():NM_SendConsoleMessage(unpack(tbl))
	end

	net.Receive("NM_Player.SendConsoleMessage", sendConsoleMessage)
end

function Player:NM_SendConsoleMessage(...)
	local tbl = {...}
	if (table.Count(tbl) < 1) then return end
	if (SERVER) then
		net.Start("NM_Player.SendConsoleMessage")
			net.WriteTable(tbl)
		net.Send(self)
	else
		MsgC(unpack(tbl))
		Msg("\n")
	end
end
