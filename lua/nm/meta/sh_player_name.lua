// RAWR!

local Player = FindMetaTable("Player")

Player.old_Nick = Player.old_Nick or Player.Nick
function Player:Nick()
	if (!IsValid(self)) then
		return ""
	end
	if (self.getDarkRPVar) then
		return self:getDarkRPVar("rpname")
	end
	return self:NM_IsDisguised() and self:NM_TempData_Get("disguise", "name") or self:NM_RealName()
end
Player.GetName = Player.Nick
Player.Name = Player.Nick

function Player:NM_RealName()
	if (!IsValid(self)) then
		return ""
	end
	return self:old_Nick()
end

// DarkRP is a GIANT WHORE
function Player:SteamName()
	return self:NM_RealName()
end