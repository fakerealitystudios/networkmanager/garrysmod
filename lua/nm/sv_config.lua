// RAWR!

// You really shouldn't edit this file, or any config file really. Use the data/nm/data/configurations.txt file instead.

NM.config.Add("api_authentication", {serverid = -1, secret = "XXXXXXXX"}, function(newValue, oldValue)
    if (NM.server && NM.server.ready) then
        NM.server.ready = false
        NM.server.index()
    end
end, nil, true)

NM.config.Add("password_auto", false, function(newValue, oldValue)
	if (newValue) then
		local pass = NM.util.GenPassword()
		RunConsoleCommand("sv_password",pass)
		MsgN("Changing the server's Password to: \""..pass.."\"")
	end
end, nil, true)

NM.config.Add("server_action_poll", 15, nil, nil, true) // Poll for server actions every x seconds, only used when socket listener is not available

// Config for api.networkmanager.center calls
NM.config.Add("nm_versioncheck", true, nil, nil, true)
NM.config.Add("nm_stats", true, nil, nil, true)
NM.config.Add("nm_hotfix", true, nil, nil, true) // I understand if you disable it, jsut stay up to date!
