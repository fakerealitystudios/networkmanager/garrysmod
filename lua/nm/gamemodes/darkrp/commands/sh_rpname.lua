// RAWR!

local function setrpname(ply, targets, name)
	NM.administration.Broadcast(ply, " set ", targets, "'s name to ", name)

	for k,target in pairs(targets) do
		target:setRPName(name)
	end
end
local setrpnameCommand = NM.commands.Add("darkrp", "setrpname", setrpname)
setrpnameCommand:AddParam(NM.commands.args.Player, "target", true, {})
setrpnameCommand:AddParam(NM.commands.args.String, "name", true, {})
setrpnameCommand:SetHelp("Set a player's RP Name")
setrpnameCommand:ConsoleSafe()
