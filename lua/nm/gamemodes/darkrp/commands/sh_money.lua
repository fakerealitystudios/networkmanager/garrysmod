// RAWR!

local function addmoney(ply, targets, amount)
	for k,target in pairs(targets) do
		local total = target:getDarkRPVar("money") + math.floor(amount)
		total = hook.Run("playerWalletChanged", target, amount, target:getDarkRPVar("money")) or total
		target:setDarkRPVar("money", total)
		if target.DarkRPUnInitialized then return end
		DarkRP.storeMoney(target, total)
	end

	NM.administration.Broadcast(ply, " gave ", targets, " ", GAMEMODE.Config.currency, total)
end
local addmoneyCommand = NM.commands.Add("darkrp", "addmoney", addmoney)
addmoneyCommand:AddParam(NM.commands.args.Player, "target", true, {})
addmoneyCommand:AddParam(NM.commands.args.Amount, "amount", true, {min = 0})
addmoneyCommand:SetHelp("Give a player money")
addmoneyCommand:ConsoleSafe()
