// RAWR!

local function jobban(ply, targets, teamid)
	for k,target in pairs(targets) do
        target:teamBan(teamid, 0)
	end

	NM.administration.Broadcast(ply, " banned ", targets, " from job ", team.GetName(teamid))
end
local jobbanCommand = NM.commands.Add("darkrp", "jobban", jobban)
jobbanCommand:AddParam(NM.commands.args.Player, "target", true, {})
jobbanCommand:AddParam(NM.commands.args.Team, "team", true, {})
jobbanCommand:SetHelp("Bans a player from a specified job")
jobbanCommand:ConsoleSafe()

local function jobunban(ply, targets, teamid)
	for k,target in pairs(targets) do
        target:teamUnBan(teamid)
	end

	NM.administration.Broadcast(ply, " unbanned ", targets, " from job ", team.GetName(teamid))
end
local jobunbanCommand = NM.commands.Add("darkrp", "jobunban", jobunban)
jobunbanCommand:AddParam(NM.commands.args.Player, "target", true, {})
jobunbanCommand:AddParam(NM.commands.args.Team, "team", true, {})
jobunbanCommand:SetHelp("Bans a player from a specified job")
jobunbanCommand:ConsoleSafe()
