// RAWR!

local function guardban(ply, targets, bool)
    for k,target in pairs(targets) do
        if (bool && (target:Team() == TEAM_GUARD || target:Team() == TEAM_GUARD_DEAD)) then
            target:KillSilent()
            target:SetTeam(TEAM_PRISONER)
        end
        target:NM_Data_Set(NM.realm, bool, "jailbreak", "guardban")
    end
    NM.administration.Broadcast(ply, {bool, " banned ", " unbanned "}, targets, " from the guard team.")
end
local guardbanCommand = NM.commands.Add("jailbreak", "guardban", guardban, {"gaurdban"}) // alias in case you don't know how to spell
guardbanCommand:AddParam(NM.commands.args.Players, "targets", true, {})
guardbanCommand:AddParam(NM.commands.args.Boolean, "bool", false, {default = true, hidden = true})
guardbanCommand:SetOpposite("unguardban", {nil, false}, {"ungaurdban"})
guardbanCommand:SetHelp("Ban a player from the guard team")
guardbanCommand:ConsoleSafe()
