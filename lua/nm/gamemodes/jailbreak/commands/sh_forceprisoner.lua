// RAWR!

local function forceprisoner(ply, targets)
	for k,target in pairs(targets) do
        target:KillSilent()
		target:SetTeam(TEAM_PRISONER)
        target.jb_nobalance = true
	end
	NM.administration.Broadcast(ply, " forced ", targets, " to the prisoner team.")
end
local forceprisonerCommand = NM.commands.Add("jailbreak", "forceprisoner", forceprisoner)
forceprisonerCommand:AddParam(NM.commands.args.Players, "targets", true, {})
forceprisonerCommand:SetHelp("Force a player to prisoner team")
forceprisonerCommand:ConsoleSafe()
