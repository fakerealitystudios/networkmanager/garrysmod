// RAWR!

local function forceguard(ply, targets)
	for k,target in pairs(targets) do
        target:KillSilent()
		target:SetTeam(TEAM_GUARD)
        target.jb_nobalance = true
	end
	NM.administration.Broadcast(ply, " forced ", targets, " to the guard team.")
end
local forceguardCommand = NM.commands.Add("jailbreak", "forceguard", forceguard)
forceguardCommand:AddParam(NM.commands.args.Players, "targets", true, {})
forceguardCommand:SetHelp("Force a player to guard team")
forceguardCommand:ConsoleSafe()
