// RAWR!

local WardenModel = Model("models/player/combine_super_soldier.mdl");
local GuardModels = {
	Model("models/player/police.mdl"),
	Model("models/player/combine_soldier.mdl"),
	Model("models/player/combine_soldier_prisonguard.mdl")
}

local transfers = 0
hook.Add("JB_NewRound", "JB_NewRound.Transfers", function()
    transfers = 3
end)

local function transferwarden(ply, targets)
    if (ply != GAMEMODE:GetGlobalVar("warden")) then
		return false, "You're not the warden!"
    elseif (!ply:NM_Alive()) then
        return false, "You're dead, you can't transfer warden!"
    end
    if (transfers <= 0) then
        return false, "Out of transfers!"
    end
	for k,target in pairs(targets) do
        if (target:Team() != TEAM_GUARD) then
            return false, target:Nick().." is not a guard!"
        else
            transfers = transfers - 1

            ply:SetModel(table.Random(GuardModels))
    		ply:SetArmor(math.max(0, ply:Armor()-25))
    		GAMEMODE:SetGlobalVar("warden", NULL)
    		GAMEMODE:SetGlobalVar("warden", target)
    		target:SetModel(WardenModel)
            target:SetArmor(math.min(100, target:Armor()+25))
        end
	end
	NM.administration.Broadcast(ply, " transfered warden to ", targets, "("..transfers.." transfers left)")
end
local transferwardenCommand = NM.commands.Add("jailbreak", "transferwarden", transferwarden)
transferwardenCommand:AddParam(NM.commands.args.Player, "target", true, {canTargetHigherRank = true})
transferwardenCommand:SetHelp("Transfer warden to another player")

local function forcewarden(ply, targets)
	for k,target in pairs(targets) do
        if (target:Team() != TEAM_GUARD) then
            return false, target:Nick().." is not a guard!"
        else
            local warden = GAMEMODE:GetGlobalVar("warden")
            if (warden && warden:Team() == TEAM_GUARD) then
                warden:SetModel(table.Random(GuardModels))
        		warden:SetArmor(math.max(0, ply:Armor()-25))
            end
    		GAMEMODE:SetGlobalVar("warden", NULL)
    		GAMEMODE:SetGlobalVar("warden", target)
    		target:SetModel(WardenModel)
            target:SetArmor(math.min(100, target:Armor()+25))
        end
	end
	NM.administration.Broadcast(ply, " set ", targets, " to warden.")
end
local forcewardenCommand = NM.commands.Add("jailbreak", "forcewarden", forcewarden)
forcewardenCommand:AddParam(NM.commands.args.Player, "target", true, {})
forcewardenCommand:SetHelp("Force warden on a player")
