// RAWR!

local function friendlyfire(ply, bool)
    if (ply != GAMEMODE:GetGlobalVar("warden")) then
        return false, "You're not the warden!"
    elseif (!ply:NM_Alive()) then
        return false, "You're dead, you can't change friendly fire!"
    end
    if (bool == nil) then
        GAMEMODE.jb_friendlyfire = !GAMEMODE.jb_friendlyfire
    elseif (bool) then
        GAMEMODE.jb_friendlyfire = true
    else
        GAMEMODE.jb_friendlyfire = false
    end
	NM.administration.Broadcast(ply, " ", {GAMEMODE.jb_friendlyfire, "enabled", "disabled"}, " friendly fire.")
end
local friendlyfireCommand = NM.commands.Add("jailbreak", "friendlyfire", friendlyfire, {"ff"})
friendlyfireCommand:AddParam(NM.commands.args.Boolean, "on/off", false, {})
friendlyfireCommand:SetHelp("Enable prisoner friendly fire")

local function forcefriendlyfire(ply, bool)
    if (bool == nil) then
        GAMEMODE.jb_friendlyfire = !GAMEMODE.jb_friendlyfire
    elseif (bool) then
        GAMEMODE.jb_friendlyfire = true
    else
        GAMEMODE.jb_friendlyfire = false
    end
	NM.administration.Broadcast(ply, " ", {GAMEMODE.jb_friendlyfire, "enabled", "disabled"}, " friendly fire.")
end
local forcefriendlyfireCommand = NM.commands.Add("jailbreak", "forcefriendlyfire", forcefriendlyfire, {"forceff"})
forcefriendlyfireCommand:AddParam(NM.commands.args.Boolean, "on/off", false, {})
forcefriendlyfireCommand:SetHelp("Enable prisoner friendly fire")
