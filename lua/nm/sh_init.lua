// RAWR!

NM = NM or {}
NM._G = NM._G or _G
NM.meta = NM.meta or {}
NM.nm_version = "1.1.2"
NM.version = "1.2.6"
NM.siteVersion = NM.version
NM.folderName = "nm"
NM.realmDelimiter = "."
NM.permissionDelimiter = "."
NM.dataDelimiter = "|"
NM.game = "garrysmod" // Game to be used for the realm
NM.realm = NM.realm or "*"..NM.realmDelimiter..NM.game // Temporary, until the gamemode loads!
NM.lastReload = CurTime()
NM.permissions = NM.permissions or {}
NM.initialized = NM.initialized or false // Should be true after the first initialization(when the server first starts)
NM.gminitialized = NM.gminitialized or false // Should be true after the first initialization(when the gamemode first starts)

NM.internaldirs = {}
include("sh_util.lua")
if (SERVER) then
	AddCSLuaFile("sh_util.lua")
end

function NM.DisplayVersions()
	MsgC(Color(100,255,100), "[NM-VC] ", Color(255,255,255), "NetworkManager "..NM.version.." loaded!\n")

	for k,v in pairs(NM) do
		if (istable(v)&&v.version) then
			local title = v.title or k:sub(1,1):upper()..k:sub(2)
			MsgC(Color(100,255,100), "[NM-VC] ", Color(255,255,255), "NM "..title.." "..v.version.." loaded!\n")
		end
	end
end

if (SERVER) then
    function NM.GetId()
        if (!NM.config) then
            return -1
        end
        return NM.config.Get("api_authentication").serverid
    end
end

function NM.Initialize()
	// We include directories in a SPECIFIC priority
	// Core items are things that will be used by the following includes, and thus must be loaded first
	NM.util.IncludeInternalDir("core")
	// Includes the base config, this file should NOT be editted
	NM.util.IncludeInternal("sh_config.lua")
	NM.util.IncludeInternal("sv_config.lua") // Server one for server only values
	hook.Run("NM_Included.Core") // The base configs and database structures are all apart of the core

	// Libraries are then loaded, these contain specific functionalities and are used in-junctions with hooks and configs(these are modules/plugins)
	NM.util.IncludeInternalDir("libs")
	hook.Run("NM_Included.Libs")
	// Includes the metatables for the NM data types
	NM.util.IncludeInternalDir("meta")
	NM.util.IncludeInternal("meta/sh_player.lua") // Re-load, it requires our own methods
	hook.Run("NM_Included.Meta")
	// Includes hooks. Hooks should be created within this folder to keep a clean structure(these are included late so we can have all modules loaded)
	NM.util.IncludeInternalDir("hooks")
	hook.Run("NM_Included.Hooks")

	NM.util.IncludeInternalDir("derma")
	NM.util.IncludeInternalDir("commands")

	// Library Initialization
	for k,v in pairs(NM) do
		if (istable(v)) then
			if (v.ShInitialize) then
				v.ShInitialize() // Shared initialize(useful when you want Initialize in server/client realms, but also in shared)
			end
			if (v.Initialize) then
				v.Initialize()
			end
		end
	end

	// Include and Initialize gamemode specific files
	if (GM || GAMEMODE) then
		NM.GMInitialize()
	end

	// Finally, display the current module versions
	NM.DisplayVersions()

	NM.initialized = true
	hook.Run("NM_Initialized")

	if (NM.config.Get("nm_versioncheck")) then
		NM.api.external.VersionCheck()
	end

	// Pulls down the hotfix lua from our api
	// disable the config "nm_hotfix" if you don't trust website loading lua(I understand, just stay up to date!)
	if (NM.config.Get("nm_hotfix")) then
		NM.api.external.LoadHotfix()
	end
end

function NM.GMInitialize()
	// Gamemode specific implementations
	if (DarkRP && gmod.GetGamemode().FolderName != "darkrp") then // just incase you renamed it or maybe it's still darkrp-master or whatever
		// Or maybe it's a deriviation(like zombierp) that wants it's own stuff and DarkRP's
		// We include it first though, cause then if we want, we could override commands and shit in the later files ;)
		NM.util.IncludeInternalDir("gamemodes/".."darkrp")
		NM.util.IncludeInternalDir("gamemodes/".."darkrp".."/hooks")
		NM.util.IncludeInternalDir("gamemodes/".."darkrp".."/config")
		NM.util.IncludeInternalDir("gamemodes/".."darkrp".."/commands")
	end
	NM.util.IncludeInternalDir("gamemodes/"..gmod.GetGamemode().FolderName)
	NM.util.IncludeInternalDir("gamemodes/"..gmod.GetGamemode().FolderName.."/hooks")
	NM.util.IncludeInternalDir("gamemodes/"..gmod.GetGamemode().FolderName.."/config")
	NM.util.IncludeInternalDir("gamemodes/"..gmod.GetGamemode().FolderName.."/commands")
	NM.util.IncludeInternalDir("maps/"..game.GetMap())
	NM.util.IncludeInternalDir("maps/"..game.GetMap().."/hooks")
	NM.util.IncludeInternalDir("maps/"..game.GetMap().."/config")

	// Library Gamemode Initialization
	for k,v in pairs(NM) do
		if (istable(v)&&v.GMInitialize) then
			v.GMInitialize()
		end
	end

	NM.gminitialized = true
end

hook.Add("Initialize", "Initialize.NM_Gamemode", function()
	NM.GMInitialize()
end)

if (SERVER) then
	function NM.OnDatabaseReady()
		for k,v in pairs(NM) do
			if (istable(v)) then
				if (v.OnDatabaseReady) then
					v.OnDatabaseReady()
				end
			end
		end
	end

	hook.Add("NM_Database.Ready", "NM_Database.Ready.Initialize", function(uid)
		NM.OnDatabaseReady()
	end)

	function NM.OnServerReady()
		for k,v in pairs(NM) do
			if (istable(v)) then
				if (v.OnServerReady) then
					v.OnServerReady()
				end
			end
		end
	end

	hook.Add("NM_Server.Ready", "NM_Server.Ready.Initialize", function(uid)
		NM.OnServerReady()
	end)

	function NM.OnShutDown()
		for k,v in pairs(NM) do
			if (istable(v)) then
				if (v.OnShutDown) then
					v.OnShutDown()
				end
			end
		end
	end

	hook.Add("ShutDown", "ShutDown.NM_ShutDown", function(uid)
		NM.OnShutDown()
	end)
end

NM.Initialize()
