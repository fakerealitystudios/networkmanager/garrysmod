// RAWR!

// NOTE: Network Manager does not implement user-rank changes nor group changes
// Nor does it supply other administration mods with NM permissions
// This is designed as such because of the extensive web-end of the service

CAMI.NM_TOKEN = "NM"

local function onGroupRegistered(camiGroup, originToken)
	if originToken == CAMI.NM_TOKEN then return end
	
	// Network Manager is more then your measly gmod...
end
hook.Add("CAMI.OnUsergroupRegistered", "NM_Administration.CamiGroupRegistered", onGroupRegistered)

local function onGroupRemoved(camiGroup, originToken)
	if originToken == CAMI.NM_TOKEN then return end
	
	// Network Manager is more then your measly gmod...
end
hook.Add("CAMI.OnUsergroupUnregistered", "NM_Administration.CamiGroupRemoved", onGroupRemoved)

local function onPlayerUserGroupChanged(ply, oldGroup, newGroup, originToken)
	if originToken == CAMI.NM_TOKEN then return end

	// Network Manager is more then your measly gmod...
end
hook.Add("CAMI.PlayerUsergroupChanged", "NM_Administration.CamiPlayerUserGroupChanged", onPlayerUserGroupChanged)

local function onSteamIDUserGroupChanged(steamId, oldGroup, newGroup, originToken)
	if originToken == CAMI.NM_TOKEN then return end

	// Network Manager is more then your measly gmod...
end
hook.Add("CAMI.SteamIDUsergroupChanged", "NM_Administration.CamiSteamIDUserGroupChanged", onSteamIDUserGroupChanged)

local function onPrivilegeRegistered(camiPriv)
	// We use a very different privilege system then other addons
end
hook.Add("CAMI.OnPrivilegeRegistered", "NM_Administration.CamiPrivilegeRegistered", onPrivilegeRegistered)

local function onPrivilegeUnregistered(priv)
	// We use a very different privilege system then other addons
end
hook.Add("CAMI.OnPrivilegeUnregistered", "NM_Administration.CamiPrivilegeUnregistered", onPrivilegeUnregistered)

local function playerHasAccess(ply, priv, callback, target, extra)
	// We replace space with period, to match our own permissions
	local priv = string.Replace(priv:lower(), " ", ".")
	if (!ply.NM_Permission_Has) then return end // We can't anwser that yet
	local result = ply:NM_Permission_Has(priv, false)
	if (result && IsValid(target) && (!extra || !extra.IgnoreImmunity)) then
		result = ply:NM_CanTarget(target)
		if (!result) then
			callback(false, "That target is immune to you.")
		end
	end
	callback(result, "You don't have permission to do that!")
	return true
end
hook.Add("CAMI.PlayerHasAccess", "NM_Administration.CamiPlayerHasAccess", playerHasAccess)

local function steamIDHasAccess(steamid, priv, callback, target, extra)
	NM.players.external.GetSteamPlayer(steamid, function(ply)
		playerHasAccess(ply, priv, callback, target, extra)
	end)
	return true
end
hook.Add("CAMI.SteamIDHasAccess", "NM_Administration.CamiSteamidHasAccess", steamIDHasAccess)
