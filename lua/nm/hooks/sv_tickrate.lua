// RAWR!

NM.tickrate = NM.tickrate or {}
NM.tickrate.interval = 5
NM.tickrate.expected = NM.tickrate.expected or 0
NM.tickrate.count = NM.tickrate.count or 0
NM.tickrate.reset = NM.tickrate.reset or 0

hook.Add("Tick", "NM_Server.TickRate", function()
    local time = SysTime()
    if (NM.tickrate.count == 0) then
        NM.tickrate.reset = time + NM.tickrate.interval + 5// add 5 seconds, we don't start tracking for a 5 seconds delay
        NM.tickrate.count = 1
    elseif (time < NM.tickrate.reset - 5) then
        // Do nothing, wait 5 seconds after the server starts
    elseif (time < NM.tickrate.reset) then
        NM.tickrate.count = NM.tickrate.count + 1
    else
        NM.tickrate.expected = math.Round(NM.tickrate.count / ((time - NM.tickrate.reset) + NM.tickrate.interval))

        NM.tickrate.reset = SysTime() + NM.tickrate.interval
        NM.tickrate.count = 0
        // Overwrite hook.
        hook.Remove("Tick", "NM_Server.TickRate")
        hook.Add("Tick", "NM_Server.TickRate", NM.tickrate.Tick)
    end
end)

function NM.tickrate.Tick()
    local time = SysTime()
    if (time > NM.tickrate.reset) then
        local rate = math.Round(NM.tickrate.count / ((time - NM.tickrate.reset) + NM.tickrate.interval))
        // TODO: Save this rate as a 'server.tickrate' server statistic
        NM.tickrate.reset = SysTime() + NM.tickrate.interval
        NM.tickrate.count = 0
    end
    NM.tickrate.count = NM.tickrate.count + 1
end