// RAWR!

hook.Add("InitPostEntity", "NM_InitPostEntity", function()
	LocalPlayer():NM_InitializePlayer()
    for k,v in pairs(player.GetAll()) do
        if (!IsValid(v)||!v.NM_InitializePlayer) then continue end
        v:NM_InitializePlayer()
    end

	hook.Run("NM_Player.ClientInitialized")
	net.Start("NM_Player.ClientInitialized")
	net.SendToServer()
end)

net.Receive("NM_Player.Ready", function()
    local ply = net.ReadEntity()
    if (!IsValid(ply)||!ply.NM_InitializePlayer) then return end
    ply:NM_InitializePlayer()
	hook.Run("NM_Player.Ready", ply)
end)

net.Receive("NM_Player.Network", function()
    local ply = net.ReadEntity()
    local tbl = net.ReadTable()
    for k,v in pairs(tbl) do
        if (NM[k] && istable(NM[k]) && NM[k].NetworkPlayer) then
            NM[k].NetworkPlayer(ply, v)
        end
    end
end)

net.Receive("NM_Player.Ranks", function()
    local ply = net.ReadEntity()
    if (IsValid(ply)) then
        ply.nm = ply.nm or {}
        ply.nm.rank = net.ReadString()
        ply.nm.subranks = net.ReadTable()

    	hook.Run("NM_Player.Ranks", ply)
    end
end)
