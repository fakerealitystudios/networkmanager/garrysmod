// RAWR!

hook.Add("PlayerSpawnedEffect", "NM_PlayerSpawned", function(ply, mdl, ent)
	ent.NM_Creator = ply
    ent.NM_Creator_Id = ply:NM_GetId()
	ent.NM_Creator_SessionId = ply:NM_GetSessionId()
end)

hook.Add("PlayerSpawnedNPC", "NM_PlayerSpawned", function(ply, ent)
	ent.NM_Creator = ply
    ent.NM_Creator_Id = ply:NM_GetId()
	ent.NM_Creator_SessionId = ply:NM_GetSessionId()
end)

hook.Add("PlayerSpawnedProp", "NM_PlayerSpawned", function(ply, mdl, ent)
	ent.NM_Creator = ply
	ent.NM_Creator_Id = ply:NM_GetId()
	ent.NM_Creator_SessionId = ply:NM_GetSessionId()
end)

hook.Add("PlayerSpawnedRagdoll", "NM_PlayerSpawned", function(ply, mdl, ent)
	ent.NM_Creator = ply
    ent.NM_Creator_Id = ply:NM_GetId()
	ent.NM_Creator_Session = ply:NM_GetSessionId()
end)

hook.Add("PlayerSpawnedSENT", "NM_PlayerSpawned", function(ply, ent)
	ent.NM_Creator = ply
    ent.NM_Creator_Id = ply:NM_GetId()
	ent.NM_Creator_SessionId = ply:NM_GetSessionId()
end)

hook.Add("PlayerSpawnedSWEP", "NM_PlayerSpawned", function(ply, ent)
	ent.NM_Creator = ply
    ent.NM_Creator_Id = ply:NM_GetId()
	ent.NM_Creator_SessionId = ply:NM_GetSessionId()
end)

hook.Add("PlayerSpawnedVehicle", "NM_PlayerSpawned", function(ply, ent)
	ent.NM_Creator = ply
    ent.NM_Creator_Id = ply:NM_GetId()
	ent.NM_Creator_SessionId = ply:NM_GetSessionId()
end)
