// RAWR!

hook.Add("CheckPassword", "CheckPassword.BanSystem", function(steam, ipAddress, svPassword, clPassword, name)
	if (NM.bans.IsBanned(steam)) then
		return false, NM.bans.GetReason(steam)
	end
end, HOOK_MONITOR_LOW)

hook.Add("PlayerInitialSpawn", "PlayerInitialSpawn.BanSystem", function(ply)
	if (NM.bans.IsBanned(ply:SteamID64())) then
		NM.administration.Kick(ply, NM.bans.GetReason(ply:SteamID64()))
		return false
	end
end, HOOK_MONITOR_HIGH) // Do this after everything else, so we don't mess things up
