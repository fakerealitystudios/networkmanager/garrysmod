// RAWR!

NM.joinTable = NM.joinTable or {}
NM.mapStart = NM.mapStart or os.time()

util.AddNetworkString("NM_Player.ClientInitialized")
util.AddNetworkString("NM_Player.Ready")
util.AddNetworkString("NM_Player.Ranks")
util.AddNetworkString("NM_Player.Network")

//hook.Remove("PlayerInitialSpawn", "PlayerAuthSpawn")

net.Receive("NM_Player.ClientInitialized", function(len, ply)
	hook.Run("NM_Player.ClientInitialized", ply)
end)

gameevent.Listen("player_connect")
hook.Add("player_connect", "NM_player_connect", function(data)
	local steamid64 = data.networkid
	if (data.networkid:find("STEAM_%d:%d:%d+")) then
		steamid64 = util.SteamIDTo64(data.networkid)
	end
	hook.Run("NM_Player.Connected", data.name, steamid64, data.address, data.bot == 1)
end)

gameevent.Listen("player_disconnect")
hook.Add("player_disconnect", "NM_player_disconnect", function(data)
	local steamid64 = data.networkid
	if (data.networkid:find("STEAM_%d:%d:%d+")) then
		steamid64 = util.SteamIDTo64(data.networkid)
	end
	hook.Run("NM_Player.Disconnected", data.name, steamid64, data.bot == 1)
end)

NM.PlayerLookup = NM.PlayerLookup or {}
hook.Add("NM_Player.Connected", "NM_Player.Connected.NetworkManager", function(name, steamid64, address, bot)
	
end)

hook.Remove("CheckPassword", "MK_CheckPassword")
hook.Add("CheckPassword", "NM_Player.CheckPassword", function(steam, ipAddress, svPassword, clPassword, name)
	local steamid = util.SteamIDFrom64(steam)
	NM.joinTable[steamid] = os.time()

	NM.dev.Log(6, string.format("<CheckPassword> %s | %s | %s attempted to use password '%s'", name, steam, ipAddress, clPassword))

	if (!svPassword || svPassword == "" || svPassword == clPassword) then
		return true
	end

	if (NM.players.external.GetPlayer(steam, function () end, nil, name)) then // Some people will have to rejoin while the server collects their data
		if (NM.server.GetSetting("whitelisted", false)) then
			return NM.players.external.list[steam]:NM_Permission_Has("nm.whitelisted") == true, "You're not on the whitelist!"
		end
		return NM.players.external.list[steam]:NM_Permission_Has("nm.whitelisted") == true, "You don't have permission to bypass the password!"
	elseif (NM.server.GetSetting("whitelisted", false)) then
		return false, "Server whitelisted and hasn't loaded your data\nTry rejoining in 5 seconds..."
	else
		return false, "Server password protected and hasn't loaded your data\nTry rejoining in 5 seconds..."
	end

	return false, "Password is incorrect"
end, HOOK_LOW)

hook.Add("PlayerAuthed", "NM_Player.Authed", function(ply, steamid, uniqueid)
	ply:NM_DB_LoadData()
end)

hook.Add("PlayerInitialSpawn", "NM_Player.InitialSpawn", function(ply)
	local time = os.time() - NM.mapStart
	if (NM.joinTable[ply:SteamID()]) then
		time = os.time() - NM.joinTable[ply:SteamID()]
	end
	if (ply:IsBot()) then
		time = 0
	end
	NM.dev.Log(6, string.format("<PlayerInitialSpawn> %s | %s | %s (took %s seconds)", ply:Nick(), ply:SteamID(), ply:IPAddress(), time))
end)

hook.Add("PlayerDisconnected", "NM_PlayerDisconnected", function(ply)
    NM.api.delete("/member/"..ply:NM_GetId().."/session/"..ply:NM_GetSessionId())
end)

hook.Add("PlayerSay", "NM_PlayerSay", function(ply, text, teamOnly)
	if (!ULib || !ulx) then // We'll hide ourselves if we see ULX... (I still dislike it though, but this is nicer then breaking things)
		local args = NM.util.ExtractArgs(text)

		if (NM.commands.Parse(ply, args, true)) then
			return ""
		end
	end
end, HOOK_HIGH)

local function networkPlayer(ply)
	hook.Run("NM_Player.Ready", ply)
	net.Start("NM_Player.Ready")
		net.WriteEntity(ply)
	net.Broadcast()

	net.Start("NM_Player.Ranks")
		net.WriteEntity(ply)
		net.WriteString(ply.nm.rank)
		net.WriteTable(ply.nm.subranks)
	net.Broadcast()

	for k,v in pairs(player.GetAll()) do
		if (v:NM_GetId() == -1) then continue end
		if (!v.nm.clientInitialized) then continue end
		net.Start("NM_Player.Ranks")
			net.WriteEntity(v)
			net.WriteString(v.nm.rank)
			net.WriteTable(v.nm.subranks)
		net.Send(ply)
	end

	// Fancy network manager library networking, enables us to easily network library data to users
	local ntwrk = {}
	local brdcst = {}
	for k,v in pairs(NM) do
		if (istable(v)) then
			if (v.NetworkPlayer) then
				ntwrk[k] = v.NetworkPlayer(ply)
			end
			if (v.NetworkBroadcast) then
				brdcst[k] = v.NetworkBroadcast(ply)
			end
		end
	end
	if (table.Count(ntwrk) > 0) then
		net.Start("NM_Player.Network")
			net.WriteEntity(ply)
			net.WriteTable(ntwrk)
		net.Send(ply)
	end
	if (table.Count(brdcst) > 0) then
		net.Start("NM_Player.Network")
			net.WriteEntity(ply)
			net.WriteTable(brdcst)
		net.Broadcast()
	end
end

hook.Add("NM_Player.Loaded", "NM_Player.Loaded", function(ply, newUser)
	ply.nm.loadedData = true

	if (ply.nm.clientInitialized) then
		networkPlayer(ply)
	end
end)

hook.Add("NM_Player.ClientInitialized", "NM_Player.ClientInitialized.SendData", function(ply)
	ply.nm.clientInitialized = true

	if (ply.nm.loadedData) then
		networkPlayer(ply)
	end

	if (NM.config.Get("show_versions", false)) then
		ply:NM_DisplayVersions()
	end
	if (ply:IsSuperAdmin() && NM.api.external.update) then
		ply:NM_SendChat(Color(100,255,100), "[NM-VC] ", Color(255,50,50), "Network manager update available! Check networkmanager.io")
		ply:NM_SendChat(Color(100,255,100), "[NM-VC] ", Color(255,255,255), "Current Version: ", Color(150,150,150), NM.version, Color(255,255,255), " Site Version: ", Color(150,150,150), NM.siteVersion)
	end
end)