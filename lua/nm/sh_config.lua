// RAWR!

// You really shouldn't edit this file, or any config file really. Use the data/nm/data/configurations.txt file instead.

NM.config.Add("api_endpoint", "https://demo.networkmanager.io");

// Verbosity of sending logs to devlog files
NM.config.Add("log_verbosity", 3, function(newValue, oldValue, data)
	if (newValue < 2) then return false end // We're instilling a hard minimum of 2
end, {clientMalleable = true}, true)

// Verbosity of sending logs to console
NM.config.Add("console_verbosity", 3, function(newValue, oldValue, data)
	if (newValue < 2) then return false end // We're instilling a hard minimum of 2
end, {clientMalleable = true}, true)

NM.config.Add("development", false, function(newValue, oldValue, data)
	if (newValue) then
		NM.config.Set("console_verbosity", 8, false) // Set higher verbosity in development mode
		NM.dev.Log(7, "NM Core set to development mode")
	end
end, {clientMalleable = true}, true)

NM.config.Add("administration_commandChatPrefix", "!")

if (CLIENT) then
	NM.config.Add("show_versions", false, nil, {clientMalleable = true}, true)
end

NM.config.Add("motd_sites", {
	["Steam Group"] = "https://www.networkmanager.io/",
	["Rules"] = "https://www.networkmanager.io/",
	["Donate"] = "https://www.networkmanager.io/",
	["Forums"] = "https://www.networkmanager.io/"
})

if (CLIENT) then
	// Called once we receive it from the server
	NM.config.Hook("motd_sites", "motd_sites.mjc", function(newValue, oldValue, data)
		if (!NM.config.networked || nut) then return end
		if (IsValid(MjcMOTDPanel_Backframe)) then MjcMOTDPanel_Backframe:Remove() end
		for k,v in pairs(newValue) do
			AddMjcMOTDButton(k, {
				func = v,
				buttoncolor = Color(36,36,36),
				buttoncolorhover = Color(158, 163, 168),
				textcolor = Color(255,255,255),
			})
		end
		MjcMOTD()
		gui.EnableScreenClicker(true)
	end)
end

// Config for api.networkmanager.center calls
NM.config.Add("nm_statistics", true, nil, {clientMalleable = true}, true)
