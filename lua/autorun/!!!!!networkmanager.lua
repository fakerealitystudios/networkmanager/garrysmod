// RAWR!

// Just so everyone knows, we use ! in the name since it has a low value in the ASCII table. We need to be loaded first otherwise we can experience weird issues.

if SERVER then
	include("nm/sv_init.lua")
else
	include("nm/cl_init.lua")
end